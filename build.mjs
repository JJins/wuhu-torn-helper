/**
 * 此脚本用于加入userscript meta，
 * 并生成日期时间与版本号
 */

import { readFileSync, writeFileSync } from "fs";
import { prodConfig } from "./rollup.config.js";

let date = new Date();
let version = process.env.npm_package_version;
let formattedDateTime = `${ date.getFullYear() }${ ('0' + (date.getMonth() + 1)).slice(-2) }${ ('0' + date.getDate()).slice(-2) }${ ('0' + date.getHours()).slice(-2) }${ ('0' + date.getMinutes()).slice(-2) }`;
let metaData =
    `// ==UserScript==
// @lastmodified ${ formattedDateTime }
// @name         芜湖助手
// @namespace    WOOH
// @version      ${ version }
// @description  托恩，起飞！
// @author       Woohoo[2687093] Sabrina_Devil[2696209]
// @match        https://www.torn.com/*
// @downloadURL  https://gitlab.com/JJins/wuhu-torn-helper/-/raw/dev/release.min.user.js
// @grant        GM_xmlhttpRequest
// @grant        unsafeWindow
// @connect      yata.yt
// @connect      github.io
// @connect      gitlab.com
// @connect      staticfile.org
// @connect      gitee.com
// ==/UserScript==
`

const data = readFileSync('./' + prodConfig.output.file, 'utf8');
writeFileSync(
    './release.min.user.js',
    metaData + data.replace('$$WUHU_DEV_VERSION$$', version),
    'utf8'
);
// rmSync('./' + prodConfig.output.file);
console.log(`版本 ${ version } 构建完成`);
