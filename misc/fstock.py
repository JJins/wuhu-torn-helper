import json
from datetime import datetime, timezone, timedelta
import os
import requests

DEST_ARR = ['mex', 'cay', 'can', 'haw', 'uni', 'arg', 'swi', 'jap', 'chi', 'uae', 'sou']
DEST_TRANS = {
    'mex': '墨', 'cay': '开', 'can': '加',
    'haw': '夏', 'uni': '嘤', 'arg': '阿',
    'swi': '瑞', 'jap': '日', 'chi': '中', 'uae': '迪', 'sou': '南',
}
VALUED_ITEM_ARR = [
    'Dahlia', 'Jaguar Plushie',
    'Banana Orchid', 'Stingray Plushie',
    'Crocus', 'Wolverine Plushie',
    'Orchid', 'Large Suitcase',
    'Red Fox Plushie', 'Nessie Plushie', 'Heather',
    'Ceibo Flower', 'Monkey Plushie', 'Tear Gas',
    'Chamois Plushie', 'Edelweiss',
    'Cherry Blossom',
    'Panda Plushie', 'Peony',
    'Camel Plushie', 'Tribulus Omanense',
    'Lion Plushie', 'African Violet', 'Xanax',
]
ITEM_TRANS = {
    'Dahlia': '花', 'Jaguar Plushie': '偶',
    'Banana Orchid': '花', 'Stingray Plushie': '偶',
    'Crocus': '花', 'Wolverine Plushie': '偶',
    'Orchid': '花', 'Large Suitcase': '大箱',
    'Red Fox Plushie': '赤狐', 'Nessie Plushie': '水怪', 'Heather': '花',
    'Ceibo Flower': '花', 'Monkey Plushie': '偶', 'Tear Gas': '催泪弹',
    'Edelweiss': '花', 'Chamois Plushie': '偶',
    'Cherry Blossom': '花',
    'Peony': '花', 'Panda Plushie': '偶',
    'Tribulus Omanense': '花', 'Camel Plushie': '偶',
    'African Violet': '花', 'Lion Plushie': '偶', 'Xanax': 'XAN',
}
HISTORY_FILE = 'stock_hist.json'
HISTORY_EXIST = os.path.exists(HISTORY_FILE)
UTC_8 = timezone(timedelta(hours=8))

URL = "https://yata.yt/api/v1/travel/export/"
resObj = requests.get(URL).json()["stocks"]

# 更新时间
lastUpdate = 0
for dest in DEST_ARR:
    res_update = resObj[dest]["update"]
    if lastUpdate < res_update:
        lastUpdate = res_update
lastUpdate = datetime.fromtimestamp(lastUpdate).astimezone(UTC_8)
UPDATE_TIME_STR = lastUpdate.strftime('%H:%M:%S')

# 整理过的对象
stockObj = {}
for dest in DEST_ARR:
    arr = []
    for f_item in resObj[dest]['stocks']:
        for v_item in VALUED_ITEM_ARR:
            if f_item['name'] == v_item:
                arr.append(f_item)
    stockObj[dest] = arr

# 移除亏本的xan
# 加拿大
index = -1
for i in range(len(stockObj['can'])):
    if stockObj['can'][i]['name'] == 'Xanax':
        index = i
if index != -1:
    del stockObj['can'][index]
# 英国
index = -1
for i in range(len(stockObj['uni'])):
    if stockObj['uni'][i]['name'] == 'Xanax':
        index = i
if index != -1:
    del stockObj['uni'][index]
# 日本
index = -1
for i in range(len(stockObj['jap'])):
    if stockObj['jap'][i]['name'] == 'Xanax':
        index = i
if index != -1:
    del stockObj['jap'][index]

# 读取库存历史
stockHistObj = {}
if HISTORY_EXIST:
    print('历史文件存在')
    with open(HISTORY_FILE, 'r') as load_f:
        stockHistObj = json.load(load_f)
else:
    stockHistObj['json'] = []
stockHistObj['json'].append({str(int(datetime.now().timestamp())): stockObj})
if len(stockHistObj['json']) > 100:
    del stockHistObj['json'][0]
with open(HISTORY_FILE, 'w') as f:
    json.dump(stockHistObj, f)
hist_arr = stockHistObj['json']


# 用于写入文件的列表
seq = ["更新时间: %s\n" % UPDATE_TIME_STR]
for dest in DEST_ARR:
    seq_str = '%s  ' % DEST_TRANS[dest]
    for i in range(len(stockObj[dest])):
        seq_str += '%s%s' % (ITEM_TRANS[stockObj[dest][i]['name']], stockObj[dest][i]['quantity'])
        if i != len(stockObj[dest]) - 1:
            seq_str += ' '
    seq_str += '\n'
    seq.append(seq_str)
# 写入文件
fo = open(file="stock.txt", mode="w", encoding="utf-8")
fo.writelines(seq)
fo.close()

print("花偶库存更新完成")
