// ==UserScript==
// @name         圣诞小镇助手(Christmas Town Helper)
// @namespace    hardy.ct.helper
// @version      3.0.2
// @description  【Christmas Town Helper@2.3.3】修改而来，增加了PDA支持、国内可用的物品价值查询、简单的汉化，希望为国内TC玩家带来更好的体验
// @author       Hardy [2131687] WOOHOO[2687093]
// @match        https://www.torn.com/christmas_town.php*
// @grant        GM_setClipboard
// @grant        GM_addStyle
// @grant        unsafeWindow
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_xmlhttpRequest
// @connect      wanzixx.stream
// ==/UserScript==

(function () {
    'use strict';
    if (window.xmas_torn_helper_flag) return;
    window.xmas_torn_helper_flag = true;

    let func = function (GM_addStyle, unsafeWindow, GM_getValue, GM_setValue, GM_xmlhttpRequest) {
        'use strict';

        /**
         * 原始脚本
         */
        let version = "2.3.3";
        // Thanks to xedx for Dark Mode support
        // Thanks Kafia for beep effect
        //Thanks to Ahab and Helcostr for the list of words and all the help.
        let listofWords = ["elf", "eve", "fir", "ham", "icy", "ivy", "joy", "pie", "toy", "gift", "gold", "list", "love", "nice", "sled", "star", "wish", "wrap", "xmas", "yule", "angel", "bells", "cider", "elves", "goose", "holly", "jesus", "merry", "myrrh", "party", "skate", "visit", "candle", "creche", "cookie", "eggnog", "family", "frosty", "icicle", "joyful", "manger", "season", "spirit", "tinsel", "turkey", "unwrap", "wonder", "winter", "wreath", "charity", "chimney", "festive", "holiday", "krampus", "mittens", "naughty", "package", "pageant", "rejoice", "rudolph", "scrooge", "snowman", "sweater", "tidings", "firewood", "nativity", "reindeer", "shopping", "snowball", "stocking", "toboggan", "trimming", "vacation", "wise men", "workshop", "yuletide", "chestnuts", "christmas", "fruitcake", "greetings", "mince pie", "mistletoe", "ornaments", "snowflake", "tradition", "candy cane", "decoration", "ice skates", "jack frost", "north pole", "nutcracker", "saint nick", "yule log", "card", "jolly", "hope", "scarf", "candy", "sleigh", "parade", "snowy", "wassail", "blizzard", "noel", "partridge", "give", "carols", "tree", "fireplace", "socks", "lights", "kings", "goodwill", "sugarplum", "bonus", "coal", "snow", "happy", "presents", "pinecone"];
        let hideDrn = true;
        let settings = { "count": 0, "spawn": 0, "speed": 0 };
        let lastSoundChirp;
        let mobile = false;
        initiate();
        let chirp = new Audio("https://www.torn.com/js/chat/sounds/Chirp_1.mp3");
        var hangmanArray = [];
        var hangmanCharactersArray = [];
        var wordFixerStart = false;
        var typeGameStart = false;
        var hangmanStart = false;
        let typoCD;
        window.addEventListener("hashchange", addBox);
        let original_fetch = unsafeWindow.fetch;
        let new_fetch = async (url, init) => {
            let response = await original_fetch(url, init)
            let respo = response.clone();
            respo.json().then((data) => {
                if (url.includes("christmas_town.php")) {
                    if (init.body) {
                        var body = JSON.parse(init.body);
                    }
                    if (url.includes("q=move") || url.includes("q=initMap")) {
                        if (url.includes("q=move")) {
                            if (wordFixerStart || hangmanStart || typeGameStart) {
                                wordFixerStart = false;
                                hangmanStart = false;
                                typeGameStart = false;
                                clearInterval(typoCD);
                                stopGame();
                            }
                        }
                        if (data.mapData) {
                            if (data.mapData.inventory && (settings.spawn === 1 || settings.speed === 1)) {
                                let obj = {};
                                obj.modifier = 0;
                                obj.speedModifier = 0;
                                for (const ornament of data.mapData.inventory) {
                                    if (ornament.category == "ornaments") {
                                        if (ornament.modifierType == 'itemSpawn') {
                                            obj.modifier += ornament.modifier;
                                        } else if (ornament.modifierType == 'speed') {
                                            obj.speedModifier += ornament.modifier;
                                        } else {
                                            console.debug('CT: Unknown ornament modifier "' + ornament.modifierType + '"');
                                        }
                                    }
                                }
                                GM_setValue("spawn", obj.modifier);
                                GM_setValue("speed", obj.speedModifier);
                                setTimeout(updateSpawnRate, 3000);
                                settings.spawn = 0;
                                settings.speed = 0;
                            }
                            if (data.mapData.items) {
                                let items = data.mapData.items;
                                if (items.length > 0) {
                                    settings.count = 1;
                                    let itemArray = [];
                                    let chestArray = [];
                                    for (const item of items) {
                                        let image = item.image.url;
                                        let position = item.position;
                                        let info = ctHelperGetInfo(image);
                                        if (info.type == "chests" || info.type == "combinationChest") {
                                            chestArray.push([info.name, position.x, position.y, info.index]);
                                            if (isChecked('sound_notif_helper', 2)) {
                                                beep();
                                            }
                                        } else {
                                            itemArray.push([info.name, position.x, position.y]);
                                            if (isChecked('sound_notif_helper', 2)) {
                                                beep();
                                            }
                                        }
                                    }
                                    chestArray.sort(function (a, b) {
                                        return a[3] - b[3];
                                    });
                                    ctHelperChangeHTML(itemArray, "hardyNearbyItems");
                                    ctHelperChangeHTML(chestArray, "hardyNearbyChests");
                                } else {
                                    if (settings.count == 1) {
                                        document.querySelector(".hardyNearbyChests").innerHTML = '<label>附近宝箱(0)</label><div class="content"></div>';
                                        document.querySelector(".hardyNearbyItems").innerHTML = '<label>附近物品(0)</label><div class="content"></div>';
                                        settings.count = 0;
                                    }
                                }
                            }
                            if (data.mapData.users) {
                                let users = data.mapData.users;
                                if (users.length > 0) {
                                    checkForNPC();
                                }
                            }
                            if (data.mapData && data.mapData.trigger && data.mapData.trigger.item) {
                                let trigger = data.mapData.trigger;
                                settings.spawn = 1;
                                settings.speed = 1;
                                if (trigger.message.includes("You find")) {
                                    let itemUrl = trigger.item.image.url;
                                    let reg = /\/images\/items\/([0-9]+)\/large\.png/g;
                                    if (reg.test(itemUrl)) {
                                        let itemId = itemUrl.split("/")[3];
                                        let savedData = getSaveData();
                                        if (savedData.items[itemId]) {
                                            savedData.items[itemId] += 1;
                                        } else {
                                            savedData.items[itemId] = 1;
                                        }
                                        localStorage.setItem("ctHelperFound", JSON.stringify(savedData));
                                    }
                                }
                            }
                        }
                    } else if (url.includes("q=miniGameAction")) {
                        if (body && body.action && body.action === "complete" && typeGameStart) {
                            typeGameStart = false;
                            clearInterval(typoCD);
                            stopGame();
                        }
                        if (wordFixerStart) {
                            if (data.finished) {
                                stopGame();
                                wordFixerStart = false;
                            } else {
                                if (data.progress && data.progress.word) {
                                    wordSolver(data.progress.word);
                                }
                            }
                        } else if (hangmanStart) {
                            if (data.mistakes === 6 || data.message.startsWith("Congratulations")) {
                                hangmanStart = false;
                                stopGame();
                            } else {
                                hangmanCharactersArray.push(body.result.character.toUpperCase());
                                if (data.positions.length === 0) {
                                    let array = [];
                                    let letter = body.result.character.toUpperCase();
                                    for (const word of hangmanArray) {
                                        if (word.indexOf(letter) === -1) {
                                            array.push(word);
                                        }
                                    }
                                    hangmanArray = array;
                                    hangmanMain();
                                } else {

                                    let array = [];
                                    let letter = body.result.character.toUpperCase();
                                    let positions = data.positions;
                                    let length = positions.length;
                                    for (const word of hangmanArray) {
                                        var index = 0;
                                        for (const position of positions) {
                                            if (word[position] === letter) {
                                                index += 1;
                                            }
                                        }
                                        if (index === length && countLetter(word, letter) == length) {
                                            array.push(word);
                                        }
                                    }
                                    hangmanArray = getUnique(array);
                                    hangmanMain();
                                }
                            }
                        } else if (typeGameStart) {
                            console.log("nothing");
                        }
                        if (body && body.action && body.action === "start") {
                            if (body.gameType) {
                                let gameType = body.gameType;
                                if (gameType == "gameWordFixer" && isChecked('word_fixer_helper', 2)) {
                                    wordFixerStart = true;
                                    startGame("Word Fixer");
                                    wordSolver(data.progress.word);

                                } else if (gameType === "gameHangman" && isChecked('hangman_helper', 2)) {
                                    hangmanStart = true;
                                    startGame("Hangman");
                                    hangmanArray = [];
                                    hangmanCharactersArray = [];
                                    let words = data.progress.words;
                                    if (words.length > 1) {
                                        hangmanStartingFunction(words[0], words[1]);
                                    } else {
                                        hangmanStartingFunction(words[0], 0);
                                    }
                                } else if (gameType === "gameTypocalypse" && isChecked('typocalypsehelper', 2)) {

                                    if (!typeGameStart) {
                                        typeGameStart = true;
                                        startGame("Typocalypse Helper");
                                        document.querySelector(".hardyGameBoxContent").addEventListener("click", (e) => {
                                            let target = e.target;
                                            if (target.className === "hardyCTTypoAnswer") {
                                                let input = document.querySelector("div[class^='game'] div[class^='board'] input");
                                                if (input) {
                                                    input.value = target.getAttribute("hardy");//the answer that has to be typed
                                                    let event = new Event('input', { bubbles: true });
                                                    let tracker = input._valueTracker;
                                                    if (tracker) {
                                                        tracker.setValue('');
                                                    }
                                                    input.dispatchEvent(event);
                                                }
                                            }
                                        });
                                        startTypo();
                                    }
                                }
                            }
                        }
                    }
                    if (data.prizes) {
                        settings.spawn = 1;
                        settings.speed = 1;
                        if (data.prizes.length > 0) {
                            let savedData = getSaveData();
                            for (const prize of data.prizes) {
                                if (prize.category === "tornItems") {
                                    let itemId = prize.type;
                                    if (savedData.items[itemId]) {
                                        savedData.items[itemId] += 1;
                                    } else {
                                        savedData.items[itemId] = 1;
                                    }
                                }
                            }
                            localStorage.setItem("ctHelperFound", JSON.stringify(savedData));
                        }
                    }
                    if (data.mapData && data.mapData.cellEvent && data.mapData.cellEvent.prizes) {
                        let prizes = data.mapData.cellEvent.prizes;
                        settings.spawn = 1;
                        settings.speed = 1;
                        if (prizes.length > 0) {
                            let savedData = getSaveData();
                            for (const prize of prizes) {
                                if (prize.category === "tornItems") {
                                    let itemId = prize.type;
                                    if (savedData.items[itemId]) {
                                        savedData.items[itemId] += 1;
                                    } else {
                                        savedData.items[itemId] = 1;
                                    }
                                }
                            }
                            localStorage.setItem("ctHelperFound", JSON.stringify(savedData));
                        }
                    }

                }
            });
            return response;
        };
        unsafeWindow.fetch = new_fetch;
        window.fetch = new_fetch;
        fetch = new_fetch;

        function addBox() {
            if (!document.querySelector(".hardyCTBox")) {
                if (document.querySelector("#christmastownroot div[class^='appCTContainer']")) {
                    let newBox = document.createElement("div");
                    let pcHTML =
                        `<div class="hardyCTHeader hardyCTShadow">Christmas Town Helper</div>
                <div class="hardyCTContent hardyCTShadow"><br>
                    <div style="display: flex; align-items: flex-start;">
                        <div class="hardyNearbyItems""><label>附近物品(0)</label>
                            <div class="content"></div>
                        </div>
                        <div style="align-self: center; width: 70%;">
                	        <div style="margin-bottom: 20px;">
                                <p><a href="#/cthelper" class="ctRecordLink">设置</a><br></p>
                            </div>
                	        <p class="ctHelperSpawnRate ctHelperSuccess">&nbsp;</p>
                            <p class="ctHelperSpeedRate ctHelperSuccess">&nbsp;</p>
                        </div>
                        <div class="hardyNearbyChests""><label>附近宝箱(0)</label>
                            <div class="content"></div>
                        </div>
                    </div>
                </div>`;
                    let mobileHTML = '<div class="hardyCTHeader hardyCTShadow">Christmas Town Helper</div><div class="hardyCTContent hardyCTShadow"><br><a href="#/cthelper" class="ctRecordLink">Settings</a><br><br><p class="ctHelperSpawnRate ctHelperSuccess">&nbsp;</p><p class="ctHelperSpeedRate ctHelperSuccess">&nbsp;</p><div class="hardyNearbyItems" style="float: left;"><label>附近物品(0)</label><div class="content"></div></div><div class="hardyNearbyChests" style="float:right;"><label>附近宝箱(0)</label><div class="content"></div></div></div>';
                    if (mobile) {
                        newBox.innerHTML = mobileHTML
                    } else {
                        newBox.innerHTML = pcHTML;
                    }
                    newBox.className = 'hardyCTBox';
                    let doc = document.querySelector("#christmastownroot div[class^='appCTContainer']");
                    doc.insertBefore(newBox, doc.firstChild.nextSibling);
                    if (timedFunction) {
                        clearInterval(timedFunction);
                    }
                    settings.spawn = 1;
                    settings.speed = 1;
                } else {
                    var timedFunction = setInterval(addBox, 1000);
                }
            }
            let pageUrl = window.location.href;
            if (pageUrl.includes("mapeditor") || pageUrl.includes("parametereditor") || pageUrl.includes("mymaps")) {
                document.querySelector(".hardyCTBox").style.display = "none";
                let node = document.querySelector(".hardyCTBox2");
                if (node) {
                    node.style.display = "none";
                }
            } else if (pageUrl.includes("cthelper")) {
                document.querySelector(".hardyCTBox").style.display = "none";
                createTable();
                let node = document.querySelector(".hardyCTBox2");
                if (node) {
                    node.style.display = "block";
                }
                console.log(node)
            } else {
                let box = document.querySelector(".hardyCTBox")
                if (box) {
                    box.style.display = "block";
                }
                let node = document.querySelector(".hardyCTBox2");
                if (node) {
                    node.style.display = "none";
                }
            }
            hideDoctorn();
        }

        function checkForNPC() {
            let npcList = document.querySelectorAll(".ct-user.npc");
            if (npcList.length > 0) {
                for (const npc of npcList) {
                    if (npc.querySelector("svg").getAttribute("fill").toUpperCase() === "#FA5B27") {
                        npc.setAttribute("npcType", "santa");
                    } else {
                        npc.setAttribute("npcType", "other");
                    }
                }
            }
        }

        function ctHelperGetInfo(link) {
            let obj = {};
            obj.type = "item";
            let array = ["/keys/", "/chests/", "/combinationChest/"];
            for (const category of array) {
                if (link.indexOf(category) !== -1) {
                    obj.type = category.replace(/\//g, "");
                }
            }
            if (obj.type === "keys") {
                if (link.includes("bronze")) {
                    obj.name = "铜钥匙";
                } else if (link.includes("gold")) {
                    obj.name = "金钥匙";
                } else if (link.includes("silver")) {
                    obj.name = "银钥匙";
                }
            } else if (obj.type == "chests") {
                if (link.includes("1.gif")) {
                    obj.name = "金宝箱";
                    obj.index = 0;
                } else if (link.includes("2.gif")) {
                    obj.name = "银宝箱";
                    obj.index = 1;
                } else if (link.includes("3.gif")) {
                    obj.name = "铜宝箱";
                    obj.index = 3;
                }
            } else if (obj.type == "combinationChest") {
                obj.name = "密码箱";
                obj.index = 2;
            } else if (obj.type == "item") {
                obj.name = "神秘礼物";
            }
            return obj;
        }

        function ctHelperChangeHTML(array, selector) {
            let length = array.length;
            if (length > 0) {
                let newArray = [];
                for (const element of array) {
                    newArray.push(`<p>${ element[0] } @ ${ element[1] }, ${ element[2] }&nbsp;</p>`);
                }
                if (selector == "hardyNearbyItems") {
                    document.querySelector("." + selector).innerHTML = `<label>附近物品(${ length })</label><div class="content">${ newArray.join("") }</div>`;
                } else {
                    document.querySelector("." + selector).innerHTML = `<label>附近宝箱(${ length })</label><div class="content">${ newArray.join("") }</div>`;
                }
            } else {
                if (selector == "hardyNearbyItems") {
                    document.querySelector("." + selector).innerHTML = `<label>附近物品(0)</label><div class="content"></div>`;
                } else {
                    document.querySelector("." + selector).innerHTML = `<label>附近宝箱(0)</label><div class="content"></div>`;
                }
            }
        }

        function applyCSS() {
            if (isChecked('santa_clawz_helper', 2)) {
                GM_addStyle(`[class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADuy'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADu4'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADms'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADjr'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADj4'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADSx'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADPy'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADOx'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADLx'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADKq'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAADCS'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAAD03'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAACun'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAACnk'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAACmg'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAACSe'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAACGL'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAAC1U'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAAC0w'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAAByX'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAABsX'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEUAAAB7g'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAACzVBMVEUAAACTM'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC9FBMVEUAAADlw'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC9FBMVEUAAAD67'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC9FBMVEUAAACnR'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAADy2'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAADrw'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAADlt'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAADl5'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAADgp'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAADgo'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAADak'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAADKx'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAADJn'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAAD9+'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAAD57'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAAD16'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAAClR'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAACdN'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAAC0R'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC91BMVEUAAABxW'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC8VBMVEUAAADKe'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC8VBMVEUAAADKc'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC7lBMVEUAAACXN'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADy1'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADw7'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADvz'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADu6'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADsx'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADrx'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADr1'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADpv'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADnt'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADe6'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADc2'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADVg'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADOv'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADLh'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADFV'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAADEx'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAAD68'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAAD28'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAACup'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAACQN'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAAC0p'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAAC0l'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAABsX'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAABpe'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAAB2V'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC/VBMVEUAAAB/e'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAADy1'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAADt5'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAADj4'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAADf0'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAADcr'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAADal'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAADLv'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAADJx'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAAD9+'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAACnl'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAACWM'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAACMh'], [class^='cell'] img[src^='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC+lBMVEUAAAC+Y'] {opacity: .2;}`);
            }
            if (isChecked('snowball_shooter_helper', 2)) {
                GM_addStyle(`[class^='moving-block'] [style*='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABHwAAAB5'], [class^='moving-block'] [style*='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABGAAAABu'], [class^='moving-block'] [style*='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABG4AAABm'], [class^='moving-block'] [style*='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABFIAAAB5'], [class^='moving-block'] [style*='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAwIAAAB6'], [class^='moving-block'] [style*='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA9QAAABu'], [class^='moving-block'] [style*='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA44AAAB4'], [class^='moving-block'] [style*='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA0gAAABm'], [class^='moving-block'] [style*='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA+IAAABm'] {opacity: .2;}`);
            }
            if (isChecked('christmas_wreath_helper', 2)) {
                GM_addStyle(`img[alt='christmas wreath'] {display: none;}`);
            }
            if (isChecked("accessibility_helper", 2)) {
                GM_addStyle('@keyframes pulse {0% {box-shadow: 0 0 0 60px;}50% {box-shadow: 0 0 0 60px;}100% {box-shadow: 0 0 0 60px;}}');
                if (isChecked("santa_helper", 2)) {
                    GM_addStyle(`div[npcType="santa"] { border-radius: 50%; color:  #a80a0a6e; animation: pulse 2s ease-out infinite; }`);
                }
                if (isChecked("item_helper", 2)) {
                    GM_addStyle(`.items-layer .ct-item img  { color: rgba(145, 135, 77, .66); border-radius: 50% ;animation: pulse 2s ease-in-out infinite; }`);
                }
                if (isChecked("npc_helper", 2)) {
                    GM_addStyle(`div[npcType="other"] { border-radius: 50%; color: #0051ff87; animation: pulse 2s ease-out infinite;}`);
                }
            } else {
                GM_addStyle('@keyframes pulse {0% {box-shadow: 0 0 0 0px;}50% {box-shadow: 0 0 0 60px;}100% {box-shadow: 0 0 0 0px;}}');
                if (isChecked("santa_helper", 2)) {
                    GM_addStyle(`div[npcType="santa"] { border-radius: 50%; color: #ff00006e; animation: pulse 2s ease-out infinite; }`);
                }
                if (isChecked("item_helper", 2)) {
                    GM_addStyle(`.items-layer .ct-item img  { color:rgba(244, 226, 130, .66); border-radius: 50% ;animation: pulse 2s ease-in-out infinite; }`);
                }
                if (isChecked("npc_helper", 2)) {
                    GM_addStyle(`div[npcType="other"] { border-radius: 50%; color: #0051ff87; animation: pulse 2s ease-out infinite;}`);
                }
            }
        }

        function sortWord(word) {
            let array = word.toUpperCase().split("");
            array.sort();
            return array.join("");
        }

        function wordSolver(jumbled) {
            var wordSolution = 'whereiscrimes2.0';
            for (const word of listofWords) {
                if (sortWord(word) === sortWord(jumbled)) {
                    wordSolution = word.toUpperCase();
                }
            }
            if (wordSolution === 'whereiscrimes2.0') {
                updateGame('<label class="ctHelperError">Sorry! couldn\'t find the solution. ):</label>');
            } else {
                updateGame(`<label class="ctHelperSuccess">${ wordSolution }</label>`);
            }
        }

        function getPrices() {
            var last_update = GM_getValue('last');
            if (last_update === null || typeof last_update == "undefined") {
                last_update = 0;
            }
            if (Date.now() / 1000 - last_update > 14400) {
                GM_xmlhttpRequest({
                    method: 'GET',
                    timeout: 20000,
                    // 域名替换
                    url: 'https://sguc.wanzixx.stream/macros/s/AKfycbyRfg1Cx2Jm3IuCWASUu8czKeP3wm5jKsie4T4bxwZHzXTmPbaw4ybPRA/exec?key=getItems',
                    onload: function (e) {
                        try {
                            let data = JSON.parse(e.responseText);
                            if (data.items) {
                                let items = data.items;
                                let obj = {};
                                obj.items = {};
                                for (var pp = 0; pp < items.length; pp++) {
                                    let id = items[pp][0];
                                    obj.items[id] = {};
                                    obj.items[id].name = items[pp][1];
                                    obj.items[id].value = items[pp][2];
                                }
                                localStorage.setItem('ctHelperItemInfo', JSON.stringify(obj));
                                GM_setValue('last', Date.now() / 1000);
                                console.log("Price data received");
                            }
                        } catch (error) {
                            console.log("Error updating prices: " + error.message);
                        }
                    }
                });
            }
        }

        function getSaveData() {
            let savedFinds = localStorage.getItem("ctHelperFound");
            var saved;
            if (typeof savedFinds == "undefined" || savedFinds === null) {
                saved = {};
                saved.items = {};
            } else {
                saved = JSON.parse(savedFinds);
            }
            return saved;
        }

        function createTable() {
            if (!document.querySelector(".hardyCTBox2")) {
                let node = document.createElement("div");
                node.className = "hardyCTBox2";
                document.querySelector(".content-wrapper").appendChild(node);
                document.querySelector(".hardyCTBox2").addEventListener("click", (e) => {
                    if (e.target.id === "hardyctHelperSave") {
                        let checkboxes = document.querySelectorAll(".hardyCTHelperCheckbox");
                        for (const checkbox of checkboxes) {
                            if (checkbox.checked) {
                                GM_setValue(checkbox.id, "yes");
                            } else {
                                GM_setValue(checkbox.id, "no");
                            }
                        }
                        location.reload();
                    } else if (e.target.id == "hardyctHelperdelete") {
                        document.querySelector(".hardyCTtextBox").innerHTML = '<p>Are you sure you want to delete the finds data?</p><button id="hardyCTConfirmDelete">Yes</button><button id="hardyCTNoDelete">No</button>';
                    } else if (e.target.id == "hardyCTConfirmDelete") {
                        let obj = { "items": {} };
                        localStorage.setItem("ctHelperFound", JSON.stringify(obj));
                        document.querySelector(".hardyCTtextBox").innerHTML = '<label class="ctHelperSuccess"Data deleted!</label>';
                        document.querySelector(".hardyCTTable").innerHTML = '';
                    } else if (e.target.id == "hardyCTNoDelete") {
                        document.querySelector(".hardyCTtextBox").innerHTML = '';
                    }
                });
            }
            document.querySelector(".hardyCTBox2").innerHTML = '<div class="hardyCTHeader">Christmas Town Helper（圣诞小镇助手）</div><div class="hardyCTTableBox"><div class="hardyCTbuttonBox" style="margin-top: 8px;"><input type="checkbox" class="hardyCTHelperCheckbox" id="santa_helper"  value="yes"' + isChecked('santa_helper', 1) + '><label for="santa_helper">高亮圣诞老人</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="npc_helper"  value="yes"' + isChecked('npc_helper', 1) + '><label for="npc_helper">高亮其他 NPCs</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="typocalypsehelper" value="yes"' + isChecked("typocalypsehelper", 1) + '><label for="typocalypsehelper">打字游戏助手(Typoclypse)</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="item_helper"  value="yes"' + isChecked('item_helper', 1) + '><label for="item_helper">高亮宝箱和物品</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="christmas_wreath_helper"  value="yes"' + isChecked('christmas_wreath_helper', 1) + '><label for="christmas_wreath_helper">花环游戏助手(Christmas Wreath)</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="snowball_shooter_helper"  value="yes"' + isChecked('snowball_shooter_helper', 1) + '><label for="snowball_shooter_helper">雪球射手助手(Snowball Shooter)</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="santa_clawz_helper" value="yes"' + isChecked('santa_clawz_helper', 1) + '><label for="santa_clawz_helper">圣诞爪爪(Santa Clawz)</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="word_fixer_helper" value="yes"' + isChecked('word_fixer_helper', 1) + '><label for="word_fixer_helper">填字游戏(Word Fixer Helper)</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="hangman_helper" value="yes"' + isChecked('hangman_helper', 1) + '><label for="hangman_helper">倒吊人(Hangman Helper)</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="accessibility_helper"  value="yes"' + isChecked('accessibility_helper', 1) + '><label for="accessibility_helper">无障碍 (淡化高亮效果并取消闪烁，用于因颜色或闪烁不适的用户)</label><br><input type="checkbox" class="hardyCTHelperCheckbox" id="sound_notif_helper" value="yes"' + isChecked('sound_notif_helper', 1) + '><label for="sound_notif_helper">找到物品时声音提醒</label><br><a href="#/" class="ctRecordLink" style="display:inline;">返回</a><button id="hardyctHelperSave">保存</button><button id="hardyctHelperdelete">删除已找到</button></div><div class="hardyCTtextBox"></div><br><hr><br><div class="hardyCTTable" style="overflow-x:auto;"></div></div>';
            let itemData = localStorage.getItem("ctHelperItemInfo");
            var marketValueData;
            if (typeof itemData === "undefined" || itemData === null) {
                marketValueData = "ched";
            } else {
                marketValueData = JSON.parse(itemData);
            }
            if (marketValueData === "ched") {
                let hardyCTTableBox = document.querySelector(".hardyCTTableBox");
                hardyCTTableBox.innerHTML = '<label class="ctHelperError">尚未获取到价格数据，请稍后刷新页面重试。Unable to get data from the spreadsheet. Kindly refresh the page. Contact Father [2131687] if the problem persists</label>';
                let backBtn = document.createElement('button');
                backBtn.innerHTML = '返回';
                hardyCTTableBox.append(backBtn);
                backBtn.addEventListener('click', () => location.hash = '');
            } else {
                let savedData = getSaveData();
                let obj = { "items": {} };

                if (savedData == obj) {
                    document.querySelector(".hardyCTTableBox").innerHTML = '<label class="ctHelperError">You haven\'t found any items yet. Try again later!</label>';
                } else {
                    let calc = {};
                    calc.totalValue = 0;
                    calc.count = 0;
                    let tableArray = [];
                    let array = [];
                    for (var mp in savedData.items) {
                        let count = savedData.items[mp];
                        let item = marketValueData.items[mp];
                        let name = item.name;
                        let value = item.value;
                        let price = count * value
                        calc.count += parseInt(count);
                        calc.totalValue += parseInt(price);
                        array.push([mp, name, count, value, price]);
                    }
                    array.sort(function (a, b) {
                        return b[4] - a[4];
                    });
                    for (const row of array) {
                        tableArray.push(`<tr><td><img src="/images/items/${ row[0] }/medium.png", alt = "${ row[1] }"></td><td>${ row[1] }</td><td>${ row[2] }</td><td>$${ formatNumber(row[3]) }</td><td>$${ formatNumber(row[4]) }</td></tr>`);
                    }
                    document.querySelector(".hardyCTTable").innerHTML = '<table><tr><th>Image</th><th>Item Name</th><th>Amount</th><th>Price</th><th>Total</th></tr>' + tableArray.join("") + `</table><p>总价值: $${ formatNumber(calc.totalValue) }</p><p> 物品数: ${ calc.count }</p><p>平均价值: $${ formatNumber(Math.round(calc.totalValue / calc.count)) }</p>`;

                }
            }
        }

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }

        function isChecked(element, returnType) {
            let value = GM_getValue(element);
            if (typeof value == "undefined" || value === null || value === "no") {
                if (returnType == 1) {
                    return "";
                } else {
                    return false;
                }
            } else {
                if (returnType == 1) {
                    return " checked";
                } else {
                    return true;
                }
            }
        }

        function firstRun() {
            if (!isChecked("firstRun", 2)) {
                GM_setValue("christmas_wreath_helper", "yes");
                GM_setValue("typocalypsehelper", "yes");
                GM_setValue("snowball_shooter_helper", "yes");
                GM_setValue("santa_clawz_helper", "yes");
                GM_setValue("word_fixer_helper", "yes");
                GM_setValue("hangman_helper", "yes");
                GM_setValue("santa_helper", "yes");
                GM_setValue("npc_helper", "yes");
                GM_setValue("item_helper", "yes");
                GM_setValue("firstRun", "blah");
                GM_setValue("version", version);
                GM_setValue("month", Date.now());
            }
        }

        function deleteOldData() {
            let now = new Date(Date.now());
            if (now.getMonth == 11) {
                if (new Date(GM_getValue("month")).getFullYear() != now.getFullYear()) {
                    let obj = { "items": {} };
                    localStorage.setItem("ctHelperFound", JSON.stringify(obj));
                    GM_setValue("month", Date.now());
                }
            }
        }

        function stopGame() {
            let node = document.querySelector(".ctHelperGameBox");
            if (node) {
                node.remove();
            }
        }

        function startGame(gameName) {
            if (!document.querySelector(".ctHelperGameBox")) {
                let node = document.createElement("div");
                node.className = "ctHelperGameBox";
                let reference = document.querySelector(".ct-wrap");
                reference.parentNode.insertBefore(node, reference);
            }
            document.querySelector(".ctHelperGameBox").innerHTML = '<div class="hardyCTHeader">' + gameName + ' Helper</div><div class="hardyGameBoxContent"></div>'
        }

        function updateGame(content) {
            let node = document.querySelector(".hardyGameBoxContent");
            if (node) {
                node.innerHTML = content;
            }
        }

        function hangmanStartingFunction(letters1, letters2) {
            if (letters2 == 0) {
                let totalLength = letters1;
                for (const word of listofWords) {
                    if (word.length === letters1 && !word.split(" ")[1]) {
                        hangmanArray.push(word.toUpperCase());
                    }
                }
            } else {
                let totalLength = letters1 + letters2 + 1;
                for (const word of listofWords) {
                    if (word.length === totalLength) {
                        if (word.split(" ")[1] && word.split(" ")[0].length == letters1 && word.split(" ")[1].length == letters2) {
                            hangmanArray.push(word.toUpperCase());
                        }
                    }
                }
            }
            hangmanMain();
        }

        function hangmanMain() {
            let array = [];
            let obj = {};
            let html1 = '<p style="font-weight: bold; font-size: 16px; margin: 8px; text-align: center;">猜测答案</p><p class="ctHelperSuccess">';
            for (const word of hangmanArray) {
                array.push(word.toUpperCase());
                let letters = getUniqueLetter(word.replace(/\s/g, "").split(""));
                for (const letter of letters) {
                    if (obj[letter]) {
                        obj[letter] += 1;
                    } else {
                        obj[letter] = 1;
                    }
                }
            }
            let sortable = [];
            for (const key in obj) {
                sortable.push([key, obj[key], String(+((obj[key] / hangmanArray.length) * 100).toFixed(2)) + "% 概率"]);
            }
            sortable.sort(function (a, b) {
                return b[1] - a[1];
            });
            let lettersArray = [];
            let limit = {};
            limit.limit = 5;
            let length = sortable.length;
            if (length > 5) {
                limit.limit = 5;
            } else {
                limit.limit = length;
            }
            for (var mkl = 0; mkl < limit.limit; mkl++) {
                let letter = sortable[mkl];
                lettersArray.push(`${ letter[0].toUpperCase() } <label class="helcostrDoesntLikeGreenCommas">(${ letter[2] })</label>`);
            }
            updateGame(html1 + array.join('<label class="helcostrDoesntLikeGreenCommas">, </label>') + '</p><p style="font-weight: bold; font-size: 16px; margin: 8px; text-align: center;">猜测字母</p><p class="ctHelperSuccess">' + lettersArray.join('<label class="helcostrDoesntLikeGreenCommas">, </label>') + '</p>');
        }

        function getUnique(array) {
            let newArray = [];
            for (var mn = 0; mn < array.length; mn++) {
                if (newArray.indexOf(array[mn]) == -1) {
                    newArray.push(array[mn]);
                }
            }
            return newArray;
        }

        function countLetter(string, letter) {
            let array = string.split("");
            let obj = {};
            obj.count = 0;
            for (const a of array) {
                if (a === letter) {
                    obj.count += 1;
                }
            }
            return obj.count;
        }

        function hideDoctorn() {
            if (hideDrn) {
                GM_addStyle(`.doctorn-widget {display: none;}`);
            }
        }

        function getUniqueLetter(argArray) {
            let newArray = [];
            let array = getUnique(argArray);
            for (const letter of array) {
                if (hangmanCharactersArray.indexOf(letter) === -1) {
                    newArray.push(letter);
                }
            }
            return newArray;
        }

        function updateSpawnRate() {
            let spawn = GM_getValue("spawn");
            let speed = GM_getValue("speed");
            if (typeof spawn === "undefined" || spawn === null) {
                settings.spawn = 1;
            } else {
                document.querySelector(".ctHelperSpawnRate").innerHTML = `掉落加成: ${ spawn }%.`;
            }
            if (typeof speed === "undefined" || speed === null) {
                settings.spawn = 1;
            } else {
                document.querySelector(".ctHelperSpeedRate").innerHTML = `速度加成: ${ speed }%.`;
            }
        }

        function checkVersion() {
            let current_version = GM_getValue('version');
            if (current_version === null || typeof current_version == "undefined" || current_version != version) {
                enableNewFeatures();
                GM_setValue('version', version);
            }
        }

        function enableNewFeatures() {
            console.log("No feature to be enabled by default");
        }

        function initiate() {
            firstRun();
            let innerWidth = window.innerWidth;
            mobile = innerWidth <= 600;
            addBox();
            checkVersion();
            applyCSS();
            getPrices()
            deleteOldData();
            let lastSound = GM_getValue("lastSound");
            if (typeof lastSound === "undefined" || lastSound === null) {
                GM_setValue("lastSound", "0");
                lastSoundChirp = 0;
            } else {
                lastSoundChirp = lastSound;
            }
        }

        function startTypo() {
            typoCD = setInterval(() => {
                let boxes = document.querySelectorAll("div[class^='game'] div[class^='board'] div[class^='gift']");
                let length = boxes.length;
                let array = [];
                if (length > 0) {
                    for (const gift of boxes) {
                        let phrase = gift.innerText;
                        array.push(`<button class="hardyCTTypoAnswer" hardy="${ phrase }">${ phrase }</button>`);
                    }
                    array.reverse();
                }
                updateGame(array.join(""));
            }, 500);
        }

        function beep() {
            let now = parseInt(Date.now() / 1000);
            let diff = now - lastSoundChirp;
            if (diff >= 60) {
                GM_setValue("lastSound", now);
                lastSoundChirp = now;
                chirp.play();
            }
        }

        GM_addStyle(`
#hardyctHelperSave {background-color: #2da651;}
#hardyctHelperSave:hover {background-color: #2da651c4;}
#hardyctHelperdelete {background-color: #f03b10;}
#hardyctHelperdelete:hover {background-color: #f03b10bd;}
.ctRecordLink:hover {background-color: #53a3d7;}
.ct-user-wrap .user-map:before {display:none;}
body.dark-mode .hardyCTShadow { box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); -moz-box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); -webkit-box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64);}
body.dark-mode .hardyCTHeader { background-color: #454545; border-radius: 0.5em 0.5em 0 0; text-align: center; text-indent: 0.5em; font-size: 16px; color: #b5bbbb; padding: 5px 0px 5px 0px;}
body:not(.dark-mode) .hardyCTHeader { background-color: #0d0d0d; border: 2px solid #000; border-radius: 0.5em 0.5em 0 0; text-align: center; text-indent: 0.5em; font-size: 16px; color: #b5bbbb; padding: 5px 0px 5px 0px;}
body:not(.dark-mode) .hardyCTContent, body:not(.dark-mode) .hardyCTTableBox, body:not(.dark-mode) .hardyGameBoxContent { border-radius: 0px 0px 8px 8px; background-color: rgb(242, 242, 242); color: black; box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); -moz-box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); -webkit-box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); padding: 5px 8px; overflow: auto; }
.hardyCTBox, .hardyCTBox2, .ctHelperGameBox {margin-bottom: 18px;}
.hardyCTBox2 table { color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border: 2px #808080 solid; margin: 20px; }
.hardyCTBox2 td, th { border: 1px solid rgba(0, 0, 0, .55); height: 30px; transition: all 0.3s; }
.hardyCTBox2 th { background: #868282; font-weight: bold; text-align: center; }
.hardyCTBox2 td { background: #c6c4c4; text-align: center;}
.hardyCTBox2 tr:nth-child(even) td { background: #F1F1F1; }
.hardyCTBox2 tr:nth-child(odd) td { background: #c6c4c4; }
.hardyCTBox2 tr td:hover { background: #666; color: #FFF; }
.hardyCTTable { padding: 5px; }
table:not([cellpadding]) td {vertical-align: middle;}
.hardyCTHelperCheckbox { margin: 8px; margin-left: 18px; }
.hardyCTtextBox { text-align: center; }
.hardyCTtextBox button { background-color: rgba(240, 60, 17, .91); }
.hardyCTBox2 button { padding: 8px 5px 8px 5px; border-radius: 4px; color: white; margin: 9px; font-weight: bold;}
.ctHelperError { color: #ff000091; margin: 5px; }
.body:not(.dark-mode) .ctHelperSuccess { color: #38a333; margin: 5px; font-weight: bold; font-size: 16px; line-height: 1.3;}
body.dark-mode .ctHelperSuccess { color: #b5bbbb; margin: 5px; font-weight: bold; font-size: 16px; line-height: 1.3;}
.hardyCTBox2 p { margin: 15px; font-weight: bold; font-family: Helvetica; }
.hardyNearbyItems label, .hardyNearbyChests label { font-weight: bold; }
.hardyCTBox p { margin-top: 9px; font-family: Helvetica; }
body:not(.dark-mode) .helcostrDoesntLikeGreenCommas {color: #333;}
body.dark-mode .helcostrDoesntLikeGreenCommas {color: #919191;}
.ctHelperSpawnRate, .ctHelperSpeedRate {text-align: center; font-size: 14px}
label[for='accessibility_helper'] {line-height: 1.6; margin-left: 8px;}
.hardyCTTypoAnswer {padding: 5px 6px; background-color: #4a9f33; color: white; margin: 5px; border-radius: 5px;}
.hardyCTTypoAnswer:hover, .hardyCTTypoAnswer:focus {color: white;}
` + (mobile ? ` .ctRecordLink { margin: 18px 9px 18px 18px; padding:10px 5px 10px 5px; background-color: #4294f2; border-radius: 4px; color: #fdfcfc; text-decoration: none; font-weight: bold;} body.dark-mode .hardyCTContent, body.dark-mode .hardyCTTableBox, body.dark-mode .hardyGameBoxContent { border-radius: 0px 0px 8px 8px; background-color: #27292d; color: #b5bbbb; box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); -moz-box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); -webkit-box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); padding: 5px 8px; overflow: auto; } .hardyNearbyItems, .hardyNearbyChests { padding: 4px; display: inline; } .hardyCTContent .content {overflow-y: auto; height: 60px; margin-right: 3px; margin-top: 3px;}` : ` .ctRecordLink { margin: 18px 9px 18px 180px; padding:10px 15px 10px 15px; background-color: #4294f2; border-radius: 4px; color: #fdfcfc; text-decoration: none; font-weight: bold;} body.dark-mode .hardyCTContent, body.dark-mode .hardyCTTableBox, body.dark-mode .hardyGameBoxContent {  height: 140px; border-radius: 0px 0px 8px 8px; background-color: #27292d; color: #b5bbbb; box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); -moz-box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); -webkit-box-shadow: 0px 4px 9px 3px rgba(119, 119, 119, 0.64); padding: 5px 8px; overflow: auto; } .hardyNearbyItems, .hardyNearbyChests { padding: 4px; display: inline; margin-top: 0px; width: 30%; } .hardyCTContent .content {overflow-y: auto; height: 100px; margin-right: 3px; margin-top: 3px;}`));
    };

    /**
     * 兼容手机
     */
    try {
        !!GM_setClipboard;
        func(GM_addStyle, unsafeWindow, GM_getValue, GM_setValue, GM_xmlhttpRequest);
    } catch {
        let GM_addStyle = (v) => {
                const wh_gStyle = document.querySelector('style#wh-gStyle');
                if (wh_gStyle) {
                    wh_gStyle.innerHTML += v;
                } else {
                    const wh_gStyle = document.createElement("style");
                    wh_gStyle.id = 'wh-gStyle';
                    wh_gStyle.innerHTML = v;
                    document.head.append(wh_gStyle);
                }
            },
            GM_getValue = (k) => {
                let string = localStorage.getItem('wh_fake_GM_DB') || '{}';
                let parsed;
                try {
                    parsed = JSON.parse(string);
                } catch {
                    parsed = {};
                }
                if (parsed && parsed[k])
                    return parsed[k];
                else
                    return undefined;
            },
            GM_setValue = (k, v) => {
                let string = localStorage.getItem('wh_fake_GM_DB') || '{}';
                console.log(new Date().toLocaleString(), '[INFO] string', string);
                let parsed;
                try {
                    parsed = JSON.parse(string);
                } catch {
                    parsed = {};
                }
                parsed[k] = v;
                localStorage.setItem('wh_fake_GM_DB', JSON.stringify(parsed));
            },
            GM_xmlhttpRequest = async (args) => {
                try {
                    let res = await PDA_httpGet('https://sguc.wanzixx.stream/macros/s/AKfycbyRfg1Cx2Jm3IuCWASUu8czKeP3wm5jKsie4T4bxwZHzXTmPbaw4ybPRA/exec?key=getItems');
                    if (args.onload) args.onload(res);
                } catch {
                    const str = `{"items":[[4555555,"Points",45000],[1,"Hammer",52],[2,"Baseball Bat",127],[3,"Crowbar",196],[4,"Knuckle Dusters",499],[5,"Pen Knife",716],[6,"Kitchen Knife",1907],[7,"Dagger",1467],[8,"Axe",2669],[9,"Scimitar",5440],[10,"Chainsaw",11340],[11,"Samurai Sword",55184],[12,"Glock 17",197],[13,"Raven MP25",308],[14,"Ruger 22/45",625],[15,"Beretta M9",1184],[16,"USP",1367],[17,"Beretta 92FS",400000],[18,"Fiveseven",5500],[19,"Magnum",16455],[20,"Desert Eagle",36061],[21,"Dual 92G Berettas",3540000],[22,"Sawed-Off Shotgun",1492],[23,"Benelli M1 Tactical",3078],[24,"MP5 Navy",3542],[25,"P90",6434],[26,"AK-47",9668],[27,"M4A1 Colt Carbine",12730],[28,"Benelli M4 Super",20819],[29,"M16 A2 Rifle",35493],[30,"Steyr AUG",73070],[31,"M249 SAW",664638],[32,"Leather Vest",1614],[33,"Police Vest",7318],[34,"Bulletproof Vest",32767],[35,"Box of Chocolate Bars",537],[36,"Big Box of Chocolate Bars",7534],[37,"Bag of Bon Bons",633],[38,"Box of Bon Bons",1120],[39,"Box of Extra Strong Mints",628],[40,"Pack of Music CDs",100],[41,"DVD Player",829],[42,"MP3 Player",696],[43,"CD Player",938],[44,"Pack of Blank CDs : 100",149],[45,"Hard Drive",268],[46,"Tank Top",199],[47,"Trainers",2245],[48,"Jacket",203],[49,"Full Body Armor",58967],[50,"Outer Tactical Vest",748209],[51,"Plain Silver Ring",497],[52,"Sapphire Ring",1008],[53,"Gold Ring",770],[54,"Diamond Ring",5113],[55,"Pearl Necklace",22101],[56,"Silver Necklace",4629],[57,"Gold Necklace",984],[58,"Plastic Watch",706],[59,"Stainless Steel Watch",905],[60,"Gold Watch",1024],[61,"Personal Computer",992],[62,"Microwave",24781],[63,"Minigun",1273190],[64,"Pack of Cuban Cigars",683],[65,"Television",1379],[66,"Morphine",14332],[67,"First Aid Kit",8119],[68,"Small First Aid Kit",4082],[69,"Simple Virus",1819],[70,"Polymorphic Virus",15400],[71,"Tunneling Virus",16150],[72,"Armored Virus",98000],[73,"Stealth Virus",200082],[74,"Santa Hat '04",0],[75,"Christmas Cracker '04",0],[76,"Snow Cannon",0],[77,"Toyota MR2",28745],[78,"Honda NSX",31875],[79,"Audi TT Quattro",53288],[80,"BMW M5",42481],[81,"BMW Z8",79524],[82,"Chevrolet Corvette Z06",49542],[83,"Dodge Charger",68367],[84,"Pontiac Firebird",58041],[85,"Ford GT40",119581],[86,"Hummer H3",20292],[87,"Audi S4",12914],[88,"Honda Integra R",14495],[89,"Honda Accord",22751],[90,"Honda Civic",8877],[91,"Volkswagen Beetle",7414],[92,"Chevrolet Cavalier",5390],[93,"Ford Mustang",17568],[94,"Reliant Robin",9286],[95,"Holden SS",24775],[96,"Coat Hanger",99498],[97,"Bunch of Flowers",134],[98,"Neutrilux 2000",0],[99,"Springfield 1911",262],[100,"Egg Propelled Launcher",0],[101,"Bunny Suit",0],[102,"Chocolate Egg '05",0],[103,"Firewalk Virus",34970],[104,"Game Console",22805],[105,"Xbox",0],[106,"Parachute",449167712],[107,"Trench Coat",352719],[108,"9mm Uzi",600161],[109,"RPG Launcher",0],[110,"Leather Bullwhip",716],[111,"Ninja Claws",5613],[112,"Test Trophy",0],[113,"Pet Rock",0],[114,"Non-Anon Doll",0],[115,"Poker Doll",0],[116,"Yoda Figurine",0],[117,"Trojan Horse",0],[118,"Evil Doll",0],[119,"Rubber Ducky of Doom",0],[120,"Teppic Bear",0],[121,"RockerHead Doll",0],[122,"Mouser Doll",0],[123,"Elite Action Man",0],[124,"Toy Reactor",0],[125,"Royal Doll",0],[126,"Blue Dragon",0],[127,"China Tea Set",0],[128,"Mufasa Toy",0],[129,"Dozen Roses",240],[130,"Skanky Doll",0],[131,"Lego Hurin",0],[132,"Mystical Sphere",0],[133,"10 Ton Pacifier",0],[134,"Horse",0],[135,"Uriel's Speakers",0],[136,"Strife Clown",0],[137,"Locked Teddy",0],[138,"Riddle's Bat",0],[139,"Soup Nazi Doll",0],[140,"Pouncer Doll",0],[141,"Spammer Doll",0],[142,"Cookie Jar",0],[143,"Vanity Mirror",0],[144,"Banana Phone",0],[145,"Xbox 360",0],[146,"Yasukuni Sword",22000000],[147,"Rusty Sword",75800],[148,"Dance Toy",0],[149,"Lucky Dime",0],[150,"Crystal Carousel",0],[151,"Pixie Sticks",287867],[152,"Ice Sculpture",0],[153,"Case of Whiskey",0],[154,"Laptop",249374],[155,"Purple Frog Doll",0],[156,"Hooorang's Key",0],[157,"Patriot Whip",0],[158,"Statue Of Aeolus",0],[159,"Bolt Cutters",417],[160,"Photographs",0],[161,"Black Unicorn",0],[162,"WarPaint Kit",0],[163,"Official Ninja Kit",0],[164,"Leukaemia Teddy Bear",0],[165,"Chocobo Flute",0],[166,"Annoying Man",0],[167,"Article on Crime",0],[168,"Unknown",0],[169,"Barbie Doll",0],[170,"Wand of Destruction",0],[171,"Jack-O-Lantern '05",0],[172,"Gas Can",186266],[173,"Butterfly Knife",1111],[174,"XM8 Rifle",8992],[175,"Taser",4096],[176,"Chain Mail",3267],[177,"Cobra Derringer",52864],[178,"Flak Jacket",4502],[179,"Birthday Cake '05",0],[180,"Bottle of Beer",3736],[181,"Bottle of Champagne",3809],[182,"Soap on a Rope",197],[183,"Single Red Rose",770],[184,"Bunch of Black Roses",766],[185,"Bunch of Balloons '05",0],[186,"Sheep Plushie",642],[187,"Teddy Bear Plushie",649],[188,"Cracked Crystal Ball",0],[189,"S&W Revolver",15688],[190,"C4 Explosive",200000],[191,"Memory Locket",0],[192,"Rainbow Stud Earring",0],[193,"Hamster Toy",0],[194,"Snowflake '05",0],[195,"Christmas Tree '05",0],[196,"Cannabis",5457],[197,"Ecstasy",42997],[198,"Ketamine",39598],[199,"LSD",44326],[200,"Opium",77899],[201,"PCP",79656],[202,"Mr Torn Crown '07",0],[203,"Shrooms",14244],[204,"Speed",149074],[205,"Vicodin",1231],[206,"Xanax",837290],[207,"Ms Torn Crown '07",0],[208,"Unknown",0],[209,"Box of Sweet Hearts",610],[210,"Bag of Chocolate Kisses",601],[211,"Crazy Cow",0],[212,"Legend's Urn",0],[213,"Dreamcatcher",0],[214,"Brutus Keychain",0],[215,"Kitten Plushie",701],[216,"Single White Rose",0],[217,"Claymore Sword",74982],[218,"Crossbow",823],[219,"Enfield SA-80",192784],[220,"Grenade",6971],[221,"Stick Grenade",8349],[222,"Flash Grenade",32254],[223,"Jackhammer",4000767],[224,"Swiss Army Knife",1874],[225,"Mag 7",46973],[226,"Smoke Grenade",105117],[227,"Spear",400],[228,"Vektor CR-21",6250],[229,"Claymore Mine",11876],[230,"Flare Gun",178],[231,"Heckler & Koch SL8",33833],[232,"SIG 550",137250],[233,"BT MP9",39510],[234,"Chain Whip",10420],[235,"Wooden Nunchakus",4100],[236,"Kama",48332],[237,"Kodachi",79953],[238,"Sai",750],[239,"Ninja Star",1229],[240,"Type 98 Anti Tank",16666667],[241,"Bushmaster Carbon 15",9479],[242,"HEG",15007],[243,"Taurus",332],[244,"Blowgun",2005],[245,"Bo Staff",303],[246,"Fireworks",2365],[247,"Katana",12412],[248,"Qsz-92",68319],[249,"SKS Carbine",5014],[250,"Twin Tiger Hooks",46500],[251,"Wushu Double Axes",92748],[252,"Ithaca 37",7217],[253,"Lorcin 380",170],[254,"S&W M29",310000],[255,"Flamethrower",2500872],[256,"Tear Gas",63696],[257,"Throwing Knife",29515],[258,"Jaguar Plushie",17594],[259,"Mayan Statue",2010],[260,"Dahlia",2703],[261,"Wolverine Plushie",10464],[262,"Hockey Stick",959],[263,"Crocus",8599],[264,"Orchid",30875],[265,"Pele Charm",2446],[266,"Nessie Plushie",42479],[267,"Heather",47662],[268,"Red Fox Plushie",43728],[269,"Monkey Plushie",48836],[270,"Soccer Ball",3190],[271,"Ceibo Flower",43533],[272,"Edelweiss",12035],[273,"Chamois Plushie",24204],[274,"Panda Plushie",72297],[275,"Jade Buddha",27370],[276,"Peony",75988],[277,"Cherry Blossom",64212],[278,"Kabuki Mask",15708],[279,"Maneki Neko",74596],[280,"Elephant Statue",4877],[281,"Lion Plushie",74593],[282,"African Violet",60438],[283,"Donator Pack",22628253],[284,"Bronze Paint Brush",0],[285,"Silver Paint Brush",0],[286,"Gold Paint Brush",0],[287,"Pand0ra's Box",0],[288,"Mr Brownstone Doll",0],[289,"Dual Axes",0],[290,"Dual Hammers",635000000],[291,"Dual Scimitars",0],[292,"Dual Samurai Swords",0],[293,"Japanese/English Dictionary",0],[294,"Bottle of Sake",3877],[295,"Oriental Log",0],[296,"Oriental Log Translation",0],[297,"YouYou Yo Yo",0],[298,"Monkey Cuffs",0],[299,"Jester's Cap",0],[300,"Gibal's Dragonfly",0],[301,"Green Ornament",0],[302,"Purple Ornament",0],[303,"Blue Ornament",0],[304,"Purple Bell",0],[305,"Mistletoe",517897],[306,"Mini Sleigh",404444],[307,"Snowman",999998],[308,"Christmas Gnome",861250],[309,"Gingerbread House",440850],[310,"Lollipop",559],[311,"Mardi Gras Beads",0],[312,"Devil Toy",0],[313,"Cookie Launcher",0],[314,"Cursed Moon Pendant",0],[315,"Apartment Blueprint",0],[316,"Semi-Detached House Blueprint",0],[317,"Detached House Blueprint",0],[318,"Beach House Blueprint",0],[319,"Chalet Blueprint",0],[320,"Villa Blueprint",0],[321,"Penthouse Blueprint",0],[322,"Mansion Blueprint",0],[323,"Ranch Blueprint",0],[324,"Palace Blueprint",0],[325,"Castle Blueprint",0],[326,"Printing Paper",49904],[327,"Blank Tokens",70913],[328,"Blank Credit Cards",90500],[329,"Skateboard",408749996],[330,"Boxing Gloves",448400000],[331,"Dumbbells",452625000],[332,"Combat Vest",3118668],[333,"Liquid Body Armor",7150000],[334,"Flexible Body Armor",12800039],[335,"Stick of Dynamite",50331],[336,"Cesium-137",674999999],[337,"Dirty Bomb",0],[338,"Sh0rty's Surfboard",0],[339,"Puzzle Piece",0],[340,"Hunny Pot",0],[341,"Seductive Stethoscope",0],[342,"Dollar Bill Collectible",0],[343,"Backstage Pass",0],[344,"Chemi's Magic Potion",0],[345,"Pack of Trojans",0],[346,"Pair of High Heels",0],[347,"Thong",10499997],[348,"Hazmat Suit",0],[349,"Flea Collar",0],[350,"Dunkin's Donut",0],[351,"Amazon Doll",0],[352,"BBQ Smoker",0],[353,"Bag of Cheetos",0],[354,"Motorbike",0],[355,"Citrus Squeezer",0],[356,"Superman Shades",0],[357,"Kevlar Helmet",0],[358,"Raw Ivory",0],[359,"Fine Chisel",0],[360,"Ivory Walking Cane",0],[361,"Neumune Tablet",771109],[362,"Mr Torn Crown '08",0],[363,"Ms Torn Crown '08",0],[364,"Box of Grenades",1074348],[365,"Box of Medical Supplies",255892],[366,"Erotic DVD",2805157],[367,"Feathery Hotel Coupon",14068699],[368,"Lawyer Business Card",501461],[369,"Lottery Voucher",1009796],[370,"Drug Pack",4218570],[371,"Dark Doll",0],[372,"Empty Box",405],[373,"Parcel",0],[374,"Birthday Present",0],[375,"Present",0],[376,"Christmas Present",0],[377,"Birthday Wrapping Paper",557],[378,"Generic Wrapping Paper",437],[379,"Christmas Wrapping Paper",971],[380,"Small Explosive Device",6029999],[381,"Gold Laptop",0],[382,"Gold Plated AK-47",0],[383,"Platinum PDA",0],[384,"Camel Plushie",95602],[385,"Tribulus Omanense",85047],[386,"Sports Sneakers",0],[387,"Handbag",0],[388,"Pink Mac-10",0],[389,"Mr Torn Crown '09",0],[390,"Ms Torn Crown '09",0],[391,"Macana",82755],[392,"Pepper Spray",1550],[393,"Slingshot",947],[394,"Brick",506],[395,"Metal Nunchakus",404833],[396,"Business Class Ticket",6070799],[397,"Flail",6799999],[398,"SIG 552",4975000],[399,"ArmaLite M-15A4",17220000],[400,"Guandao",208333],[401,"Lead Pipe",82],[402,"Ice Pick",15083],[403,"Box of Tissues",220],[404,"Bandana",800],[405,"Loaf of Bread",0],[406,"Afro Comb",69994],[407,"Compass",12270],[408,"Sextant",24000],[409,"Yucca Plant",8652],[410,"Fire Hydrant",20166],[411,"Model Space Ship",19500],[412,"Sports Shades",2775],[413,"Mountie Hat",20950],[414,"Proda Sunglasses",2848],[415,"Ship in a Bottle",41333],[416,"Paper Weight",2386],[417,"RS232 Cable",300],[418,"Tailors Dummy",0],[419,"Small Suitcase",1831000],[420,"Medium Suitcase",3548022],[421,"Large Suitcase",10133428],[422,"Vanity Hand Mirror",279000],[423,"Poker Chip",0],[424,"Rabbit Foot",0],[425,"Voodoo Doll",0],[426,"Bottle of Tequila",3743],[427,"Sumo Doll",2932],[428,"Casino Pass",47784606],[429,"Chopsticks",2991],[430,"Coconut Bra",422000],[431,"Dart Board",5611],[432,"Crazy Straw",1071],[433,"Sensu",3200],[434,"Yakitori Lantern",4633],[435,"Dozen White Roses",0],[436,"Snowboard",8142],[437,"Glow Stick",25592],[438,"Cricket Bat",22750],[439,"Frying Pan",318],[440,"Pillow",499997],[441,"Khinkeh P0rnStar Doll",0],[442,"Blow-Up Doll",0],[443,"Strawberry Milkshake",0],[444,"Breadfan Doll",0],[445,"Chaos Man",0],[446,"Karate Man",0],[447,"Burmese Flag",0],[448,"Bl0ndie's Dictionary",0],[449,"Hydroponic Grow Tent",0],[450,"Leopard Coin",1535000],[451,"Florin Coin",1523333],[452,"Gold Noble Coin",1574995],[453,"Ganesha Sculpture",12239746],[454,"Vairocana Buddha Sculpture",4550000],[455,"Quran Script : Ibn Masud",16000000],[456,"Quran Script : Ubay Ibn Kab",15706481],[457,"Quran Script : Ali",14990000],[458,"Shabti Sculpture",23800000],[459,"Egyptian Amulet",0],[460,"White Senet Pawn",9288333],[461,"Black Senet Pawn",9019258],[462,"Senet Board",565000],[463,"Epinephrine",912626],[464,"Melatonin",307109],[465,"Serotonin",1342107],[466,"Snow Globe '09",0],[467,"Dancing Santa Claus '09",4499998],[468,"Christmas Stocking '09",0],[469,"Santa's Elf '09",0],[470,"Christmas Card '09",0],[471,"Admin Portrait '09",0],[472,"Blue Easter Egg",62900],[473,"Green Easter Egg",64799],[474,"Red Easter Egg",61242],[475,"Yellow Easter Egg",46479],[476,"White Easter Egg",221750],[477,"Black Easter Egg",75333],[478,"Gold Easter Egg",1099999],[479,"Metal Dog Tag",0],[480,"Bronze Dog Tag",0],[481,"Silver Dog Tag",0],[482,"Gold Dog Tag",0],[483,"MP5k",4571],[484,"AK74U",10718],[485,"Skorpion",5093],[486,"TMP",7500],[487,"Thompson",7560],[488,"MP 40",6250],[489,"Luger",8650],[490,"Blunderbuss",5133],[491,"Zombie Brain",0],[492,"Human Head",0],[493,"Medal of Honor",24990000],[494,"Citroen Saxo",31332],[495,"Classic Mini",8666],[496,"Fiat Punto",67596],[497,"Nissan Micra",19499],[498,"Peugeot 106",6740],[499,"Renault Clio",10996],[500,"Vauxhall Corsa",9581],[501,"Volvo 850",33749],[502,"Alfa Romeo 156",11900],[503,"BMW X5",34667],[504,"Seat Leon Cupra",10091],[505,"Vauxhall Astra GSI",8470],[506,"Volkswagen Golf GTI",10000],[507,"Audi S3",0],[508,"Ford Focus RS",51257],[509,"Honda S2000",29000],[510,"Mini Cooper S",51222],[511,"Sierra Cosworth",5845600],[512,"Lotus Exige",18000],[513,"Mitsubishi Evo X",228306],[514,"Porsche 911 GT3",84500],[515,"Subaru Impreza STI",59500],[516,"TVR Sagaris",34500],[517,"Aston Martin One-77",1226777],[518,"Audi R8",179983],[519,"Bugatti Veyron",0],[520,"Ferrari 458",855749],[521,"Lamborghini Gallardo",959386],[522,"Lexus LFA",703000],[523,"Mercedes SLR",948200],[524,"Nissan GT-R",71042],[525,"Mr Torn Crown '10",0],[526,"Ms Torn Crown '10",0],[527,"Bag of Candy Kisses",20548],[528,"Bag of Tootsie Rolls",43835],[529,"Bag of Chocolate Truffles",95403],[530,"Can of Munster",1499048],[531,"Bottle of Pumpkin Brew",68091],[532,"Can of Red Cow",2067406],[533,"Can of Taurine Elite",3621379],[534,"Witch's Cauldron",0],[535,"Electronic Pumpkin",200000],[536,"Jack O Lantern Lamp",400000],[537,"Spooky Paper Weight",199995],[538,"Medieval Helmet",15000000],[539,"Blood Spattered Sickle",65000000],[540,"Cauldron",0],[541,"Bottle of Stinky Swamp Punch",287694],[542,"Bottle of Wicked Witch",169880],[543,"Deputy Star",0],[544,"Wind Proof Lighter",80374750],[545,"Dual TMPs",0],[546,"Dual Bushmasters",0],[547,"Dual MP5s",0],[548,"Dual P90s",0],[549,"Dual Uzis",0],[550,"Bottle of Kandy Kane",67849],[551,"Bottle of Minty Mayhem",168401],[552,"Bottle of Mistletoe Madness",281595],[553,"Can of Santa Shooters",1501771],[554,"Can of Rockstar Rudolph",2101523],[555,"Can of X-MASS",3636391],[556,"Bag of Reindeer Droppings",95679],[557,"Advent Calendar",33265],[558,"Santa's Snot",32749],[559,"Polar Bear Toy",55000],[560,"Fruitcake",702],[561,"Book of Carols",11197964],[562,"Sweater",9165],[563,"Gift Card",2997498],[564,"Glasses",104894],[565,"High-Speed Drive",91877],[566,"Mountain Bike",551190],[567,"Cut-Throat Razor",135140],[568,"Slim Crowbar",146800],[569,"Balaclava",679249],[570,"Advanced Driving Manual",141184],[571,"Ergonomic Keyboard",171331],[572,"Tracking Device",106248],[573,"Screwdriver",332732],[574,"Fanny Pack",117116],[575,"Tumble Dryer",89734],[576,"Chloroform",144656],[577,"Heavy Duty Padlock",87665],[578,"Duct Tape",160757],[579,"Wireless Dongle",155500],[580,"Horse's Head",1264889],[581,"Book",8000000],[582,"Tin Foil Hat",0],[583,"Brown Easter Egg",70000],[584,"Orange Easter Egg",47817],[585,"Pink Easter Egg",58666],[586,"Jawbreaker",284750],[587,"Bag of Sherbet",284300],[588,"Goodie Bag",31895888],[589,"Undefined",0],[590,"Undefined 2",0],[591,"Undefined 3",0],[592,"Undefined 4",0],[593,"Mr Torn Crown '11",0],[594,"Ms Torn Crown '11",0],[595,"Pile of Vomit",5552778],[596,"Rusty Dog Tag",0],[597,"Gold Nugget",15140000],[598,"Witch's Hat",0],[599,"Golden Broomstick",0],[600,"Devil's Pitchfork",0],[601,"Christmas Lights",305000],[602,"Gingerbread Man",299997],[603,"Golden Wreath",564629],[604,"Pair of Ice Skates",0],[605,"Diamond Icicle",815000],[606,"Santa Boots",949963],[607,"Santa Gloves",943294],[608,"Santa Hat",3849999],[609,"Santa Jacket",2112500],[610,"Santa Trousers",1876667],[611,"Snowball",16583],[612,"Tavor TAR-21",349518],[613,"Harpoon",204000],[614,"Diamond Bladed Knife",820747],[615,"Naval Cutlass",40000000],[616,"Trout",17910],[617,"Banana Orchid",11780],[618,"Stingray Plushie",10636],[619,"Steel Drum",7293],[620,"Nodding Turtle",3967],[621,"Snorkel",16667],[622,"Flippers",7167],[623,"Speedo",8433],[624,"Bikini",18151],[625,"Wetsuit",20066],[626,"Diving Gloves",3004],[627,"Dog Poop",999811],[628,"Stink Bombs",966843],[629,"Toilet Paper",1001291],[630,"Mr Torn Crown '12",0],[631,"Ms Torn Crown '12",0],[632,"Petrified Humerus",0],[633,"Latex Gloves",0],[634,"Bag of Bloody Eyeballs",46155],[635,"Straitjacket",0],[636,"Cinnamon Ornament",161963],[637,"Christmas Express",898888],[638,"Bottle of Christmas Cocktail",199973],[639,"Golden Candy Cane",803500],[640,"Kevlar Gloves",406249],[641,"WWII Helmet",122688],[642,"Motorcycle Helmet",61993995],[643,"Construction Helmet",19320],[644,"Welding Helmet",0],[645,"Safety Boots",80460],[646,"Hiking Boots",7108],[647,"Leather Helmet",1200],[648,"Leather Pants",1399],[649,"Leather Boots",1007],[650,"Leather Gloves",1027],[651,"Combat Helmet",3193618],[652,"Combat Pants",3069699],[653,"Combat Boots",2423791],[654,"Combat Gloves",2058102],[655,"Riot Helmet",800000001],[656,"Riot Body",585000000],[657,"Riot Pants",430000001],[658,"Riot Boots",256848586],[659,"Riot Gloves",254116718],[660,"Dune Helmet",416666666],[661,"Dune Vest",520313603],[662,"Dune Pants",398750000],[663,"Dune Boots",292250000],[664,"Dune Gloves",291750001],[665,"Assault Helmet",400000001],[666,"Assault Body",1700000001],[667,"Assault Pants",1500000001],[668,"Assault Boots",0],[669,"Assault Gloves",889000001],[670,"Delta Gas Mask",0],[671,"Delta Body",2645000000],[672,"Delta Pants",0],[673,"Delta Boots",2900000000],[674,"Delta Gloves",0],[675,"Marauder Face Mask",0],[676,"Marauder Body",0],[677,"Marauder Pants",0],[678,"Marauder Boots",0],[679,"Marauder Gloves",0],[680,"EOD Helmet",0],[681,"EOD Apron",0],[682,"EOD Pants",0],[683,"EOD Boots",0],[684,"EOD Gloves",0],[685,"Torn Bible",0],[686,"Friendly Bot Guide",0],[687,"Egotistical Bear",0],[688,"Brewery Key",0],[689,"Signed Jersey",0],[690,"Mafia Kit",0],[691,"Octopus Toy",0],[692,"Bear Skin Rug",0],[693,"Tractor Toy",0],[694,"Mr Torn Crown '13",0],[695,"Ms Torn Crown '13",0],[696,"Piece of Cake",0],[697,"Rotten Eggs",0],[698,"Peg Leg",0],[699,"Antidote",0],[700,"Christmas Angel",140916],[701,"Eggnog",1500000],[702,"Sprig of Holly",248148],[703,"Festive Socks",601184],[704,"Respo Hoodie",0],[705,"Staff Haxx Button",0],[706,"Birthday Cake '14",0],[707,"Lump of Coal",293750],[708,"Gold Rosette",0],[709,"Silver Rosette",0],[710,"Bronze Rosette",0],[711,"Coin : Factions",0],[712,"Coin : Casino",0],[713,"Coin : Education",0],[714,"Coin : Hospital",0],[715,"Coin : Jail",0],[716,"Coin : Travel Agency",0],[717,"Coin : Companies",0],[718,"Coin : Stock Exchange",0],[719,"Coin : Church",0],[720,"Coin : Auction House",0],[721,"Coin : Race Track",0],[722,"Coin : Museum",0],[723,"Coin : Drugs",0],[724,"Coin : Dump",0],[725,"Coin : Estate Agents",0],[726,"Scrooge's Top Hat",0],[727,"Scrooge's Topcoat",65655487],[728,"Scrooge's Trousers",17333334],[729,"Scrooge's Boots",0],[730,"Scrooge's Gloves",0],[731,"Empty Blood Bag",16900],[732,"Blood Bag : A+",18295],[733,"Blood Bag : A-",20833],[734,"Blood Bag : B+",20462],[735,"Blood Bag : B-",23197],[736,"Blood Bag : AB+",22993],[737,"Blood Bag : AB-",26350],[738,"Blood Bag : O+",18058],[739,"Blood Bag : O-",23249],[740,"Mr Torn Crown",0],[741,"Ms Torn Crown",0],[742,"Molotov Cocktail",66683],[743,"Christmas Sweater '15",0],[744,"Book : Brawn Over Brains",0],[745,"Book : Time Is In The Mind",0],[746,"Book : Keeping Your Face Handsome",0],[747,"Book : A Job For Your Hands",0],[748,"Book : Working 9 Til 5",0],[749,"Book : Making Friends, Enemies, And Cakes",0],[750,"Book : High School For Adults",0],[751,"Book : Milk Yourself Sober",0],[752,"Book : Fight Like An Asshole",0],[753,"Book : Mind Over Matter",0],[754,"Book : No Shame No Pain",0],[755,"Book : Run Like The Wind",0],[756,"Book : Weaseling Out Of Trouble",0],[757,"Book : Get Hard Or Go Home",0],[758,"Book : Gym Grunting - Shouting To Success",0],[759,"Book : Self Defense In The Workplace",0],[760,"Book : Speed 3 - The Rejected Script",0],[761,"Book : Limbo Lovers 101",0],[762,"Book : The Hamburglar's Guide To Crime",0],[763,"Book : What Are Old Folk Good For Anyway?",0],[764,"Book : Medical Degree Schmedical Degree",0],[765,"Book : No More Soap On A Rope",0],[766,"Book : Mailing Yourself Abroad",0],[767,"Book : Smuggling For Beginners",0],[768,"Book : Stealthy Stealing of Underwear",0],[769,"Book : Shawshank Sure Ain't For Me!",0],[770,"Book : Ignorance Is Bliss",0],[771,"Book : Winking To Win",0],[772,"Book : Finders Keepers",0],[773,"Book : Hot Turkey",0],[774,"Book : Higher Daddy, Higher!",0],[775,"Book : The Real Dutch Courage",0],[776,"Book : Because I'm Happy - The Pharrell Story",0],[777,"Book : No More Sick Days",0],[778,"Book : Duke - My Story",0],[779,"Book : Self Control Is For Losers",0],[780,"Book : Going Back For More",0],[781,"Book : Get Drunk And Lose Dignity",0],[782,"Book : Fuelling Your Way To Failure",0],[783,"Book : Yes Please Diabetes",0],[784,"Book : Ugly Energy",0],[785,"Book : Memories And Mammaries",0],[786,"Book : Brown-nosing The Boss",0],[787,"Book : Running Away From Trouble",0],[788,"Certificate of Awesome",28448],[789,"Certificate of Lame",28923],[790,"Plastic Sword",6192],[791,"Mediocre T-Shirt",1974],[792,"Penelope",0],[793,"Cake Frosting",0],[794,"Lock Picking Kit",0],[795,"Special Fruitcake",0],[796,"Felovax",0],[797,"Zylkene",0],[798,"Duke's Safe",0],[799,"Duke's Selfies",0],[800,"Duke's Poetry",0],[801,"Duke's Dog's Ashes",0],[802,"Duke's Will",0],[803,"Duke's Gimp Mask",0],[804,"Duke's Herpes Medication",0],[805,"Duke's Hammer",0],[806,"Old Lady Mask",23998],[807,"Exotic Gentleman Mask",26000],[808,"Ginger Kid Mask",47100],[809,"Young Lady Mask",31367],[810,"Moustache Man Mask",24000],[811,"Scarred Man Mask",26347],[812,"Psycho Clown Mask",122012],[813,"Nun Mask",24650],[814,"Tyrosine",612224],[815,"Keg of Beer",10099198],[816,"Glass of Beer",0],[817,"Six Pack of Alcohol",1018993],[818,"Six Pack of Energy Drink",13907253],[819,"Rosary Beads",1653250],[820,"Piggy Bank",0],[821,"Empty Vial",0],[822,"Vial of Blood",0],[823,"Vial of Urine",0],[824,"Vial of Saliva",0],[825,"Questionnaire ",0],[826,"Agreement",0],[827,"Perceptron : Calibrator",0],[828,"Donald Trump Mask '16",0],[829,"Yellow Snowman '16",0],[830,"Nock Gun",0],[831,"Beretta Pico",32899999],[832,"Riding Crop",0],[833,"Sand",0],[834,"Sweatpants",966666],[835,"String Vest",325000],[836,"Black Oxfords",5000000],[837,"Rheinmetall MG 3",0],[838,"Homemade Pocket Shotgun",0],[839,"Madball",0],[840,"Nail Bomb",1495662],[841,"Classic Fedora",1016666],[842,"Pinstripe Suit Trousers",1122000],[843,"Duster",0],[844,"Tranquilizer Gun",0],[845,"Bolt Gun",0],[846,"Scalpel",0],[847,"Nerve Gas",0],[848,"Kevlar Lab Coat",0],[849,"Loupes",0],[850,"Sledgehammer",335000001],[851,"Wifebeater",0],[852,"Metal Detector",0],[853,"Graveyard Key",0],[854,"Questionnaire : Completed",0],[855,"Agreement : Signed",0],[856,"Spray Can : Black",0],[857,"Spray Can : Red",0],[858,"Spray Can : Pink",0],[859,"Spray Can : Purple",0],[860,"Spray Can : Blue",0],[861,"Spray Can : Green",0],[862,"Spray Can : Yellow",0],[863,"Spray Can : Orange",0],[864,"Salt Shaker",29665],[865,"Poison Mistletoe",12662333],[866,"Santa's List '17",0],[867,"Soapbox",0],[868,"Turkey Baster",46975],[869,"Elon Musk Mask '17",0],[870,"Love Juice",14878944],[871,"Bug Swatter",0],[872,"Nothing",0],[873,"Bottle of Green Stout",1014128],[874,"Prototype",0],[875,"Rotten Apple",0],[876,"Festering Chicken",0],[877,"Moldy Pizza",0],[878,"Smelly Cheese",0],[879,"Sour Milk",0],[880,"Stale Bread",0],[881,"Spoiled Fish",0],[882,"Insurance Policy ",0],[883,"Bank Statement",0],[884,"Car Battery",0],[885,"Scrap Metal",0],[886,"Torn City Times",0],[887,"Magazine",0],[888,"Umbrella",0],[889,"Travel Mug",0],[890,"Headphones",0],[891,"Undefined",0],[892,"Mix CD",0],[893,"Lost and Found Office Key",0],[894,"Cosmetics Case",0],[895,"Phone Card",0],[896,"Subway Pass",0],[897,"Bottle Cap",0],[898,"Silver Coin",0],[899,"Silver Bead",0],[900,"Lucky Quarter",0],[901,"Daffodil",0],[902,"Bunch of Carnations",0],[903,"White Lily",0],[904,"Funeral Wreath",0],[905,"Car Keys",0],[906,"Handkerchief",0],[907,"Candle",0],[908,"Paper Bag",0],[909,"Tin Can",0],[910,"Betting Slip",0],[911,"Fidget Spinner",0],[912,"Majestic Moose",0],[913,"Lego Wonder Woman",0],[914,"CR7 Doll",0],[915,"Stretch Armstrong Doll",0],[916,"Beef Femur",0],[917,"Snake's Fang",0],[918,"Icey Igloo",0],[919,"Federal Jail Key",0],[920,"Halloween Basket : Spooky",0],[921,"Michael Myers Mask '18",0],[922,"Toast Jesus '18",0],[923,"Cheesus '18",0],[924,"Bottle of Christmas Spirit",1153838],[925,"Scammer in the Slammer '18",0],[926,"Gronch Mask '18",0],[927,"Baseball Cap",5000000],[928,"Bermudas",5000000],[929,"Blouse",0],[930,"Boob Tube",0],[931,"Bush Hat",0],[932,"Camisole",0],[933,"Capri Pants",0],[934,"Cardigan",19349999],[935,"Cork Hat",0],[936,"Crop Top",25000000],[937,"Fisherman Hat",0],[938,"Gym Shorts",0],[939,"Halterneck",0],[940,"Raincoat",0],[941,"Pantyhose",0],[942,"Pencil Skirt",0],[943,"Peplum Top",3600000],[944,"Polo Shirt",0],[945,"Poncho",0],[946,"Puffer Vest",0],[947,"Mackintosh",0],[948,"Shorts",0],[949,"Skirt",12000000],[950,"Travel Socks",4000000],[951,"Turtleneck",0],[952,"Yoga Pants",9000000],[953,"Bronze Racing Trophy",0],[954,"Silver Racing Trophy",0],[955,"Gold Racing Trophy",0],[956,"Pack of Blank CDs : 250",0],[957,"Pack of Blank CDs : 50",0],[958,"Chest Harness",3974997],[959,"Choker",3999998],[960,"Fishnet Stockings",2483333],[961,"Knee-high Boots",6196992],[962,"Lingerie",10850000],[963,"Mankini",0],[964,"Mini Skirt",4500000],[965,"Nipple Tassels",0],[966,"Bowler Hat",1383333],[967,"Fitted Shirt",7999999],[968,"Bow Tie",3049499],[969,"Neck Tie",4000000],[970,"Waistcoat",2125001],[971,"Blazer",4525000],[972,"Suit Trousers",10399495],[973,"Derby Shoes",3700000],[974,"Smoking Jacket",25000000],[975,"Monocle",0],[976,"Bronze Microphone",0],[977,"Silver Microphone",0],[978,"Gold Microphone",0],[979,"Paint Mask",0],[980,"Ladder",0],[981,"Wire Cutters",0],[982,"Ripped Jeans",7133354],[983,"Bandit Mask",1200000],[984,"Bottle of Moonshine",1012500],[985,"Can of Goose Juice",336607],[986,"Can of Damp Valley",639718],[987,"Can of Crocozade",999843],[988,"Fur Coat",8700000],[989,"Fur Scarf",3936728],[990,"Fur Hat",2000000],[991,"Platform Shoes",2633334],[992,"Silver Flats",2878370],[993,"Crystal Bracelet",12000001],[994,"Cocktail Ring",6599990],[995,"Sun Hat",3347999],[996,"Square Sunglasses",6150001],[997,"Statement Necklace",0],[998,"Floral Dress",2450000],[1001,"Shrug",0],[1002,"Eye Patch",0],[1003,"Halloween Basket : Creepy",0],[1004,"Halloween Basket : Freaky",0],[1005,"Halloween Basket : Frightful",0],[1006,"Halloween Basket : Haunting",0],[1007,"Halloween Basket : Shocking",0],[1008,"Halloween Basket : Terrifying",0],[1009,"Halloween Basket : Horrifying",0],[1010,"Halloween Basket : Petrifying",0],[1011,"Halloween Basket : Nightmarish",0],[1012,"Blood Bag : Irradiated",24907],[1013,"Jigsaw Mask '19",0],[1014,"Reading Glasses",800000],[1015,"Chinos",2405000],[1016,"Collared Shawl",1400000],[1017,"Pleated Skirt",1100420],[1018,"Flip Flops",0],[1019,"Bingo Visor",0],[1020,"Cover-ups",0],[1021,"Sandals",0],[1022,"Golf Socks",0],[1023,"Flat Cap",0],[1024,"Slippers",0],[1025,"Bathrobe",19316666],[1026,"Party Hat '19",1358333],[1027,"Badge : 15th Anniversary",0],[1028,"Birthday Cupcake",1530320],[1029,"Strippogram Voucher",11007874],[1030,"Dong : Thomas",0],[1031,"Dong : Greg",0],[1032,"Dong : Effy",0],[1033,"Dong : Holly",0],[1034,"Dong : Jeremy",3000000],[1035,"Anniversary Present",85000000],[1036,"Greta Mask '19",0],[1037,"Anatoly Mask '19",0],[1038,"Santa Beard",75000000],[1039,"Bag of Humbugs",272469],[1040,"Christmas Cracker",975670],[1041,"Special Snowflake",0],[1042,"Concussion Grenade",4514450],[1043,"Paper Crown : Green",399965],[1044,"Paper Crown : Yellow",353332],[1045,"Paper Crown : Red",350278],[1046,"Paper Crown : Blue",300000],[1047,"Denim Shirt",0],[1048,"Denim Vest",999999],[1049,"Denim Jacket",962495],[1050,"Denim Jeans",0],[1051,"Denim Shoes",0],[1052,"Denim Cap",974000],[1053,"Bread Knife",0],[1054,"Semtex",0],[1055,"Poison Umbrella",0],[1056,"Millwall Brick",0],[1057,"Gentleman Cache",7158332],[1058,"Gold Chain",0],[1059,"Snapback Hat",0],[1060,"Saggy Pants",0],[1061,"Oversized Shirt",4557500],[1062,"Basketball Shirt",3000001],[1063,"Parachute Pants",0],[1064,"Tube Dress",1733334],[1065,"Gold Sneakers",27749998],[1066,"Shutter Shades",6732963],[1067,"Silver Hoodie",0],[1068,"Bucket Hat",0],[1069,"Puffer Jacket",9875006],[1070,"Durag",1500001],[1071,"Onesie",0],[1072,"Baseball Jacket",3250000],[1073,"Braces",2500000],[1074,"Panama Hat",2000002],[1075,"Pipe",0],[1076,"Shoulder Sweater",0],[1077,"Sports Jacket",5256986],[1078,"Old Wallet",0],[1079,"Cardholder",0],[1080,"Billfold",0],[1081,"Coin Purse",0],[1082,"Zip Wallet",0],[1083,"Clutch",0],[1084,"Credit Card",0],[1085,"Lipstick",0],[1086,"License",0],[1087,"Tampon",0],[1088,"Receipt",0],[1089,"Family Photo",0],[1090,"Lint",0],[1091,"Handcuffs",0],[1092,"Lubricant",0],[1093,"Hit Contract",0],[1094,"Syringe",0],[1095,"Spoon",0],[1096,"Cell Phone",0],[1097,"Assless Chaps",0],[1098,"Opera Gloves",0],[1099,"Booty Shorts",3310000],[1100,"Collar",0],[1101,"Ball Gag",0],[1102,"Blindfold",5818520],[1103,"Maid Uniform",13000000],[1104,"Maid Hat",2235000],[1105,"Ball Gown",0],[1106,"Fascinator Hat",7330001],[1107,"Wedding Dress",0],[1108,"Wedding Veil",29999996],[1109,"Head Scarf",1899420],[1110,"Nightgown",0],[1111,"Pullover",1133332],[1112,"Elegant Cache",7500000],[1113,"Naughty Cache",13473395],[1114,"Elderly Cache",2679000],[1115,"Denim Cache",1824500],[1116,"Wannabe Cache",7216667],[1117,"Cutesy Cache",8298499],[1118,"Armor Cache",752375000],[1119,"Melee Cache",499727272],[1120,"Small Arms Cache",393500000],[1121,"Medium Arms Cache",631666667],[1122,"Heavy Arms Cache",1125000000],[1123,"Spy Camera",0],[1124,"Cloning Device",0],[1125,"Card Skimmer",0],[1126,"Tutu",0],[1127,"Knee Socks",2648334],[1128,"Kitty Shoes",32500000],[1129,"Cat Ears",6625000],[1130,"Bunny Ears",5833334],[1131,"Puppy Ears",4000001],[1132,"Heart Sunglasses",0],[1133,"Hair Bow",1872500],[1134,"Lolita Dress",9800000],[1135,"Unicorn Horn",139999993],[1136,"Check Skirt",2000001],[1137,"Polka Dot Dress",18999000],[1138,"Ballet Shoes",1300000],[1139,"Dungarees",0],[1140,"Tights",1999990],[1141,"Pennywise Mask '20",0],[1142,"Tiger King Mask '20",0],[1143,"Medical Mask",9000000],[1144,"Chin Diaper",0],[1145,"Tighty Whities",2450000],[1146,"Tangerine",405000],[1147,"Helmet of Justice",0],[1148,"Broken Bauble",424444],[1149,"Purple Easter Egg",171666],[1150,"Ski Mask",0],[1151,"Bunny Nose",0],[1152,"SMAW Launcher",0],[1153,"China Lake",0],[1154,"Milkor MGL",0],[1155,"PKM",0],[1156,"Negev NG-5",0],[1157,"Stoner 96",0],[1158,"Meat Hook",59333333],[1159,"Cleaver",63333333],[1176,"Arca Fortunae",0],[1177,"Sandworm Mask '21",0],[1178,"Party Popper",0],[1179,"Eye Bleach",0],[1180,"Prince Philip Mask '21",0],[1181,"Krampus Mask '21",0],[1182,"Head Bandage",0],[1183,"Medical Eye Patch",6350000],[1184,"Knee Brace",7010000],[1185,"Band-Aids",0],[1186,"Torso Bandage",0],[1187,"Prosthetic Arm",0],[1188,"Prosthetic Leg",0],[1189,"Formaldehyde",0],[1190,"Hook Hand",0],[1191,"Plaster Cast Leg",4999999],[1192,"Plaster Cast Arm",7444438],[1193,"Neck Brace",0],[1194,"Halo Vest",13596533],[1195,"Crutches",16000001],[1196,"Medical Diaper",6000000],[1197,"Hospital Gown",5000001],[1198,"Sticky Notes",0],[1199,"Casket",0],[1200,"Nitrous Tank",0],[1201,"Rope",0],[1202,"Window Breaker",0],[1203,"Lockpicks",0],[1204,"Skeleton Key",0],[1205,"Wrench",0],[1206,"Truck Nuts",0],[1207,"Thimble",0],[1208,"Beach Ball",0],[1209,"Hunting Trophy",0],[1210,"Hoe",0],[1211,"Fishing Rod",0],[1212,"Bleach",0],[1213,"Lye",0],[1214,"Towel",0],[1215,"Scissors",0],[1216,"Clippers",0],[1217,"Shaving Foam",0],[1219,"Oxygen Tank",0],[1220,"Massage Oil",0],[1221,"Jigsaw Puzzle",0],[1222,"Picture Frame",0],[1223,"Cigar Cutter",0],[1224,"Ash Tray",0],[1225,"Bowling Trophy",0],[1226,"Fertilizer",0],[1227,"Igniter Cord",0],[1228,"Paper Ream",0],[1230,"Detonator",0],[1231,"Golf Club",0],[1232,"Garden Gnome",0],[1233,"Wheelbarrow",0],[1234,"Shovel",0],[1235,"Blanket",0],[1236,"Crockpot",0],[1237,"Plunger",0],[1238,"Silver Cutlery Set",0],[1239,"Stash Box",0],[1240,"Perfume",0],[1241,"Croquet Set",0],[1242,"Horseshoe",0],[1243,"Persian Rug",0],[1244,"Typewriter",0],[1245,"Chandelier",0],[1246,"Inkwell",0],[1247,"Bull Semen",0],[1248,"Ammonia",0],[1249,"Hydrochloric Acid",0],[1250,"Anchor",0],[1251,"Boat Engine",0],[1252,"Steel Ingot",0],[1253,"Tractor Part",0],[1254,"Tire",0],[1255,"Bone Saw",0],[1256,"Machine Part",0],[1257,"Cattle Prod",0],[1258,"Binoculars",0],[1259,"Razor Wire",0],[1260,"Stamp Collection",0],[1261,"Bucket",0],[1262,"Urea",0],[1263,"Phosphorus",0],[1264,"Potassium Nitrate",0],[1265,"Grain",0],[1266,"Shampoo",0],[1267,"Detergent",0],[1268,"Vitamins",0],[1269,"Cough Syrup",0],[1270,"Paper Towels",0],[1271,"Pepper Mill",0],[1272,"Toothbrush",0],[1273,"Toothpaste",0],[1274,"Mouthwash",0],[1275,"Mop",0],[1276,"Broom",0],[1277,"Floor Cleaner",0],[1278,"Model Spine",0],[1279,"Massage Table",0],[1280,"Scalp Massager",0],[1281,"Dentures",0],[1282,"Gold Tooth",0],[1283,"Bleaching Tray",0],[1284,"Dental Mirror",0],[1285,"Paperclips",0],[1286,"Stapler",0],[1287,"Hole Punch",0],[1288,"Notepad",0],[1289,"Permanent Marker",0],[1290,"Toner",0],[1291,"Bloody Apron",0],[1292,"Bone",0],[1293,"Injury Cache",16274000],[1294,"Glitter Bomb",990000],[1295,"Hell Priest Mask '22",0],[1296,"Ban Hammer",0],[1297,"Donkey Adoption Certificate",60767708]]}`;
                    const fake_res = {};
                    fake_res.responseText = str;
                    if (args.onload) args.onload(fake_res);
                }
            };
        func(GM_addStyle, window, GM_getValue, GM_setValue, GM_xmlhttpRequest);
    }
})();
