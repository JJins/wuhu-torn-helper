# 芜湖助手 Torncity 翻译插件

[![LICENSE](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

[CHANGELOG](CHANGELOG.md)

一个为浏览器网页MMORPG游戏定制的辅助增强用户脚本，包含了一系列便携功能。

此脚本不包含自动化相关代码。

## 编译

    npm init
    npm run rollup

## 使用

[release.min.user.js](https://gitlab.com/JJins/wuhu-torn-helper/-/raw/dev/README_ZHCN.md)

请使用 Tampermonkey (浏览器) 或 TornPDA 安装。
