export const customInjector = (varName) => {
    let rt = ((__var) => {
        const inject = (ob) => {
            if (document && document.head) {
                ob?.disconnect();
                const style = document.createElement('style');
                style.setAttribute('type', 'text/css');
                style.innerHTML = __var;
                document.head.appendChild(style);
            }
        };
        if (document && document.head) {
            inject();
        } else {
            new MutationObserver((_, ob) => {
                inject(ob);
            }).observe(document.documentElement, { childList: true });
        }
    }).toString();
    return `(${ rt })(${ varName })`;
};
