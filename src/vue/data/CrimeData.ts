export const CrimeData = [
    {
      value: "2",
      label: "2n - 搜刮",
      children: [
        { value: "searchtrainstation", label: "2-1 搜火车站" },
        { value: "searchbridge", label: "2-2 搜桥洞" },
        { value: "searchbins", label: "2-3 搜垃圾桶" },
        { value: "searchfountain", label: "2-4 搜喷泉" },
        { value: "searchdumpster", label: "2-5 搜垃圾箱" },
        { value: "searchmovie", label: "2-6 搜电影院" },
      ],
    },
    {
      value: "3",
      label: "3n - 卖盗版碟",
      children: [
        { value: "cdrock", label: "3-1 卖摇滚碟" },
        { value: "cdheavymetal", label: "3-2 卖重金属碟" },
        { value: "cdpop", label: "3-3 卖KDA" },
        { value: "cdrap", label: "3-4 卖嘻哈碟" },
        { value: "cdreggae", label: "3-5 卖雷鬼碟" },
        { value: "dvdhorror", label: "3-6 卖恐怖电影碟" },
        { value: "dvdaction", label: "3-7 卖动作电影碟" },
        { value: "dvdromance", label: "3-8 卖爱情电影碟" },
        { value: "dvdsci", label: "3-9 卖科幻电影碟" },
        { value: "dvdthriller", label: "3-10 卖惊悚电影碟" },
      ],
    },
    {
      value: "4",
      label: "4n - 零元购",
      children: [
        { value: "sweetshop", label: "4-1 糖果店零元购" },
        { value: "marketstall", label: "4-2 路边摊零元购" },
        { value: "clothesshop", label: "4-3 服装店零元购" },
        { value: "jewelryshop", label: "4-4 珠宝店零元购" },
      ],
    },
    {
      value: "5",
      label: "5n - 扒窃",
      children: [
        { value: "hobo", label: "5-1 扒挎包" },
        { value: "kid", label: "5-2 扒小孩" },
        { value: "oldwoman", label: "5-3 扒老阿婆" },
        { value: "businessman", label: "5-4 扒商业男" },
        { value: "lawyer", label: "5-5 扒律师" },
      ],
    },
    {
      value: "6",
      label: "6n - 入室",
      children: [
        { value: "apartment", label: "6-1 进公寓" },
        { value: "house", label: "6-2 进平房" },
        { value: "mansion", label: "6-3 进豪宅" },
        { value: "cartheft", label: "6-4 扒车门" },
        { value: "office", label: "6-5 进办公室" },
      ],
    },
    {
      value: "7",
      label: "7n - 武装抢劫",
      children: [
        { value: "swiftrobbery", label: "7-1 抢了就跑" },
        { value: "thoroughrobbery", label: "7-2 一个不留" },
        { value: "swiftconvenient", label: "7-3 小卖铺行动" },
        { value: "thoroughconvenient", label: "7-4 小卖铺打砸抢" },
        { value: "swiftbank", label: "7-5 太平洋银行差事" },
        { value: "thoroughbank", label: "7-6 太平洋银行差事（困难）" },
        { value: "swiftcar", label: "7-7 劫车" },
        { value: "thoroughcar", label: "7-8 洗劫一空" },
      ],
    },
    {
      value: "8",
      label: "8n - 贩毒",
      children: [
        { value: "cannabis", label: "8-1 运叶子" },
        { value: "amphetamines", label: "8-2 运安非他命" },
        { value: "cocaine", label: "8-3 运可卡因" },
        { value: "drugscanabis", label: "8-4 卖叶子" },
        { value: "drugspills", label: "8-5 卖小药丸" },
        { value: "drugscocaine", label: "8-6 卖可卡因" },
      ],
    },
    {
      value: "9",
      label: "9n - 网络投毒",
      children: [
        { value: "simplevirus", label: "9-1 简单病毒" },
        { value: "polymorphicvirus", label: "9-2 多态病毒" },
        { value: "tunnelingvirus", label: "9-3 隧道病毒" },
        { value: "armoredvirus", label: "9-4 加壳病毒" },
        { value: "stealthvirus", label: "9-5 隐形病毒" },
      ],
    },
    {
      value: "10",
      label: "10n - 刺杀",
      children: [
        { value: "assasination", label: "10-1 刺杀一个目标" },
        { value: "driveby", label: "10-2 驾车射击" },
        { value: "carbomb", label: "10-3 自爆卡车" },
        { value: "murdermobboss", label: "10-4 刺杀帮派头目" },
      ],
    },
    {
      value: "11",
      label: "11n - 放火",
      children: [
        { value: "home", label: "11-1 家里放火" },
        { value: "Carlot", label: "11-2 停车场纵火" },
        { value: "OfficeBuilding", label: "11-3 办公楼纵火" },
        { value: "aptbuilding", label: "11-4 公寓楼纵火" },
        { value: "warehouse", label: "11-5 烧仓库" },
        { value: "motel", label: "11-6 烧炮楼" },
        { value: "govbuilding", label: "11-7 烧政府办公楼" },
      ],
    },
    {
      value: "12",
      label: "12n - GTA",
      children: [
        { value: "parkedcar", label: "12-1 偷停着的车" },
        { value: "movingcar", label: "12-2 偷开着的车" },
        { value: "carshop", label: "12-3 偷展示的车" },
      ],
    },
    {
      value: "13",
      label: "13n - 点数店",
      children: [
        { value: "pawnshop", label: "13-1 点数店" },
        { value: "pawnshopcash", label: "13-2 抢点市现金" },
      ],
    },
    {
      value: "14",
      label: "14n - 伪造",
      children: [
        { value: "makemoney2", label: "14-1 印钞" },
        { value: "maketokens2", label: "14-2 造赌场代币" },
        { value: "makecard", label: "14-3 伪造信用卡" },
      ],
    },
    {
      value: "15",
      label: "15n - 绑架",
      children: [
        { value: "napkid", label: "15-1 绑小孩" },
        { value: "napwomen", label: "15-2 绑女人" },
        { value: "napcop", label: "15-3 绑警察" },
        { value: "napmayor", label: "15-4 绑市长" },
      ],
    },
    {
      value: "16",
      label: "16n - 军火走私",
      children: [
        { value: "trafficbomb", label: "16-1 走私火药" },
        { value: "trafficarms", label: "16-2 走私枪支" },
      ],
    },
    {
      value: "17",
      label: "17n - 定时炸弹",
      children: [
        { value: "bombfactory", label: "17-1 炸工厂" },
        { value: "bombbuilding", label: "17-2 炸大楼" },
      ],
    },
    {
      value: "18",
      label: "18n - 黑客",
      children: [
        { value: "hackbank", label: "18-1 黑银行" },
        { value: "hackfbi", label: "18-2 黑fbi" },
      ],
    },
  ];
  