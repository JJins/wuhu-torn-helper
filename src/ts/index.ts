import "reflect-metadata";
import App from "./App";
import EntryPoint from "./starter/EntryPoint";
import { Injectable } from "./container/Injectable";
import ClassName from "./container/ClassName";
import { Container } from "./container/Container";

@EntryPoint
@ClassName('Index')
@Injectable()
class Index {
    public static main() {
        Container.factory(App).run()
    }
}
