import Device from "../enum/Device";
import IGlobal from "../interface/IGlobal";
import { Injectable } from "../container/Injectable";
import ClassName from "../container/ClassName";
import Logger from "./Logger";

/**
 * 存储脚本用到的参数
 */
@Injectable()
@ClassName('Global')
export default class Global implements IGlobal {

    GM_xmlhttpRequest: Function = null;
    // 设备类型
    device: Device = window.innerWidth >= 1000 ? Device.PC : window.innerWidth <= 600 ? Device.MOBILE : Device.TABLET;
    unsafeWindow: Window & typeof globalThis = null;
    // document body 属性
    bodyAttrs: {
        'data-country'?: string;
        'data-celebration'?: string;
        'data-traveling'?: 'true' | 'false';
        'data-abroad'?: 'true' | 'false';
    } = null;
    // href: string = window.location.href;
    // 弹窗
    // popup_node: MyHTMLElement|Popup = null;
    /**
     * @deprecated 使用getInstance替代
     */
    // beer = null;
    // 留存的通知
    // notifies: NotifyWrapper = { count: 0 };
    // 海外库存
    // fStock = null;
    // 玩家名和数字id
    // player_info = null;
    // PDA运行环境
    // isPDA: boolean = false;
    // PDA自带apikey
    // PDA_APIKey: string = null;
    // 脚本版本
    // version: string = null;
    // window 副本
    // window: Window & typeof globalThis = window;

    constructor(
        // private readonly infoUtils: InfoUtils,
        private readonly logger: Logger,
    ) {
        if (typeof unsafeWindow !== 'undefined') {
            this.logger.info('存在unsafeWindow, 引入');
            this.unsafeWindow = unsafeWindow || null;
            window.addRFC = this.unsafeWindow.addRFC;
            window.getAction = this.unsafeWindow.getAction;
            window.initializeTooltip = this.unsafeWindow.initializeTooltip;
            window.renderMiniProfile = this.unsafeWindow.renderMiniProfile;
            window.ReactDOM = this.unsafeWindow.ReactDOM;
        }
        if (typeof GM_xmlhttpRequest === 'function') {
            // 上层调用如果使用eval此处GM_xmlhttpRequest可能不存在于window中
            this.GM_xmlhttpRequest = window.GM_xmlhttpRequest || GM_xmlhttpRequest || null;
        }
        // this.version = '$$WUHU_DEV_VERSION$$';
        // this.PDA_APIKey = '###PDA-APIKEY###';
        // this.isPDA = !this.PDA_APIKey.includes('###');
        // this.device = window.innerWidth >= 1000 ? Device.PC : window.innerWidth <= 600 ? Device.MOBILE : Device.TABLET;
        // this.player_info = this.infoUtils.getPlayerInfo();
        // this.popup_node = null;
        // this.notifies = { count: 0 };
        // this.href = window.location.href;
        this.bodyAttrs = {};

        for (let i = 0; i < document.body.attributes.length; i++) {
            let item = document.body.attributes.item(i);
            this.bodyAttrs[item.name] = item.value;
        }

        // 当窗口关闭时关闭所有还存在的通知
        // window.addEventListener(
        //     'beforeunload',
        //     () => {
        //         if (this.notifies.count !== 0) {
        //             for (let i = 0; i < this.notifies.count; i++) {
        //                 (this.notifies[i] !== null) && (this.notifies[i].close())
        //             }
        //         }
        //     }
        // );

        // this.logger.info('WH脚本参数初始化结束');
    }
}
