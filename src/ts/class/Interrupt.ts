import ClassName from "../container/ClassName";
import { Injectable } from "../container/Injectable";


@ClassName('Interrupt')
@Injectable()
export default class Interrupt {

    public conditionInterrupt() {
        let title: HTMLElement | { innerText: string } = (document.querySelector('#skip-to-content') ||
            document.querySelector('[href*="#skip-to-content"]')) as HTMLElement || { innerText: '' };
        let condition = (
            document.title.toLowerCase().includes('just a moment') ||
            title.innerText.toLowerCase().includes('please validate') ||
            document.querySelector('div.container div.cf .iAmUnderAttack') !== null
        );
        if (condition) throw new Error('芜湖');
    }
}
