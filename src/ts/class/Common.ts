// import depoHelper from "../func/module/depoHelper";
// import TravelHelper from "../feature/TravelHelper";
// import priceWatcherHandle from "../func/module/priceWatcherHandle";
// import CompanyHelper from "../feature/CompanyHelper";
// import AttackHelper from "./action/AttackHelper";
// import SidebarHelper from "../feature/SidebarHelper";
// import CommonUtils from "./utils/CommonUtils";
// import FetchUtils from "./utils/FetchUtils";
// import FetchEventCallback from "./action/FetchEventCallback";
// import globVars from "../globVars";
// import TranslateNew from "./action/TranslateNew";
// import ClassName from "../container/ClassName";
// import { Injectable } from "../container/Injectable";
// import LocalConfigWrapper from "./LocalConfigWrapper";
// import Logger from "./Logger";
// import BuyBeerHelper from "../feature/BuyBeerHelper";
// import ModuleLoader from "./ModuleLoader";
// import TornPDAUtils from "./utils/TornPDAUtils";
// import TravelItem from "../feature/TravelItem";
// import IconHelper from "../feature/IconHelper";
// import MsgWrapper from "./utils/MsgWrapper";
// import toThousands from "../func/utils/toThousands";
// import { WHIntervalLoader } from "../monitor/WHIntervalLoader";
//
// /**
//  * 脚本不区分页面的通用功能入口
//  */
// @Injectable()
// @ClassName('Common')
// export class Common {
//     private readonly logger = Logger.factory(Common)
//
//     constructor(
//         private readonly localConfigWrapper: LocalConfigWrapper,
//         // private readonly fetchEventCallback: FetchEventCallback,
//         // private readonly translateNew: TranslateNew,
//         // private readonly tornPDAUtils: TornPDAUtils,
//         // private readonly buyBeerHelper: BuyBeerHelper,
//         // private readonly fetchUtils: FetchUtils,
//         // private readonly moduleLoader: ModuleLoader,
//         private readonly msgWrapper: MsgWrapper,
//     ) {
//     }
//
//     public resolve(mainMethod) {
//         // window.setInterval(()=>this.msgWrapper.create('test',{sysNotify:true},'info'),2000);
//
//         // // fetch方法处理
//         // globVars.responseHandlers.push(
//         //     (...args: any[]) => this.fetchEventCallback.responseHandler.apply(this.fetchEventCallback, args)
//         // );
//         // // fetch方法处理-翻译
//         // globVars.responseHandlers.push(
//         //     (...args: any[]) => this.translateNew.responseHandler.apply(this.translateNew, args)
//         // );
//
//         // // 价格监控
//         // priceWatcherHandle(this.tornPDAUtils.isPDA(), this.tornPDAUtils.APIKey);
//
//         // 啤酒提醒
//         // if (this.localConfigWrapper.config._15Alarm) this.buyBeerHelper.start();
//
//         // this.moduleLoader.push(SidebarHelper);
//         // this.moduleLoader.push(TravelItem);
//         // this.moduleLoader.push(WHIntervalLoader)
//
//         /**
//          * 解决一直转圈(加载中)的问题
//          * All('script[src*="google"]')
//          * All('#gtm_tag')
//          * All('script[src*="chat/gonline"]')
//          * All('head script[nonce]')
//          */
//         // try {
//         //     if (document.readyState === 'interactive' && this.localConfigWrapper.config.SolveGoogleScriptPendingIssue) {
//         //         window.stop();
//         //         document.open();
//         //         let readyStateChangeHandler = () => {
//         //             this.logger.info('document.readyState', document.readyState);
//         //             if (document.readyState === 'complete') {
//         //                 document.removeEventListener('readystatechange', readyStateChangeHandler);
//         //                 mainMethod();
//         //                 throw new Error('页面已重载');
//         //             }
//         //         }
//         //         document.addEventListener('readystatechange', readyStateChangeHandler);
//         //         this.fetchUtils.fetchText(window.location.href).then(resp => {
//         //             let removed = resp;
//         //             [
//         //                 /<script id="gtm_tag">.+?<\/script>/ms,
//         //                 /<script async src="https:\/\/www\.google.+?<\/script>/ms,
//         //                 /<script nonce=".+?gtag.+?<\/script>/ms,
//         //                 /<script.+?google.+?\/script>/,
//         //             ].forEach(remove => {
//         //                 removed = removed.replace(remove, '');
//         //             });
//         //             this.logger.info({ removed });
//         //             document.write(removed);
//         //             document.close();
//         //         });
//         //     }
//         // } catch (e) {
//         //     this.logger.error('【解决一直转圈(加载中)的问题】错误',e)
//         // }
//
//         // // 存钱相关
//         // try {
//         //     depoHelper();
//         // } catch (e) {
//         //     this.logger.error('【存钱相关】错误',e)
//         // }
//
//         // // 飞行相关
//         // this.moduleLoader.push(TravelHelper);
//
//         // 战斗相关
//         // this.moduleLoader.push(AttackHelper);
//
//         // 公司助手
//         // this.moduleLoader.push(CompanyHelper);
//
//         // // 菜单
//         // this.moduleLoader.push(IconHelper);
//
//         // this.moduleLoader.load().then();
//
//         // // 自定义CSS
//         // if (this.localConfigWrapper.config.CustomCss) {
//         //     this.logger.info('应用自定义CSS');
//         //     CommonUtils.addStyle(this.localConfigWrapper.config.CustomCss);
//         // }
//         //
//         // // 现金变动提醒
//         // if (this.localConfigWrapper.config.CashChangeAlert) CommonUtils.elementReady("#user-money").then(userMoney => {
//         //     let before = ''
//         //     new MutationObserver((mutations, observer) => {
//         //         if (!this.localConfigWrapper.config.CashChangeAlert) {
//         //             observer.disconnect();
//         //             this.msgWrapper.create('现金变动提醒已关闭', { sysNotify: true });
//         //             return;
//         //         }
//         //         this.logger.info("现金变动提醒", mutations);
//         //         mutations.forEach(item => {
//         //             if (item.attributeName === 'data-money') {
//         //                 let change = userMoney.innerText
//         //                     .trim()
//         //                     .replaceAll(/[,$]/g, '')
//         //                 if (change !== before) {
//         //                     this.msgWrapper.create(
//         //                         '现金变动 ' + item.oldValue + ' --> ' + toThousands(change),
//         //                         { sysNotify: true }
//         //                     );
//         //                     before = change
//         //                 }
//         //             }
//         //         });
//         //     }).observe(userMoney, { attributes: true, attributeOldValue: true })
//         // });
//     }
// }
