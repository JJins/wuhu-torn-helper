import Popup from "../utils/Popup";
import { createApp } from "vue";
import ItemPrice from "../../../vue/ItemPrice.vue";
import { MENU_ITEM_TYPE } from "../../interface/MenuItem";

// 物品查价
export default {
    domType: MENU_ITEM_TYPE.BUTTON,
    domId: '',
    domText: '🍺 物品查价',
    clickFunc: () => {
        let app = createApp(ItemPrice);
        app.mount(
            new Popup('', '物品查价', () => app.unmount())
                .element
        );
    }
}
