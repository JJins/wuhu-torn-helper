import Alert from "../utils/Alert";
import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";

@ClassName('UpdateTranslateDict')
@Injectable()
export default class UpdateTranslateDict {

    public handle(): void {
        new Alert('计划中');
    }
}
