import Popup from "../utils/Popup";
import Alert from "../utils/Alert";
import DialogMsgBox from "../utils/DialogMsgBox";
import CommonUtils from "../utils/CommonUtils";
import { MenuItemConfig } from "../ZhongIcon";
import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";

@ClassName('AdditionalSettingsHandler')
@Injectable()
export default class AdditionalSettingsHandler {

    constructor(
        private readonly commonUtils: CommonUtils,
    ) {
    }

    public show(): void {
        let pop = new Popup('', '更多设定');

        let menuList: MenuItemConfig[] = [
            {
                domType: 'button', domId: '', domText: '清空设置', clickFunc() {
                    new DialogMsgBox('将清空所有芜湖助手相关设置并刷新页面，确定？', {
                        callback: () => {
                            localStorage.removeItem('wh_trv_alarm');
                            localStorage.removeItem('wh_trans_settings');
                            localStorage.removeItem('whuuid');
                            localStorage.removeItem('wh-gs-storage');
                            localStorage.removeItem('WHTEST');
                            new Alert('已清空，刷新页面');
                            window.location.reload();
                        }
                    });
                }
            },
            {
                domType: "button", domId: '', domText: '通知权限', clickFunc() {
                }
            },
            {
                domType: 'button', domId: '', domText: '第三方API通信权限', clickFunc() {
                }
            },
        ];
        menuList.forEach(i => pop.element.append(this.commonUtils.elemGenerator(i, pop.element)));
    }
}
