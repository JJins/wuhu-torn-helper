import { MenuItemConfig } from "../ZhongIcon";
import Timer from "../utils/Timer";
import BuyBeerHelper from "../../feature/BuyBeerHelper";
import UpdateTranslateDict from "./UpdateTranslateDict";
import landedRedirect from "../../func/module/landedRedirect";
import Alert from "../utils/Alert";
import ViewLogsHandler from "./ViewLogsHandler";
import AdditionalSettingsHandler from "./AdditionalSettingsHandler";
import Popup from "../utils/Popup";
import CommonUtils from "../utils/CommonUtils";
import CustomCssHandler from "./CustomCssHandler";
import Provider from "../provider/Provider";
import { MENU_ITEM_TYPE } from "../../interface/MenuItem";
import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import { Container } from "../../container/Container";
import Logger from "../Logger";

@ClassName('SettingsHandler')
@Injectable()
class SettingsHandler extends Provider {
    className = 'SettingsHandler';

    private list: MenuItemConfig[] = [];

    constructor(
        private readonly buyBeerHelper: BuyBeerHelper,
        private readonly commonUtils: CommonUtils,
        private readonly updateTranslateDict: UpdateTranslateDict,
        private readonly customCssHandler: CustomCssHandler,
        private readonly viewLogsHandler: ViewLogsHandler,
        private readonly additionalSettingsHandler: AdditionalSettingsHandler,
        private readonly logger: Logger,
    ) {
        super();
        this.constructWuhuSettingList();
    }

    public show(): void {
        let startTime = new Timer();
        this.logger.info('构造设置开始');
        let pop = new Popup(CommonUtils.loading_gif_html(), '芜湖助手设置');
        window.setTimeout(() => {
            let tmp = document.createElement('div');
            tmp.classList.add('gSetting');
            this.list.forEach(set => this.commonUtils.elemGenerator(set, tmp));
            // 本日不提醒
            tmp.querySelector('#wh-qua-alarm-check-btn')
                .addEventListener('click', () => this.buyBeerHelper.skip_today());
            pop.getElement().innerHTML = '';
            pop.getElement().appendChild(tmp);
            (window.initializeTooltip) && (window.initializeTooltip('#wh-popup-cont', 'white-tooltip'));
            this.logger.info('构造设置结束 ' + startTime.getTimeMs());
        }, 0)
    }

    // 设置
    private constructWuhuSettingList(): SettingsHandler {
        let timer = new Timer();
        this.logger.info('构造设置列表开始');
        const date = new Date();

        let beer = this.buyBeerHelper;

        let list = this.list;

        // 12月时加入圣诞小镇选项
        if (date.getMonth() === 11) {
            list.push({
                domType: 'plain',
                domId: '',
                domHTML: '圣诞小镇',
                tagName: 'h4',
            })
            list.push({
                domType: 'checkbox',
                domId: 'wh-xmastown-wt',
                domText: ' 圣诞小镇攻略',
                dictName: 'xmasTownWT',
                isHide: true,
            });
            list.push({
                domType: 'checkbox',
                domId: 'wh-xmastown-notify',
                domText: ' 圣诞小镇物品提示',
                dictName: 'xmasTownNotify',
                isHide: true,
            });
        }

        // 翻译
        list.push({
            domType: 'plain',
            domId: '',
            domHTML: '翻译',
            tagName: 'h4',
        });
        // 开启翻译
        list.push({
            domType: 'checkbox',
            domId: 'wh-trans-enable',
            domText: ' 开启翻译',
            dictName: 'transEnable',
            isHide: true,
        });
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 新翻译',
            dictName: 'transNew',
            tip: '改进后的翻译，更好的性能',
        });
        // 更新翻译词库
        list.push({
            domType: 'button',
            domId: '',
            domText: '更新翻译词库',
            isTornBtn: true,
            clickFunc: () => this.updateTranslateDict.handle()
        });
        // 收集数据以改进翻译质量
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 收集数据以改进翻译质量',
            dictName: 'CollectPlayerData',
            tip: '未实现功能<br/>收集玩家基础信息与页面上(未翻译)的内容上传以改进翻译'
        });

        // 战斗
        list.push({
            domType: 'plain',
            domId: '',
            domHTML: '战斗',
            tagName: 'h4',
        });
        // 光速拔刀
        list.push({
            domType: 'select',
            domId: 'wh-quick-attack-index',
            domText: '光速拔刀 ',
            domSelectOpt: [
                {
                    domVal: 'pri',
                    domText: '主手',
                },
                {
                    domVal: 'sec',
                    domText: '副手',
                },
                {
                    domVal: 'wea',
                    domText: '近战',
                },
                {
                    domVal: 'gre',
                    domText: '手雷',
                },
                {
                    domVal: 'fis',
                    domText: '拳头',
                },
                {
                    domVal: 'kic',
                    domText: '脚踢',
                },
                {
                    domVal: 'none',
                    domText: '关闭',
                },
            ],
            dictName: 'quickAttIndex',
            tip: '将Start Fight按钮移动到指定格子上',
        });
        // 光速跑路
        list.push({
            domType: 'select',
            domId: 'wh-quick-mug',
            domText: '光速跑路 ',
            domSelectOpt: [
                {
                    domVal: 'leave',
                    domText: '跑路(LEAVE)',
                },
                {
                    domVal: 'mug',
                    domText: '打劫(MUG)',
                },
                {
                    domVal: 'hosp',
                    domText: '住院(HOSP)',
                },
                {
                    domVal: 'none',
                    domText: '关闭',
                },
            ],
            dictName: 'quickFinishAtt',
            tip: '将结束后指定按钮移动到上面指定的格子上',
        });
        // 攻击链接转跳
        list.push({
            domType: 'checkbox',
            domId: 'wh-attack-relocate',
            domText: ' 真·攻击界面转跳',
            dictName: 'attRelocate',
            tip: '在无法打开攻击界面的情况下依然可以转跳到正确的攻击页面，（失效，Ched疑似已移除此转跳）',
        });
        // 盯梢模式强度
        list.push({
            domType: 'select',
            domId: '',
            domText: '盯梢模式强度 ',
            domSelectOpt: [
                { domVal: '550', domText: '强' },
                { domVal: '950', domText: '标准' },
                { domVal: '1450', domText: '弱' }
            ],
            dictName: 'WatchTargetFreq',
            tip: '越强盯得越紧（刷新越快），越容易触发Torn大流量保护机制<br/>强 ~0.6s<br/>标准 ~1s<br/>弱 ~1.5s',
        })
        // 危险行为⚠️
        // if (WuhuConfig.get('dangerZone') === true) {
        //     // 攻击界面自刷新
        //     list.push({
        //         domType: 'select',
        //         domId: 'wh-attack-reload',
        //         domText: '⚠️攻击界面自动刷新 ',
        //         dictName: 'attReload',
        //         domSelectOpt: [
        //             {
        //                 domVal: 'none',
        //                 domText: '无间隔',
        //             },
        //             {
        //                 domVal: '1',
        //                 domText: '约1s',
        //             },
        //             {
        //                 domVal: '2',
        //                 domText: '约2s',
        //             },
        //             {
        //                 domVal: '3',
        //                 domText: '约3s',
        //             },
        //             {
        //                 domVal: '4',
        //                 domText: '约4s',
        //             },
        //             {
        //                 domVal: '5',
        //                 domText: '约5s',
        //             },
        //             {
        //                 domVal: 'disabled',
        //                 domText: '关闭',
        //             },
        //         ],
        //         isHide: true,
        //         tip: '危险功能：接机时常用，将自动刷新页面直到目标落地',
        //     });
        //     // 自动开打和结束
        //     list.push({
        //         domType: 'checkbox',
        //         domId: 'wh-auto-start-finish',
        //         domText: ' ⚠️自动开打和结束',
        //         dictName: 'autoStartFinish',
        //         tip: '脚本将会自动按下战斗和结束按钮',
        //         isHide: true,
        //     });
        // } else {
        //     WuhuConfig.set('autoStartFinish', false)
        //     WuhuConfig.set('attReload', 6)
        // }

        // 飞行
        list.push({
            domType: 'plain',
            domId: '',
            domHTML: '飞行',
            tagName: 'h4',
        });
        // 起飞警告
        list.push({
            domType: 'checkbox',
            domId: 'wh-energy-alert',
            domText: ' 起飞爆E警告',
            dictName: 'energyAlert',
            tip: '起飞前计算来回是否会爆体，红字警告',
            isHide: true,
        });
        // 飞行闹钟
        list.push({
            domType: 'checkbox',
            domId: 'wh-trv-alarm-check',
            domText: ' 飞行闹钟',
            dictName: 'trvAlarm',
            tip: '(仅PC) 飞行页面将显示一个内建的闹钟，落地前声音提醒，需要打开浏览器声音权限',
            isHide: true,
        });
        // 海外警告
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 海外警告',
            dictName: 'abroadWarning',
            tip: '海外落地后每30秒通知警告',
        });
        // 落地转跳
        list.push({
            domType: 'button', domId: '',
            isTornBtn: true, domText: '落地转跳', clickFunc: landedRedirect
        });

        // 公司
        list.push({
            domType: 'plain',
            domId: '',
            domHTML: '公司',
            tagName: 'h4',
        });
        // 浮动存钱框
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 浮动存钱框',
            dictName: 'floatDepo',
            tip: '打开公司或帮派的存钱页面后存钱框将浮动显示',
        });
        // 公司转跳存钱
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 公司转跳存钱',
            dictName: 'companyRedirect',
            tip: '打开公司页面时自动打开存钱选项卡',
        });
        // 收起公司冰蛙效率表
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 收起公司冰蛙效率表',
            dictName: 'companyBWCollapse',
            tip: '开启后可手动显示隐藏冰蛙公司表格',
        });
        // 任何位置一键存钱
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 任何位置一键存钱',
            dictName: 'companyDepositAnywhere',
            tip: '在所有页面显示一键存钱按钮，Torn OK状态下可用，此功能未完全测试无害，使用请慎重',
        });
        // 火车检测开关
        list.push({
            domType: 'checkbox',
            domId: null,
            domText: '火车检测',
            dictName: 'CHTrainsDetectSwitch',
            tip: '检测明日火车是否会超出上限，需要系统通知权限'
        });

        // 啤酒
        list.push({
            domType: 'plain',
            domId: '',
            domHTML: '啤酒',
            tagName: 'h4',
        });
        // 啤酒提醒
        list.push({
            domType: 'checkbox',
            domId: 'wh-qua-alarm-check',
            domText: '<span> 啤酒提醒 </span><button id="wh-qua-alarm-check-btn" class="torn-btn">今日不提醒</button>',
            dictName: '_15Alarm',
            tip: '每小时的整15分钟的倍数时通知提醒抢啤酒或者血包',
            isHide: true,
            changeEv: function (ev) {
                ev.target.checked ? beer.start() : beer.stop();
            },
        });
        // 啤酒提醒状态
        list.push({
            domType: 'button',
            domId: '',
            isTornBtn: true,
            domText: '啤酒提醒状态',
            clickFunc: function () {
                new Alert(`啤酒提醒${ beer.status() }`);
            }
        });
        // 啤酒提醒时间
        list.push({
            domType: 'button',
            domId: '',
            domText: '啤酒提醒时间设定',
            isTornBtn: true,
            // tip: '通知提前时间',
            clickFunc: () => this.buyBeerHelper.setTimeHandler()
        });

        // 个人资料
        list.push({
            domType: 'plain',
            domId: '',
            domHTML: '个人资料',
            tagName: 'h4',
        });
        list.push({
            domType: 'checkbox',
            domText: '隐藏个人资料头像',
            dictName: 'HideProfileImg',
            domId: ''
        });
        list.push({
            domType: 'checkbox',
            domText: '显示曾用名',
            dictName: 'ShowNameHistory',
            domId: ''
        });
        list.push({
            domType: 'checkbox',
            domText: '迷你资料卡显示上次行动时间',
            dictName: 'ShowMiniProfLastAct',
            domId: ''
        });

        // 侧边栏
        list.push({
            domType: 'plain',
            domId: '',
            domHTML: '侧边栏',
            tagName: 'h4',
        });
        // 4条转跳
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 4条转跳',
            dictName: 'barsRedirect',
            tip: '点击4条时转跳对应页面，手机端不建议开启',
        });
        // 添加隐藏边栏按钮
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 添加隐藏边栏按钮',
            dictName: 'HideSidebarBtn',
            tip: '仅PC'
        });
        // 现金变动提醒
        list.push({
            domType: 'checkbox',
            domId: '',
            domText: ' 现金变动提醒',
            dictName: 'CashChangeAlert',
            tip: '插件图标下方显示现金、现金变动时通知提醒<br/>注: 刷新页面生效、系统级通知需要浏览器权限'
        });

        // 其他
        list.push({
            domType: 'plain',
            domId: '',
            domHTML: '其他',
            tagName: 'h4',
        });
        // 任务助手
        list.push({
            domType: 'checkbox',
            domId: 'wh-mission-lint',
            domText: ' 任务助手',
            dictName: 'missionHint',
            tip: 'Duke任务的一些中文小提示',
            isHide: true,
        });
        // 捡垃圾助手
        list.push({
            domType: 'checkbox',
            domId: 'wh-city-finder',
            domText: ' 捡垃圾助手',
            dictName: 'cityFinder',
            tip: '城市地图中放大显示物品并且估计价值',
            isHide: true,
        });
        // 快速crime
        list.push({
            domType: 'checkbox',
            domId: 'wh-quick-crime',
            domText: ' 快速犯罪',
            dictName: 'quickCrime',
            tip: '显示快捷操作按钮，目前不支持自定义',
            isHide: true,
        });
        // 叠E保护
        list.push({
            domType: 'checkbox',
            domId: 'wh-SEProtect-check',
            domText: ' 叠E保护',
            dictName: 'SEProtect',
            tip: '隐藏健身房的锻炼按钮，防止误操作',
            isHide: true,
        });
        // PT一键购买
        list.push({
            domType: 'checkbox',
            domId: 'wh-ptQuickBuy-check',
            domText: ' PT一键购买',
            dictName: 'ptQuickBuy',
            tip: 'PT市场页面购买时跳过确认',
            isHide: true,
        });
        // 搜索页空白占位区
        list.push({
            domType: 'checkbox',
            domId: null,
            domText: ' 搜索页空白占位区',
            dictName: 'SearchPagePlaceholder',
        });
        // 解决一直转圈(加载中)的问题
        list.push({
            domType: 'checkbox',
            domId: null,
            domText: ' 解决一直转圈(加载中)的问题',
            dictName: 'SolveGoogleScriptPendingIssue',
            tip: 'Torn引入Google登陆后可能会出现这种问题<br/>启用功能影响:<br/>页面加载速度变慢，与其他插件冲突，如遇到问题请关闭<br/>并使用其他更科学的方法解决'
        });
        // 记住图标位置
        list.push({
            domType: 'checkbox',
            domId: null,
            domText: ' 记住图标位置',
            dictName: 'SaveIconPosition',
            tip: '记住图标移动后的位置'
        });
        // 清除多余的脚本
        // list.push({
        //     domType: 'checkbox',
        //     domId: '',
        //     domText: ' 清除多余的脚本',
        //     dictName: 'removeScripts',
        //     tip: '清除Google相关脚本、顶部横幅等',
        // });

        // dev
        list.push({
            domType: 'checkbox',
            domId: 'wh-dev-mode',
            domText: '开发模式',
            dictName: 'isDev',
            isHide: true,
        });
        // 自定义CSS
        list.push({
            domType: 'button',
            domId: null,
            domText: '自定义CSS',
            isTornBtn: true,
            clickFunc: () => this.customCssHandler.handle()
        });
        // 查看logs
        list.push({
            domType: 'button',
            domId: null,
            domText: '查看日志',
            isTornBtn: true,
            clickFunc: () => this.viewLogsHandler.handle()
        });
        // 更多设定
        list.push({
            domType: 'button', domId: 'wh-otherBtn', domText: '更多设定',
            isTornBtn: true,
            clickFunc: () => this.additionalSettingsHandler.show()
        });

        this.logger.info('构造设置列表结束' + timer.getTimeMs());
        return this;
    }
}

export default {
    domType: MENU_ITEM_TYPE.BUTTON,
    domText: '⚙️ 助手设置',
    clickFunc: () => Container.factory(SettingsHandler).show()
};
