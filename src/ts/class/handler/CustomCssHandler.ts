import Popup from "../utils/Popup";
import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import LocalConfigWrapper from "../LocalConfigWrapper";

@ClassName('CustomCssHandler')
@Injectable()
export default class CustomCssHandler {

    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper,
    ) {
    }

    public handle(): void {
        let pop = new Popup('<div><textarea></textarea><button class="torn-btn">保存</button><style>#wh-popup textarea{display: block;}</style></div>', '自定义CSS');
        let textarea = pop.element.querySelector('textarea');
        let button = pop.element.querySelector('button');
        textarea.value = this.localConfigWrapper.config.CustomCss || '';
        button.addEventListener('click', () => {
            this.localConfigWrapper.config.CustomCss = textarea.value || '';
        });
    }
}
