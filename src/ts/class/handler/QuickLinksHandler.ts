import CommonUtils from "../utils/CommonUtils";
import QUICK_LINK_CSS from "../../../static/css/quick_link.module.css";
import Popup from "../utils/Popup";
import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";

@ClassName('QuickLinksHandler')
@Injectable()
export default class QuickLinksHandler {

    private styleAdded: boolean = false;
    private list = [];

    constructor(
        private readonly commonUtils: CommonUtils,
    ) {
        let list = this.list;
        // 生存手册
        list.push({
            name: '生存手册',
            url: 'https://docs.qq.com/doc/DTVpmV2ZaRnB0RG56',
            new_tab: true,
            img: 'https://www.torn.com/images/items/293/medium.png',
        });
        // 买啤酒
        list.push({
            name: '抢啤酒',
            url: 'https://www.torn.com/shops.php?step=bitsnbobs',
            new_tab: true,
            img: 'https://www.torn.com/images/items/180/medium.png',
        });
        // 买XAN
        list.push({
            name: '买XAN',
            url: 'https://www.torn.com/imarket.php#/p=shop&step=shop&type=&searchname=Xanax',
            new_tab: true,
            img: 'https://www.torn.com/images/items/206/medium.png',
        });
        // 起飞
        list.push({
            name: '起飞',
            url: 'https://www.torn.com/travelagency.php',
            new_tab: true,
            img: 'https://www.torn.com/images/items/396/medium.png',
        });
        // 买PT
        list.push({
            name: '买PT',
            url: 'https://www.torn.com/pmarket.php',
            new_tab: true,
            img: 'https://www.torn.com/images/items/722/medium.png',
        });
        // 租PI
        list.push({
            name: '租PI',
            url: 'https://www.torn.com/properties.php?step=rentalmarket#/property=13',
            new_tab: false,
            img: 'https://www.torn.com/images/v2/properties/350x230/350x230_default_private_island.png',
        });
        // 找工作
        list.push({
            name: '找工作',
            url: 'https://www.torn.com/joblist.php#!p=main',
            new_tab: false,
            img: 'https://www.torn.com/images/items/421/medium.png',
        });
        // 下悬赏
        list.push({
            name: '下悬赏',
            url: 'https://www.torn.com/bounties.php#/p=add',
            new_tab: false,
            img: 'https://www.torn.com/images/items/431/medium.png',
        });
    }

    public handle(): void {
        if (!this.styleAdded) {
            this.commonUtils.styleInject(QUICK_LINK_CSS);
            this.styleAdded = true;
        }
        const list = this.list;
        let insert = '<p>';
        list.forEach(el => {
            insert += `<a href="${ el.url }"${ el.new_tab ? ' target="_blank"' : '' }><span class="wh-link-collection-img" style="background: url(${ el.img })"></span><span>${ el.name }</span></a>`;
        });
        insert += '</p>'
        let popup = new Popup(insert, '常用链接');
        popup.element.classList.add('wh-link-collection-cont');
        popup.element.addEventListener('click', ev => {
            let target = ev.target as HTMLElement;
            if (target.tagName.toLowerCase() === 'a' || target.tagName.toLowerCase() === 'span') {
                popup.close();
            }
        });
    }
}