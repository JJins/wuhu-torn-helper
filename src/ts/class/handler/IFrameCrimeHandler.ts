// import CommonUtils from "../utils/CommonUtils";
// import Popup from "../utils/Popup";
// import QUICK_CRIMES_HTML from "../../../static/html/quick_crimes.html";
// import ClassName from "../../container/ClassName";
// import { Injectable } from "../../container/Injectable";
//
// @ClassName('IFrameCrimeHandler')
// @Injectable()
// export default class IFrameCrimeHandler {
//
//     public handle(): void {
//         // 弹出小窗口
//         const ifHTML = `<iframe src="/crimes.php?step=main" style="width:100%;max-width: 450px;margin: 0 auto;display: none;height: 340px;"></iframe>`;
//         const popup_insert = `<p>加载中请稍后${ CommonUtils.loading_gif_html() }</p><div id="wh-quick-crime-if-container"></div>`;
//         const $popup = new Popup(popup_insert, '小窗快速犯罪').getElement();
//         // 运行状态node
//         let loading_node = $popup.querySelector('p:first-of-type');
//         // if容器
//         const if_cont = $popup.querySelector('#wh-quick-crime-if-container');
//         if_cont.innerHTML = ifHTML;
//
//         // if内未加载脚本时插入的快捷crime node
//         const mobile_prepend_node = document.createElement('div');
//         mobile_prepend_node.classList.add('wh-translate');
//         mobile_prepend_node.innerHTML = QUICK_CRIMES_HTML;
//
//         // if对象加载后运行
//         let cIframe = $popup.querySelector('iframe');
//
//         // 加载状态
//         const if_onload_func = () => {
//             // if内部文档对象
//             const ifDocu = cIframe.contentWindow.document;
//             // 内部插件运行flag
//             const ifWH = cIframe.contentWindow.WHTRANS;
//             // 文档加载完成后移除
//             if (!!loading_node) loading_node.remove();
//             // 文档加载完成后才显示if
//             cIframe.style.display = 'block';
//             // 验证码flag
//             const isValidate = ifDocu.querySelector('h4#skip-to-content').innerText.toLowerCase().includes('validate');
//             // 如果iframe内部未运行脚本
//             if (ifWH === undefined) {
//                 // 隐藏顶部
//                 CommonUtils.elementReady('#header-root', ifDocu).then(e => e.style.display = 'none');
//                 // 隐藏4条
//                 CommonUtils.elementReady('#sidebarroot', ifDocu).then(e => e.style.display = 'none');
//                 // 隐藏聊天
//                 CommonUtils.elementReady('#chatRoot', ifDocu).then(e => e.style.display = 'none');
//                 // 非验证码页面隐藏滚动条
//                 if (!isValidate) ifDocu.body.style.overflow = 'hidden';
//                 // 调整容器位置
//                 CommonUtils.elementReady('.content-wrapper', ifDocu).then(elem => {
//                     // 加入
//                     elem.prepend(mobile_prepend_node);
//                     elem.style.margin = '0px';
//                     elem.style.position = 'absolute';
//                     elem.style.top = '-35px';
//                     new MutationObserver((m, o) => {
//                         o.disconnect();
//                         if (!elem.querySelector('.wh-translate')) elem.prepend(mobile_prepend_node);
//                         o.observe(elem, { childList: true, subtree: true });
//                     })
//                         .observe(elem, { childList: true, subtree: true });
//                 });
//                 // 隐藏返回顶部按钮
//                 CommonUtils.elementReady('#go-to-top-btn button', ifDocu).then(e => e.style.display = 'none');
//             }
//         };
//         cIframe.onload = if_onload_func;
//
//         // 超时判断
//         let time_counter = 0;
//         let time_out_id = window.setInterval(() => {
//             loading_node = $popup.querySelector('p:first-of-type');
//             if (!loading_node) {
//                 clearInterval(time_out_id);
//                 time_out_id = undefined;
//                 return;
//             }
//             time_counter++;
//             if (time_counter > 0 && !loading_node.querySelector('button')) {
//                 const reload_btn = document.createElement('button');
//                 reload_btn.innerHTML = '重新加载';
//                 reload_btn.onclick = () => {
//                     reload_btn.remove();
//                     time_counter = 0;
//                     if_cont.innerHTML = null;
//                     if_cont.innerHTML = ifHTML;
//                     cIframe = $popup.querySelector('iframe');
//                     cIframe.onload = if_onload_func;
//                 };
//                 loading_node.append(reload_btn);
//             }
//         }, 1000);
//     }
// }