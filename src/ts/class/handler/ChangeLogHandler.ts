// import Popup from "../utils/Popup";
// import CommonUtils from "../utils/CommonUtils";
// import MDUtils from "../utils/MDUtils";
// import { MENU_ITEM_TYPE } from "../../interface/MenuItem";
// import Provider from "../provider/Provider";
// import ClassName from "../../container/ClassName";
// import { Injectable } from "../../container/Injectable";
// import { Container } from "../../container/Container";
// import Logger from "../Logger";
//
// /**
//  * @deprecated
//  */
// @ClassName('ChangeLogHandler')
// @Injectable()
// export class ChangeLogHandler extends Provider {
//     constructor(
//         private readonly mdUtils: MDUtils,
//         private readonly logger: Logger,
//     ) {
//         super();
//     }
//
//     public show(): void {
//         let popup = new Popup(
//             '更新历史：<br/><a target="_blank" href="https://gitlab.com/JJins/wuhu-torn-helper/-/blob/dev/CHANGELOG.md">https://gitlab.com/JJins/wuhu-torn-helper/-/blob/dev/CHANGELOG.md</a><br/>',
//             '更新历史'
//         ).element;
//         popup.classList.add('wh-changeLog');
//         let progressBar = document.createElement('div');
//         progressBar.style.height = '2px';
//         progressBar.style.width = '1%';
//         progressBar.style.backgroundColor = 'red';
//         let progressText = document.createElement('p');
//         progressText.innerText = '加载更新文件……';
//         progressText.style.textAlign = 'center';
//         let style = document.createElement('style');
//         style.innerHTML = `.wh-changeLog h2,.wh-changeLog h3,.wh-changeLog h4 {margin:8px 0;}.wh-changeLog li{list-style: inside;}`;
//
//         popup.append(progressBar, progressText, style);
//
//         CommonUtils
//             .COFetch('https://gitlab.com/JJins/wuhu-torn-helper/-/raw/dev/CHANGELOG.md?' + performance.now())
//             .then(update => {
//                 progressBar.style.width = '60%';
//                 progressText.innerText = '解析中……';
//                 let md = this.mdUtils.parse(update);
//                 popup.append(md);
//                 progressBar.style.width = '100%';
//                 progressText.innerText = '加载完成';
//
//                 window.setTimeout(() => {
//                     progressBar.remove();
//                     progressText.remove()
//                 }, 3000);
//             })
//             .catch(e => {
//                 this.logger.error(e);
//                 progressBar.remove();
//                 progressText.innerText = '无法加载';
//             });
//     }
//
// }
//
// export default {
//     domType: MENU_ITEM_TYPE.BUTTON,
//     domText: '🐞 更新历史',
//     clickFunc: () => Container.factory(ChangeLogHandler).show()
// };
