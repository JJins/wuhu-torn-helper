/**
 * 收集数据上传
 */
export default class Uploader {

    /**
     * 上传数据
     * @param uid 玩家id
     * @param uname 玩家昵称 可选
     * @param meta_data 上传数据json格式
     */
    upload(uid: string, uname: string, meta_data: string) {
    }

    /**
     * 获取服务端的数据
     * @param key 请求的密钥
     */
    getCloudData(key:string):string {
        return null;
    }
}
