import { Injectable } from "../../container/Injectable";
import ClassName from "../../container/ClassName";
import Debug from "../provider/Debug";

@Injectable()
@ClassName('NetHighLvlWrapper')
export default class NetHighLvlWrapper {
    @Debug
    public async doGymTrain(type: BATTLE_STAT, count: number): Promise<string> {
        let rs: string;
        try {
            rs = await (await window.fetch(
                window.addRFC("https://www.torn.com/gym.php?step=train"),
                {
                    "headers": {
                        "accept": "*/*",
                        "content-type": "application/json",
                        "sec-ch-ua-mobile": "?0",
                        "sec-fetch-dest": "empty",
                        "sec-fetch-mode": "cors",
                        "sec-fetch-site": "same-origin",
                        "x-requested-with": "XMLHttpRequest"
                    },
                    "referrer": "https://www.torn.com/gym.php",
                    "referrerPolicy": "strict-origin-when-cross-origin",
                    "body": `{\"step\":\"train\",\"stat\":\"${ type }\",\"repeats\":${ count }}`,
                    "method": "POST",
                    "mode": "cors",
                    "credentials": "include"
                }
            )).text();
        } catch (e) {
            rs = e.message;
        }
        return rs;
    }

    public async doTravelFly(destId, key, type): Promise<string> {
        return await (await fetch(window.addRFC("https://www.torn.com/travelagency.php"), {
            "headers": {
                "accept": "*/*",
                "accept-language": "zh-CN,zh;q=0.9",
                "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-origin",
                "x-requested-with": "XMLHttpRequest"
            },
            "referrer": "https://www.torn.com/travelagency.php",
            "referrerPolicy": "strict-origin-when-cross-origin",
            "body": `step=travel&id=${ destId }&type=${ type }`,
            "method": "POST",
            "mode": "cors",
            "credentials": "include"
        })).text()
    }
}

export enum BATTLE_STAT {
    STR = 'strength',
    DEF = 'defense',
    SPD = 'speed',
    DEX = 'dexterity'
}
