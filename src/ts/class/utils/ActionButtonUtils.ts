import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import Logger from "../Logger";
import globVars from "../../globVars";

@ClassName('ActionButtonUtils')
@Injectable()
export default class ActionButtonUtils {
    private hasAdded: boolean = false;
    private readonly logger = Logger.factory(ActionButtonUtils)

    public add(txt: string, func: (ev: Event) => void = () => null): void {

        let added = { txt, func }
        globVars.buttons.push(added)

        this.logger.info({ globVars })
    }

}
