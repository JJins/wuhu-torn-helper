import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";

@ClassName('TornPDAUtils')
@Injectable()
export default class TornPDAUtils {
    private readonly _APIKey: string = '###PDA-APIKEY###';

    public get APIKey(): string {
        return this._APIKey;
    }

    public isPDA(): boolean {
        return !this._APIKey.startsWith('#');
    }
}
