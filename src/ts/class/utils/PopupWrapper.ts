import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import Popup from "./Popup";
import { InjectionKey } from "vue";

@ClassName('PopupWrapper')
@Injectable()
export default class PopupWrapper {
    public create(html: string, title: string, onClosing: () => unknown) {
        return new Popup(html, title, onClosing);
    }
}

export const PopupWrapperKey = Symbol('PopupWrapperKey') as InjectionKey<PopupWrapper>;
