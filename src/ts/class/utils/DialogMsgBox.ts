import DIALOG_MSG_BOX_HTML from "../../../static/html/dialog_msg_box.html";
import Logger from "../Logger";
import { Container } from "../../container/Container";

export default class DialogMsgBox {
    private static existed = false;
    private readonly container: HTMLElement;

    constructor(msg: string, opt: DialogMsgBoxOptions,
                private readonly logger: Logger = Container.factory(Logger)) {
        logger.info('创建DialogMsgBox', { msg, opt });
        let { title = '提示', callback, cancel } = opt;
        if (!callback) {
            logger.error('无callback');
            throw new Error('无callback');
        }
        if (DialogMsgBox.existed) {
            logger.error('无法创建DialogMsgBox：已存在');
            throw new Error('无法创建DialogMsgBox：已存在');
        }
        this.container = document.createElement('div');
        this.container.id = 'wh-dialog';
        this.container.innerHTML = DIALOG_MSG_BOX_HTML.replace('{{}}', title).replace('{{}}', msg);
        let [confirm, cancelBtn] = Array.from(this.container.querySelectorAll('button'));
        confirm.addEventListener('click', () => {
            callback(this.container);
            this.destroy();
        });
        cancelBtn.addEventListener('click', () => {
            cancel ? cancel() : null;
            this.destroy();
        });
        document.body.append(this.container);
        // this.hideChat();
        DialogMsgBox.existed = true;
    }

    // private hideChat() {
    //     document.querySelector('#chatRoot').classList.add('wh-hide');
    // }
    //
    // private showChat() {
    //     document.querySelector('#chatRoot').classList.remove('wh-hide');
    // }

    private destroy() {
        this.container.remove();
        // this.showChat();
        DialogMsgBox.existed = false;
    }
}

interface DialogMsgBoxOptions {
    title?: string;
    callback: Function;
    cancel?: Function;
}