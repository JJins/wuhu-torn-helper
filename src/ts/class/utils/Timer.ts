/**
 * 基于performance.now()包装的计时器
 */
export default class Timer {
    private readonly startTime: number;

    public constructor() {
        this.startTime = performance.now();
    }

    /**
     * 返回计时器计数，毫秒、以ms结束
     *
     * `'99ms'`
     */
    public getTimeMs(): string {
        return ((performance.now() - this.startTime) | 0) + 'ms';
    }
}
