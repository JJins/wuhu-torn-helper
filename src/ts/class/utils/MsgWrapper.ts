import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import IWHNotify from "../../interface/IWHNotify";
import { InjectionKey } from "vue";
import { ElMessage } from "element-plus";
import NotificationUtils from "./NotificationUtils";
import WindowActiveState from "../action/WindowActiveState";

@ClassName('MsgWrapper')
@Injectable()
export default class MsgWrapper {
    constructor(
        private readonly notificationUtils: NotificationUtils,
        private readonly windowActiveState: WindowActiveState,
    ) {
    }

    create(msg: string, options: IWHNotify = {}, type: 'info' | 'warning' | 'error' | 'success' = 'info') {
        if (!this.windowActiveState.get()) return null;
        if (options?.sysNotify) {
            this.notificationUtils.push(msg, options);
        }
        return ElMessage({
            message: msg,
            type,
            showClose: true,
            dangerouslyUseHTMLString: true,
        });
    }
}

export const MsgWrapperKey = Symbol('MsgWrapperKey') as InjectionKey<MsgWrapper>;
