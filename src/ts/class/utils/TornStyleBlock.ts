import Log from "../Log";
import MathUtils from "./MathUtils";
import { Container } from "../../container/Container";

export default class TornStyleBlock {
    private readonly baseElement: HTMLElement;
    private readonly headElement: HTMLElement;
    private readonly elem: HTMLElement;

    public constructor(
        private readonly title: string,
        private readonly mathUtils: MathUtils = Container.factory(MathUtils),
    ) {
        this.baseElement = document.createElement('div');
        this.headElement = document.createElement('div');
        this.elem = document.createElement('div');
        // let hr = document.createElement('hr');

        this.headElement.classList.add('title-black', 'm-top10', 'top-round');
        this.headElement.innerHTML = title;
        this.elem.classList.add('cont-gray', 'bottom-round');
        this.elem.style.padding = '0.5em';
        // hr.classList.add('delimiter-999', 'm-top10', 'm-bottom10');

        this.baseElement.append(this.headElement, this.elem);
        this.baseElement.id = 'WHTornStyleBlock' + mathUtils.getRandomInt(0, 100);
        this.baseElement.insertAdjacentHTML('beforeend', '<hr class="delimiter-999 m-top10 m-bottom10" />');
    }

    public append(...el: Element[]): TornStyleBlock {
        this.elem.append(...el);
        return this;
    }

    public appendChild(...el: Element[]): TornStyleBlock {
        return this.append(...el)
    }

    public getBase(): HTMLElement {
        return this.baseElement;
    }

    public getElement(): HTMLElement {
        return this.elem;
    }

    public insert2Dom(after: string = '#websocketConnectionData'): TornStyleBlock {
        let anchor = document.querySelector(after);
        if (anchor) {
            anchor.after(this.baseElement);
        } else {
            Log.error('[#websocketConnectionData] 不存在，无法将block加入dom树');
            throw '芜湖！[#websocketConnectionData] 不存在，无法将block加入dom树';
        }
        return this;
    }

    public remove(): void {
        this.baseElement.remove();
    }

    public setTitle(str: string): void {
        this.headElement.innerHTML = str;
    }

    public setContent(html: string): void {
        this.elem.innerHTML = html;
    }

    public querySelector(str: string): HTMLElement {
        return this.baseElement.querySelector(str);
    }

    public querySelectorAll(str: string): NodeListOf<HTMLElement> {
        return this.baseElement.querySelectorAll(str);
    }
}