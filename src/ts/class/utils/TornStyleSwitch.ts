import MathUtils from "./MathUtils";
import { Container } from "../../container/Container";

export default class TornStyleSwitch {
    private readonly baseElement;
    private readonly randomId;
    private readonly input;

    constructor(
        private readonly label: string,
        private readonly checked: boolean = false,
        private readonly mathUtils: MathUtils = Container.factory(MathUtils),
    ) {
        this.randomId = mathUtils.getRandomInt(100, 2000);
        this.baseElement = document.createElement('span');
        this.baseElement.id = 'WHSwitch' + this.randomId;
        this.baseElement.innerHTML = `<input class="checkbox-css" type="checkbox" id="WHCheck${ this.randomId }" ${ checked ? 'checked' : '' }/>
<label for="WHCheck${ this.randomId }" class="non-selection marker-css">${ label }</label>`;
        this.input = this.baseElement.querySelector('input') as HTMLInputElement;
    }

    public getBase(): HTMLElement {
        return this.baseElement
    };

    public getInput(): HTMLInputElement {
        return this.input;
    }

    public getHtml(): string {
        return this.baseElement.innerHTML;
    }
}