import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import { InjectionKey } from "vue";

@ClassName('MathUtils')
@Injectable()
export default class MathUtils {
    className = 'MathUtils';

    // 得到一个两数之间的随机整数
    public getRandomInt(min: number, max: number): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        //不含最大值，含最小值
        return Math.floor(Math.random() * (max - min)) + min;
    }
}

export const MathUtilsKey = Symbol('MathUtilsKey') as InjectionKey<MathUtils>
