import IWHNotify from "../../interface/IWHNotify";
import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import Logger from "../Logger";

@ClassName('NotificationUtils')
@Injectable()
export default class NotificationUtils {
    constructor(
        // TODO 循环依赖
        // private readonly global: Global,
        private readonly logger: Logger,
    ) {
    }

    private permission: boolean = window.Notification && window.Notification.permission === 'granted';

    public push(msg: string, options: IWHNotify = {}): void {
        // this.logger.info({msg, options})
        // let { notifies } = this.global;

        if (options.sysNotify && this.permission) {
            let tmpNode = document.createElement('p');
            tmpNode.innerHTML = msg;
            let notify = new Notification('芜湖助手', {
                body: this.logger.getTime() + '\r\n' + tmpNode.innerText,
                // requireInteraction: true,
                // renotify: true,
                // tag: '芜湖助手' + Utils.getRandomInt(0, 99),
            });
            // let id = notifies.count++;
            // notifies[id] = notify;
            // notify.addEventListener(
            //     'close',
            //     () => {
            //         notifies[id] = null;
            //     }
            // );
            notify.addEventListener(
                'click',
                () => {
                    options.sysNotifyClick ? options.sysNotifyClick() : null;
                    window.focus();
                }
            );
            // notify.addEventListener(
            //     'show',
            //     () => {
            //         // setTimeout(() => notify.close(), (options.timeout || 3) * 1000);
            //         Log.info('通知id: ', id)
            //     }
            // );
        }
    }
}
