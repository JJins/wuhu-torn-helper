import TornStyleBlock from "../utils/TornStyleBlock";
import TornStyleSwitch from "../utils/TornStyleSwitch";
import CommonUtils from "../utils/CommonUtils";
import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import LocalConfigWrapper from "../LocalConfigWrapper";
import IFeature from "../../man/IFeature";

@ClassName('SearchHelper')
@Injectable()
export default class SearchHelper implements IFeature {
    description(): string {
        return "搜索助手";
    }

    iStart(): void | Promise<void> {
        this.init()
    }

    urlExcludes(): RegExp[] {
        return [];
    }

    urlIncludes(): RegExp[] {
        return [/page\.php\?sid=UserList/];
    }

    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper,
    ) {
    }

    public init(): void {
        let block = new TornStyleBlock('搜索助手');
        let placeholderSwitch = new TornStyleSwitch('底部空白占位区');
        block.append(placeholderSwitch.getBase()).insert2Dom();
        CommonUtils.addStyle('body.WHSearchPagePlaceholder .content.logged-in {margin-bottom: 340px;}');
        let config = this.localConfigWrapper.config.SearchPagePlaceholder;
        placeholderSwitch.getInput().checked = config || false;
        config ? this.enable() : this.disable();
        placeholderSwitch.getInput().addEventListener('change', () => {
            placeholderSwitch.getInput().checked ? this.enable() : this.disable();
        });
    }

    private enable(): void {
        document.body.classList.add('WHSearchPagePlaceholder');
        this.localConfigWrapper.config.SearchPagePlaceholder = true;
    }

    private disable(): void {
        document.body.classList.remove('WHSearchPagePlaceholder');
        this.localConfigWrapper.config.SearchPagePlaceholder = false;
    }
}
