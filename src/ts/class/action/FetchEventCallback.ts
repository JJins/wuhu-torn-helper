import { MiniProfile } from "../../interface/responseType/MiniProfile";
import CommonUtils from "../utils/CommonUtils";
import Provider from "../provider/Provider";
import ResponseInject from "../../interface/ResponseInject";
import { Injectable } from "../../container/Injectable";
import ClassName from "../../container/ClassName";
import LocalConfigWrapper from "../LocalConfigWrapper";
import Logger from "../Logger";
import MsgWrapper from "../utils/MsgWrapper";
import { fetchYata } from "../../func/module/fetchYata";
import toThousands from "../../func/utils/toThousands";

/**
 * fetch 事件监听回调
 */
@Injectable()
@ClassName("FetchEventCallback")
export default class FetchEventCallback extends Provider implements ResponseInject {
    className = "FetchEventCallback";

    newNode = document.createElement('div')
    bsEstNode = document.createElement('div')

    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper,
        private readonly commonUtils: CommonUtils,
        private readonly logger: Logger,
        private readonly msgWrapper: MsgWrapper,
    ) {
        super();
    }

    /**
     * fetch 返回后处理
     * @param url
     * @param response
     */
    public responseHandler(url: string, response) {
        // mini profile 中添加上次动作
        if (url.startsWith('/page.php?sid=UserMiniProfile&userID')) {
            window.setTimeout(async () => {
                let cont = CommonUtils.querySelector('[class*=profile-mini-_userProfileWrapper___]');
                let resp: MiniProfile = response.json as MiniProfile;
                if (this.localConfigWrapper.config.ShowMiniProfLastAct) {
                    this.logger.info({ resp })
                    let formatted = this.commonUtils.secondsFormat(resp.user.lastAction.seconds);

                    (await cont).append(this.newNode);
                    this.newNode.innerText = '上次动作: ' + formatted;
                }
                if (this.localConfigWrapper.config.isBSEstMiniProfOn) {
                    const id = resp.user.userID
                    const apikey = localStorage.getItem('APIKey')
                    this.bsEstNode.innerHTML = `[BS估算] [${ id }]获取中...`;
                    (await cont).append(this.bsEstNode)
                    if (!apikey) {
                        this.bsEstNode.innerHTML = '[BS估算] 未配置APIKey，无法估算BS'
                        this.logger.error('MINI Profile bs估算失败: APIKey为空')
                    } else {
                        const bsData = fetchYata(id, apikey)
                        bsData.then(data => {
                            // 网速过慢时可能mini profile容器已更新新内容，与上次请求的用户数据不同，需要判断
                            if (this.bsEstNode.innerHTML.includes(resp.user.userID.toString())) {
                                this.bsEstNode.innerHTML = `[BS估算] ${ resp.user.playerName }[${ id }] ${ toThousands(data.total) }`
                            }
                        })
                            .catch(err => {
                                this.bsEstNode.innerHTML = `[BS估算] ${ err.message }`
                            })
                    }
                }
            }, 0);
        }
    }
}
