import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import Logger from "../Logger";
import NetHighLvlWrapper, { BATTLE_STAT } from "../utils/NetHighLvlWrapper";
import { InjectionKey } from "vue";
import MsgWrapper from "../utils/MsgWrapper";

type GymResponse = {
    success: boolean,
    // 成功才有 You gained 689,636.71 strength
    gainMessage?: string,
    message: string,
    text?: string,
    error?: string,
};

@ClassName("QuickGymTrain")
@Injectable()
export default class QuickGymTrain {
    constructor(
        private readonly logger: Logger,
        private readonly netHighLvlWrapper: NetHighLvlWrapper,
        private readonly msgWrapper: MsgWrapper,
    ) {
    }

    doTrain(type = BATTLE_STAT.STR, count = 199) {
        window.setTimeout(async () => {
            let resObj: GymResponse;
            try {
                resObj = JSON.parse(await this.netHighLvlWrapper.doGymTrain(type, count))
            } catch (e) {
                resObj = { success: false, message: '解析失败' };
                this.logger.error(e.stack || e.message || e);
            }
            let msgRs = resObj.success ? '成功' : '失败';
            let msgMsg = resObj.message || resObj.text || resObj.error;
            this.msgWrapper.create(
                '锻炼结果: ' + msgRs + '<br/>提示: ' + (resObj.gainMessage || msgMsg),
                {}, resObj.success ? 'success' : 'error'
            );
        }, 0);
    }
}

export const QuickGymTrainKey = Symbol('QuickGymTrainKey') as InjectionKey<QuickGymTrain>;
