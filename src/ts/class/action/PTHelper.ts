import TornStyleBlock from "../utils/TornStyleBlock";
import TornStyleSwitch from "../utils/TornStyleSwitch";
import { Injectable } from "../../container/Injectable";
import ClassName from "../../container/ClassName";
import LocalConfigWrapper from "../LocalConfigWrapper";
import IFeature from "../../man/IFeature";
import MsgWrapper from "../utils/MsgWrapper";

@Injectable()
@ClassName('PTHelper')
export default class PTHelper implements IFeature {
    private observer: MutationObserver;
    private usersPointSell: HTMLDivElement;

    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper,
        private readonly msgWrapper: MsgWrapper,
    ) {
    }

    description(): string {
        return "pt一键购买";
    }

    iStart(): void | Promise<void> {
        this.start()
    }

    urlExcludes(): RegExp[] {
        return [];
    }

    urlIncludes(): RegExp[] {
        return [/pmarket\.php/];
    }

    start() {

        this.observer = new MutationObserver(e => {
            for (const t of e) {
                t.addedNodes.forEach(e => 'LI' === (e as HTMLElement).tagName && this.removeConfirm(e))
            }
        });
        this.usersPointSell = document.querySelector<HTMLDivElement>('.users-point-sell');

        let block = new TornStyleBlock('PT一键购买').insert2Dom();
        let switcher = new TornStyleSwitch('开启');
        block.append(switcher.getBase());
        let toggle = switcher.getInput();
        toggle.checked = this.localConfigWrapper.config.ptQuickBuy;
        if (toggle.checked) {
            this.msgWrapper.create('一键购买已开启');
            for (const index in this.usersPointSell.children) {
                'LI' === this.usersPointSell.children[index].tagName && this.removeConfirm(this.usersPointSell.children[index])
            }
            this.observer.observe(this.usersPointSell, { childList: true })
        }
        toggle.addEventListener('change', () => {
            this.localConfigWrapper.config.ptQuickBuy = toggle.checked;
            if (toggle.checked) {
                for (const index in this.usersPointSell.children) {
                    'LI' === this.usersPointSell.children[index].tagName && this.removeConfirm(this.usersPointSell.children[index])
                }
                this.observer.observe(this.usersPointSell, { childList: true });
                this.msgWrapper.create('一键购买已开启');
            } else {
                for (const index in this.usersPointSell.children) {
                    'LI' === this.usersPointSell.children[index].tagName && this.rollbackConfirm(this.usersPointSell.children[index])
                }
                this.observer.disconnect();
                this.msgWrapper.create('一键购买已关闭');
            }
        });
    }

    private removeConfirm(elem): void {
        let el = elem.firstElementChild;
        el.classList.add('yes');
        let old_href = el.getAttribute('href');
        let new_href = old_href.replace(/=buy/, '=buy1').replace(/&points=\d{1,9}$/, '');
        el.setAttribute('href', new_href);
    }

    private rollbackConfirm(elem): void {
        let el = elem.firstElementChild;
        el.classList.remove('yes');
        let old_href = el.getAttribute('href');
        let new_href = old_href.replace(/=buy1/, '=buy');
        el.setAttribute('href', new_href);
    }
}
