import CommonUtils from "../utils/CommonUtils";
import TornStyleBlock from "../utils/TornStyleBlock";
import TornStyleSwitch from "../utils/TornStyleSwitch";
import ResponseInject from "../../interface/ResponseInject";
import globVars from "../../globVars";
import IUserProfileData from "../../interface/IUserProfileData";
import ClassName from "../../container/ClassName";
import { Injectable } from "../../container/Injectable";
import LocalConfigWrapper from "../LocalConfigWrapper";
import Logger from "../Logger";
import IFeature from "../../man/IFeature";
import { fetchYata } from "../../func/module/fetchYata";
import MsgWrapper from "../utils/MsgWrapper";
import toThousands from "../../func/utils/toThousands";
import { timePastFormat } from "../../func/utils/timePastFormat";

@ClassName('ProfileHelper')
@Injectable()
export default class ProfileHelper implements ResponseInject, IFeature {
    private block: TornStyleBlock;

    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper,
        private readonly commonUtils: CommonUtils,
        private readonly logger: Logger,
        private readonly msgWrapper: MsgWrapper,
    ) {
    }

    description(): string {
        return "个人资料页面辅助";
    }

    iStart(): void | Promise<void> {
        this.start()
    }

    urlExcludes(): RegExp[] {
        return [];
    }

    // 曾用名已检测过标记
    private task = true;

    urlIncludes(): RegExp[] {
        return [/profiles\.php\?XID=/];
    }

    start() {

        CommonUtils.addStyle('body.wh-hide_profile_img .profile-image a.profile-image-wrapper .img-wrap img{display:none;}');
        // let id = document.querySelector('link[rel="canonical"]').getAttribute('href').split('=')[1];
        let id = (new URL(window.location.href)).searchParams.get('XID');
        // id获取格式判断
        if (!this.commonUtils.isValidUid(id)) {
            this.logger.error('[ProfileHelper] id格式错误');
        }
        if (this.localConfigWrapper.config.HideProfileImg) {
            this.logger.info('[ProfileHelper] 隐藏头像');
            document.body.classList.toggle('wh-hide_profile_img');
        }
        this.block = new TornStyleBlock('芜湖助手').insert2Dom();
        // 隐藏头像
        try {
            let hideImgSwitch = new TornStyleSwitch('隐藏头像', this.localConfigWrapper.config.HideProfileImg);
            this.block.append(hideImgSwitch.getBase());
            hideImgSwitch.getInput().addEventListener('change', () => {
                document.body.classList.toggle('wh-hide_profile_img');
                this.localConfigWrapper.config.HideProfileImg = hideImgSwitch.getInput().checked;
            });
            if (this.localConfigWrapper.config.ShowNameHistory) {
                globVars.responseHandlers.push((...args: any[]) => this.responseHandler.apply(this, args));
            }
        } catch (e) {
            this.logger.error('隐藏头像时出错了', e.stack)
        }
        // bs估算
        if (this.localConfigWrapper.config.isBSEstProfOn) {
            try {
                const apikey = localStorage.getItem('APIKey')
                if (!apikey) {
                    this.msgWrapper.create('BS估算失败: 尚未设定APIKey', null, 'error')
                }
                const promise = fetchYata(parseInt(id), apikey)
                const domNode = document.createElement('div')
                domNode.innerHTML = 'BS估算中...'
                domNode.classList.add('mt-4')
                domNode.style.border = '1px solid green'
                domNode.style.padding = '2px'
                this.block.append(domNode)
                const buildType = { Offensive: '攻击型', Defensive: '防御型', Balanced: '平衡型' }
                promise.then(data => {
                    domNode.innerHTML = `<b>BS估算</b><br/>
BS: ${ toThousands(data.total) }<br/>
评分: ${ toThousands(data.score) }<br/>
风格: ${ buildType[data.type] }<br/>
偏差: ${ data.skewness }%<br/>
估算时间: ${ timePastFormat(Date.now() - data.timestamp * 1000) }前
`
                }).catch(err => {
                    domNode.innerHTML = 'BS估算出错了: ' + err.message
                    throw new TypeError('BS估算出错了: ' + err.message)
                })
            } catch (e) {
                this.msgWrapper.create('BS估算失败' + e.message, null, 'error')
                throw new TypeError('BS估算失败' + e.message)
            }
        }
    }

    responseHandler(url: string, body: { json: unknown; text: string; isModified: boolean }) {
        if (url.includes('profiles.php?step=getProfileData') && this.task) {
            // 曾用名
            const nameHistoryNode = document.createElement('p');
            nameHistoryNode.innerHTML = '曾用名：';
            this.block.append(nameHistoryNode);
            let resp = body.json as IUserProfileData;
            if (resp.userInformation.previousAliases.length > 0) { // 曾用名列表
                resp.userInformation.previousAliases.forEach(item => nameHistoryNode.innerHTML += item + ' ');
            } else {
                nameHistoryNode.innerHTML += '暂无';
            }
            let lastAction = -1
            let onlineStatusTitle = '-'
            if (resp.basicInformation?.lastAction.seconds) {
                lastAction = resp.basicInformation.lastAction.seconds
            }
            if (resp.basicInformation.icons) {
                for (let i = 0; i < resp.basicInformation.icons.length; i++) {
                    let item = resp.basicInformation.icons[i]
                    if (item.id === 1) {
                        onlineStatusTitle = '🟢️ 在线'
                        break
                    }
                    if (item.id === 62) {
                        onlineStatusTitle = '🟡 挂机'
                        break
                    }
                    if (item.id === 2) {
                        onlineStatusTitle = '⚪ 离线'
                        break
                    }
                }
            }
            const lastActionNode = document.createElement('p')
            lastActionNode.innerHTML = `${ onlineStatusTitle } ${ this.commonUtils.secondsFormat(lastAction) }`
            this.block.append(lastActionNode)
            this.task = false;
        }
    }
}
