const notifyKey = Symbol('ConfigNotifyKey');

class DefaultConfigType {
    // 开启翻译
    transEnable = false;

    @Notified()
    transNew = true;
    // 快速犯罪
    quickCrime = true;
    // 任务助手
    missionHint = true;
    // 小镇攻略
    xmasTownWT = true;
    // 小镇提醒
    xmasTownNotify = true;
    // 起飞爆e
    energyAlert = true;
    // 飞行闹钟
    trvAlarm = true;
    // 啤酒提醒
    _15Alarm = true;

    @Notified()
    _15_alarm_ignore: number[] = [];
    // 啤酒提醒时间
    _15AlarmTime = 50;
    // 捡垃圾助手
    cityFinder = false;

    // 叠E保护
    @Notified()
    SEProtect = false;
    // PT一键购买
    ptQuickBuy = false;
    // 光速拔刀 6-关闭
    quickAttIndex = 2;
    // 光速跑路 0-leave 1-mug 2-hos 3-关闭
    quickFinishAtt = 3;
    // 自动开打和结束
    autoStartFinish = false;
    // 攻击自刷新 0-无间隔 1-5s 6-关闭
    attReload = 6;
    /** @deprecated */
    attRelocate = false;
    // 价格监视
    priceWatcher = { xan: -1, pt: -1 };
    // 开发者模式
    isDev = false;
    // 4条转跳
    barsRedirect = true;
    // 浮动存钱框
    floatDepo = true;
    // 公司转跳存钱
    companyRedirect = true;
    // 收起公司冰蛙效率表
    companyBWCollapse = true;
    // 海外警告
    abroadWarning = true;

    // 落地转跳
    @Notified()
    landedRedirect = '';
    // 任何位置一键存钱
    companyDepositAnywhere = false;
    // 火车提醒时间
    CHTrainsDetect = 0;
    // 火车提醒开关
    CHTrainsDetectSwitch = true;

    // 隐藏个人资料头像
    @Notified()
    HideProfileImg = false;
    // 显示曾用名
    ShowNameHistory = true;
    // 盯梢模式强度 0-550 1-950 2-1450 ms
    WatchTargetFreq = 1;
    // 隐藏侧边栏
    HideSidebar = false;
    // 添加隐藏边栏按钮
    HideSidebarBtn = true;
    // 搜索页占位区
    SearchPagePlaceholder = true;

    // 解决一直转圈(加载中)的问题
    @Notified()
    SolveGoogleScriptPendingIssue = false;
    // 图标坐标
    IconPosition: Partial<{ x: number, y: number }> = {};
    // 记住图标位置
    SaveIconPosition = false;
    // 现金变动提醒
    CashChangeAlert = false;
    // 收集数据以改进翻译质量
    CollectPlayerData = true;
    // 迷你资料卡显示上次行动时间
    ShowMiniProfLastAct = true;

    // 登陆邮箱
    @Notified()
    autoLoginEmail = '';
    // 登陆密码
    autoLoginPwd = '';

    // 自定义css
    @Notified()
    CustomCss = '';

    monitorOn = ['drugCDMonitor']
    drugCDMonitorInterval = 60000

    // mini profile显示bs估算
    @Notified()
    isBSEstMiniProfOn = false
    // profile页面显示bs估算
    @Notified()
    isBSEstProfOn = true
}

export type Config = DefaultConfigType;

let config = new DefaultConfigType();

export default config as Readonly<Config>;

function Notified(f: boolean = true) {
    return Reflect.metadata(notifyKey, f);
}

export function isNotified(propertyKey: string): boolean {
    return Reflect.getMetadata(notifyKey, config, propertyKey);
}
