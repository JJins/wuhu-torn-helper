import { GetClassName } from "../../container/ClassName";
import Logger from "../Logger";
import { Container } from "../../container/Container";

/**
 * 方法装饰器
 * @param target
 * @param propertyKey
 * @param descriptor
 */
export default function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const logger: Logger = Container.factory(Logger);

    const original = descriptor.value;

    descriptor.value = function (...args) {
        // Log.info('[debug] 参数 ', JSON.stringify({
        //     class: target.className || target.name,
        //     method: propertyKey,
        //     args
        // }));

        let result;
        try {
            result = original.call(this, ...args) || null;
        } catch (err) {
            logger.error('[debug]', err.stack || err.message || err, '参数' + JSON.stringify({
                class: GetClassName(target) ?? target.name,
                method: propertyKey,
                args
            }));
        }
        logger.info(
            '[debug]',
            '参数' + JSON.stringify({
                class: GetClassName(target) ?? target.name,
                method: propertyKey,
                args
            }),
            '结果',
            { result }
        );
        return result;
    }
}
