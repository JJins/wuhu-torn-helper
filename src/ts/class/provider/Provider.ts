import ClassWithName from "../../interface/ClassWithName";

/**
 * 基类、单例
 * @deprecated
 */
export default class Provider implements ClassWithName {
    readonly className: string = 'Provider';
    // private static _instance;
    //
    // private static readonly pool = {};
    //
    // constructor(...args: unknown[]) {
    // }

    // 返回继承类的实例
    // public static getInstance<T extends typeof Provider>(this: T, ...args: unknown[]): InstanceType<T> {
    //     if (!this._instance) {
    //         let startTime = new Timer();
    //         this._instance = new this(...args);
    //         let thatName = this._instance.getClassName() || this.name;
    //         Log.info('实例已创建,', thatName, this._instance, '耗时' + startTime.getTimeMs());
    //         Provider.pool[thatName] = this._instance;
    //     }
    //     return this._instance;
    // }

    // public static getPool() {
    //     return {
    //         pool: Provider.pool,
    //     }
    // }
    //
    // public getClassName() {
    //     return this.className;
    // }
}
