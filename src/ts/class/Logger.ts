import "reflect-metadata";
import ClassName, { GetClassName } from "../container/ClassName";
import { Injectable } from "../container/Injectable";
import Log from "./Log";
import { InjectionKey } from "vue";

@Injectable()
@ClassName('Logger')
export default class Logger {
    info(...o: any): void {
        return Log.info(...o);
    }

    warn(...o: any): void {
        return Log.warn(...o);
    }

    error(...o: any): void {
        return Log.error(...o);
    }

    debug() {
        return Log.debug()
    }

    getCounter() {
        return Log.getCounter()
    }

    getTime() {
        return Log.getTime()
    }

    static factory<T>(classT: ClassType<T>): Logger {
        let className = GetClassName(classT);
        return new class extends Logger {
            info(...o: any): void {
                return super.info(`[${ className }]`, ...o);
            }

            warn(...o: any): void {
                return super.warn(`[${ className }]`, ...o);
            }

            error(...o: any): void {
                return super.error(`[${ className }]`, ...o);
            }
        }();
    }
}

export const LoggerKey = Symbol() as InjectionKey<Logger>;
