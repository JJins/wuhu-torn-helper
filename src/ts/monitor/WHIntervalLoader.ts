/**
 * 监控模块的循环处理部分
 */

import drugCDMonitor from "./DrugCDMonitor";
import { Injectable } from "../container/Injectable";
import ClassName from "../container/ClassName";
import LocalConfigWrapper from "../class/LocalConfigWrapper";
import IntervalSwitch from "./IntervalSwitch";
import IFeature from "../man/IFeature";
import ATK_PAGE_REG from "../feature/url/ATK_PAGE_REG";
import ALL_PAGE_REG from "../feature/url/ALL_PAGE_REG";

export default interface IntervalType {
    handler: () => void
    switcher: (s: IntervalSwitch) => void
    getId: () => number
    setId: (id: number) => void
    getMs: () => number
    setMs: (ms: number) => void
}

const intervalUnits: { [key: string]: IntervalType } = {
    drugCDMonitor,
}

@Injectable()
@ClassName('WHIntervalLoader')
export class WHIntervalLoader implements IFeature {
    description(): string {
        return "新监控模块";
    }

    iStart(): void | Promise<void> {
        this.init()
    }

    urlExcludes(): RegExp[] {
        return [ATK_PAGE_REG];
    }

    urlIncludes(): RegExp[] {
        return [ALL_PAGE_REG];
    }

    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper,
    ) {
    }

    init() {
        const list = this.localConfigWrapper.config.monitorOn
        for (let i = 0; i < list.length; i++) {
            intervalUnits[list[i]].switcher(IntervalSwitch.ON)
        }
    }
}
