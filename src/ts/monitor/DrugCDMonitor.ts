import IntervalType from "./WHIntervalLoader";
import ClassName from "../container/ClassName";
import IntervalSwitch from "./IntervalSwitch";
import { SWITCHER } from "./SWITCHER";
import getSidebarData from "../func/utils/getSidebarData";
import { Container } from "../container/Container";
import MsgWrapper from "../class/utils/MsgWrapper";
import LocalConfigWrapper from "../class/LocalConfigWrapper";

@ClassName('DrugCDMonitor')
class DrugCDMonitor implements IntervalType {
    private id: number = null
    private ms: number
    private readonly msgWrapper = Container.factory(MsgWrapper)

    getId(): number {
        return this.id
    }

    getMs(): number {
        return this.ms
    }

    handler(): void {
        const data = getSidebarData()
        if (data?.statusIcons?.icons?.drug_cooldown?.timerExpiresAt * 1000 < Date.now()) {
            this.msgWrapper.create('警告: 药CD停转', { sysNotify: true })
        }
    }

    setMs(ms: number): void {
        this.ms = ms
    }

    switcher(s: IntervalSwitch): void {
        SWITCHER(this, DrugCDMonitor, s)
    }

    setId(id: number): void {
        this.id = id
    }

}

const drugCDMonitor = new DrugCDMonitor()
drugCDMonitor.setMs(Container.factory(LocalConfigWrapper).config.drugCDMonitorInterval)
export default drugCDMonitor
