import { GetClassName } from "../container/ClassName";
import IntervalType from "./WHIntervalLoader";
import IntervalSwitch from "./IntervalSwitch";

export function SWITCHER(self: IntervalType, clazz: new () => IntervalType, s: IntervalSwitch) {
    if (self.getMs() === null) {
        throw new Error('循环时间为空')
    }
    switch (s) {
        case IntervalSwitch.ON:
            if (self.getId()) {
                throw new TypeError(GetClassName(clazz) + ' 正在启用')
            }
            self.setId(window.setInterval(() => self.handler(), self.getMs()))
            break
        case IntervalSwitch.OFF:
            if (self.getId()) {
                window.clearInterval(self.getId())
                self.setId(null)
            } else {
                throw new TypeError(GetClassName(clazz) + ' 未启用')
            }
    }
}
