export default interface PriceData {
    timestamp: number
    data: {
        [k: number]: ItemPrice
    }
}

export interface ItemPrice {
    name: string
    price: number
}
