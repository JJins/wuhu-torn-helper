export default interface BazaarApiResponse {
    bazaar: { ID: number, cost: number, quantity: number }[]
}
