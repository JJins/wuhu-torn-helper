import { MenuItemConfig } from "../class/ZhongIcon";
import { InjectionKey } from "vue";

export default interface MenuItem extends MenuItemConfig {
    domType: MENU_ITEM_TYPE
}

export enum MENU_ITEM_TYPE {
    BUTTON = 'button',
    PLAIN = 'plain',
    CHECKBOX = 'checkbox',
    SELECT = 'select',
}

// export
export const menuItemList = Symbol('menuItemList') as InjectionKey<MenuItem[]>;
