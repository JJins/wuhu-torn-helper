interface NotifyWrapper {
    count: number;

    [notifyId: number]: Notification;
}
