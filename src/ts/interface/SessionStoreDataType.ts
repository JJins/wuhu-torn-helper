/**
 * 存在于 session storage 中的 sidebarData+uid
 */
export interface SessionStoreDataType {
    sliderHintDisabled: boolean
    selectedStatusIcon: any
    progressBarFetchActive: boolean
    linkWithContextMenu: number
    statusIcons: StatusIcons
    user: User
    listsStatuses: ListsStatuses
    listsLinksStatuses: ListsLinksStatuses
    bars: Bars
    account: Account
    areas: Areas
    lists: Lists
    footer: Footer
    dataSync: boolean
    sidebarFetched: boolean
    currentUrl: string
}

export interface StatusIcons {
    size: string
    icons: Icons
    visible: boolean
    onTop: boolean
}

export interface Icons {
    enbyGender: EnbyGender
    donator: Donator
    company: Company
    faction: Faction
    education: Education
    bazaar: Bazaar
    stock_market: StockMarket
    booster_cooldown?: BoosterCooldown
    drug_cooldown?: DrugCooldown
}

export interface EnbyGender {
    iconID: string
    title: string
}

export interface Donator {
    iconID: string
    title: string
    subtitle: string
    link: string
}

export interface Company {
    iconID: string
    title: string
    subtitle: string
    link: string
}

export interface Faction {
    iconID: string
    title: string
    subtitle: string
    link: string
}

export interface Education {
    iconID: string
    title: string
    subtitle: string
    link: string
    serverTimestamp: number
    timerExpiresAt: number
    isShortFormatTimer: boolean
}

export interface Bazaar {
    iconID: string
    title: string
    subtitle: string
    link: string
}

export interface StockMarket {
    iconID: string
    title: string
    subtitle: string
    link: string
}

export interface BoosterCooldown {
    iconID: string
    title: string
    factionUpgrade: string
    serverTimestamp: number
    timerExpiresAt: number
    isShortFormatTimer: boolean
}

export interface DrugCooldown {
    iconID: string
    title: string
    subtitle: string
    serverTimestamp: number
    timerExpiresAt: number
    isShortFormatTimer: boolean
}

export interface User {
    userID: number
    name: string
    link: string
    money: Money
    donator: boolean
    level: Level
    points: Points
    merits: Merits
    refills: Refills
    lifeModifier: number
    status: any
}

export interface Money {
    value: number
}

export interface Level {
    value: number
    upgradePossibility: boolean
    link: string
}

export interface Points {
    value: number
    link: string
}

export interface Merits {
    value: number
    link: string
}

export interface Refills {
    value: string
    link: string
}

export interface ListsStatuses {
    areas: boolean
    lists: boolean
}

export interface ListsLinksStatuses {
    staff: boolean
    friends: boolean
    enemies: boolean
}

export interface Bars {
    energy: Energy
    nerve: Nerve
    happy: Happy
    life: Life
    chain: Chain
}

export interface Energy {
    name: string
    timestampToUpdate: number
    timeToUpdate: number
    statstamp: number
    amount: number
    max: number
    step: number
}

export interface Nerve {
    name: string
    timestampToUpdate: number
    timeToUpdate: number
    statstamp: number
    amount: number
    max: number
    step: number
}

export interface Happy {
    name: string
    timestampToUpdate: number
    timeToUpdate: number
    statstamp: number
    amount: number
    max: number
    step: number
}

export interface Life {
    name: string
    timestampToUpdate: number
    timeToUpdate: any
    statstamp: number
    amount: number
    max: number
    step: number
}

export interface Chain {
    name: string
    timestampToUpdate: number
    timeToUpdate: number
    amount: number
    max: number
    step: number
    coolDown: number
    endCoolDownTimestamp: number
    bonuses: number
    link: string
}

export interface Account {
    messages: Messages
    events: Events
    awards: Awards
}

export interface Messages {
    linkOrder: number
    name: string
    amount: number
    link: string
    icon: string
    highlightStatus: boolean
    status: any
    isMobileOnly: boolean
}

export interface Events {
    linkOrder: number
    name: string
    amount: number
    link: string
    icon: string
    highlightStatus: boolean
    status: string
    isMobileOnly: boolean
}

export interface Awards {
    linkOrder: number
    name: string
    amount: number
    link: string
    icon: string
    highlightStatus: boolean
    status: any
    isMobileOnly: boolean
}

export interface Areas {
    messages: Messages2
    events: Events2
    awards: Awards2
    gym: Gym
    crimes: Crimes
    items: Items
    city: City
    missions: Missions
    casino: Casino
    calendar: Calendar
    education: Education2
    properties: Properties
    friends_list: FriendsList
    enemies_list: EnemiesList
    elimination: Elimination
    community_events: CommunityEvents
}

export interface Messages2 {
    linkOrder: number
    name: string
    amount: number
    link: string
    icon: string
    highlightStatus: boolean
    status: any
    isMobileOnly: boolean
}

export interface Events2 {
    linkOrder: number
    name: string
    amount: number
    link: string
    icon: string
    highlightStatus: boolean
    status: string
    isMobileOnly: boolean
}

export interface Awards2 {
    linkOrder: number
    name: string
    amount: number
    link: string
    icon: string
    highlightStatus: boolean
    status: any
    isMobileOnly: boolean
}

export interface Gym {
    linkOrder: number
    name: string
    shortName: any
    link: string
    icon: string
    styleStatus: any
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface Crimes {
    linkOrder: number
    name: string
    shortName: any
    link: string
    icon: string
    styleStatus: any
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface Items {
    linkOrder: number
    name: string
    shortName: any
    link: string
    icon: string
    styleStatus: any
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface City {
    linkOrder: number
    name: string
    shortName: any
    link: string
    icon: string
    styleStatus: any
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface Missions {
    linkOrder: number
    name: string
    shortName: any
    link: string
    icon: string
    styleStatus: any
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface Casino {
    linkOrder: number
    name: string
    shortName: any
    link: string
    icon: string
    styleStatus: any
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface Calendar {
    linkOrder: number
    name: string
    shortName: string
    link: string
    icon: string
    styleStatus: any
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface Education2 {
    linkOrder: number
    name: string
    shortName: string
    link: string
    icon: string
    styleStatus: any
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface Properties {
    linkOrder: number
    name: string
    shortName: string
    link: string
    icon: string
    styleStatus: any
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface FriendsList {
    linkOrder: number
    name: string
    icon: string
    link: string
    elements: any
    status: any
    isMobileOnly: boolean
}

export interface EnemiesList {
    linkOrder: number
    name: string
    icon: string
    link: string
    elements: Element[]
    status: any
    isMobileOnly: boolean
}

export interface Element {
    name: string
    link: string
    status: string
    lastAction: number
}

export interface Elimination {
    linkOrder: number
    name: string
    shortName: string
    link: string
    icon: string
    styleStatus: string
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface CommunityEvents {
    linkOrder: number
    name: string
    shortName: string
    link: string
    icon: string
    styleStatus: string
    highlightStatus: boolean
    status: any
    activeStatusUrlRegex: any
    amount: any
    isMobileOnly: boolean
}

export interface Lists {
    company_list: CompanyList
    faction_list: FactionList
    friends_list: FriendsList2
    enemies_list: EnemiesList2
}

export interface CompanyList {
    linkOrder: number
    name: string
    icon: string
    link: string
    elements: Element2[]
    status: any
    isMobileOnly: boolean
}

export interface Element2 {
    name: string
    link: string
    status: string
    lastAction: number
}

export interface FactionList {
    linkOrder: number
    name: string
    icon: string
    link: string
    elements: Element3[]
    status: any
    isMobileOnly: boolean
}

export interface Element3 {
    name: string
    link: string
    status: string
    lastAction: number
}

export interface FriendsList2 {
    linkOrder: number
    name: string
    icon: string
    link: string
    elements: any
    status: any
    isMobileOnly: boolean
}

export interface EnemiesList2 {
    linkOrder: number
    name: string
    icon: string
    link: string
    elements: Element4[]
    status: any
    isMobileOnly: boolean
}

export interface Element4 {
    name: string
    link: string
    status: string
    lastAction: number
}

export interface Footer {
    serverName: string
    dayTextualFormat: string
    dateFormat: string
    dayTimeFormat: string
}
