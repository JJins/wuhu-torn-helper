/**
 * 处理网络请求响应的类型
 */
export default interface ResponseInject {
    /**
     * @param url 请求url
     * @param body 响应体代理
     * @param opt 请求参数
     */
    responseHandler(
        url: string,
        body: {
            json: unknown,
            text: string,
            isModified: boolean
        },
        opt: {
            method: 'GET' | 'POST',
            requestBody: string
        }
    ): void
}
