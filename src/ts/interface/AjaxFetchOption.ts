export default interface AjaxFetchOption {
    url: string,
    headers?: { 'X-Requested-With'?: string, 'Content-Type'?: string };
    referrer?: string;
    method?: 'GET' | 'POST';
    body?: any;
}