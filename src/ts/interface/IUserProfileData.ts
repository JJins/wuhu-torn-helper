export default interface IUserProfileData {
    user: {
        playerName: string,
        userID: number,
        displayPersonalDetails: boolean,
        isFriend: boolean,
        isEnemy: boolean,
        signUp: number,
        role: string,
        sendMoneyWarning: string
    };
    currentUser: {
        "playerName": string,
        "userID": number,
        "money": number,
        "canSendAnonymously": boolean,
        "loggedIn": boolean
    };
    userInformation: {
        "name": string,
        "previousAliases": string[],
        "level": number,
        "rank": {
            "userTitle": string,
            "userRank": string
        },
        "age": number,
        "honorTitle": string,
        "image": unknown
    };
    profileButtons: unknown;
    userStatus: {
        "status": {
            "type": "ok" | "traveling-from" | "abroad-hospital" | "abroad"
        },
        "trait": {
            "loot": unknown
        }
    };
    medalInformation: unknown;
    basicInformation: {
        icons: { id: number, title: string }[],
        lastAction: { seconds: number }
    };
    personalInformation: unknown;
    competitionStatus: unknown;
    staffTools: null;
    profileSignature: null;
}