/**
 * Mini Profile 返回类型
 */
export interface MiniProfile {
    icons: { id: number, type: string, title: string, description?: string }[];
    profileButtons: {
        buttons: {
            addToEnemyList: Button,
            addToFriendList: Button,
            attack: Button,
            bail: Button,
            bust: Button,
            initiateChat: Button,
            initiateTrade: Button,
            personalStats: Button,
            placeBounty: Button,
            report: Button,
            revive: Button,
            sendMessage: Button,
            sendMoney: Button,
            viewBazaar: Button,
            viewDisplayCabinet: Button,
        }
    };
    user: {
        lastAction: {
            seconds: number
        };
        rank: {
            userRank: string,
            userTitle: string
        },
        sendMoneyWarning: string,
        playerName: string,
        userID: number,
    };
    userStatus: {
        status: {
            from: { simpleName: string },
            to: { simpleName: string },
            in: { simpleName: string, relatedName: string },
            type: 'ok' | 'traveling-to' | 'traveling-from' | 'abroad' | 'abroad-hospital' | 'jail',
            description: string,
        }
    };
}

export interface Button {
    actionDescription: string,
    link: string,
    state: "hidden" | "active" | "disabled",
    message: string,
}
