export default interface Sidebar {
    account: {
        awards: Area,
        events: Area,
        messages: Area,
    },
    areas: {
        calendar: Area,
        casino: Area,
        city: Area,
        crimes: Area,
        education: Area,
        forums: Area,
        gym: Area,
        hall_of_fame: Area,
        home: Area,
        hospital: Area,
        items: Area,
        jail: Area,
        job: Area,
        missions: Area,
        my_faction: Area,
        newspaper: Area,
        properties: Area,
        recruit_citizens: Area,
    },
    bars: {
        chain: Bar,
        energy: Bar,
        happy: Bar,
        life: Bar,
        nerve: Bar,
    },
}

interface Bar {
    name: string
}

interface Area {
    name: string,
    shortName?: string,
}
