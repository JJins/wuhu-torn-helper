/**
 * 物品详情
 */
export default interface InventoryItemInfo {
    itemID: number
    itemInfo: string
    // html 格式
    itemInfoContent: string
    itemName: string
    // html 格式
    itemRareTitle: string
    itemType: "Item"
    itemValue: string
}
