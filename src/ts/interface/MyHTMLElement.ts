interface MyHTMLElement extends HTMLElement {
    sys_notify?: Notification;
    msgInnerText?: string;
    close?: () => void;
    initTimer?: HTMLElement;

    // 对象的其他参数
    [key: string]: any;
}