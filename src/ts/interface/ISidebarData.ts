export default interface ISidebarData {
    statusIcons?: StatusIcons;
    user?: {
        userID: number,
        name: string,
        link: string,
        money: {
            value: number,
        },
        donator: boolean,
        level: {
            value: number,
            upgradePossibility: boolean,
            link: string,
        }
    };
    bars?: {
        energy: Bar,
        nerve: Bar,
        happy: Bar,
        life: Bar,
        chain: Bar,
    };
    areas?: {
        [area: string]: Area
    }
    headerData?: HeaderData;

    [key: string]: unknown;
}

interface Area {
    "name": string,
    "shortName": string,
    "link": string,
    "icon": string,
    "styleStatus": string,
    "linkOrder": number,
    "highlightStatus": unknown,
    "status": unknown,
    "favorite": boolean,
    "added": unknown,
    "amount": unknown
}

interface Bar {
    "name": string,
    "timeToUpdate": number,
    "amount": number,
    "max": number,
    "step": number,
    "coolDown"?: number,
    "endCoolDownTimestamp"?: number,
    "timestampToUpdate": number,
    "bonuses"?: number,
    "link"?: string
}

interface HeaderData {
    "user": {
        "state": {
            "status": "ok" | string,
            "isLoggedIn": boolean,
            "isDonator": boolean,
            "isTravelling": boolean,
            "isAbroad": boolean
        },
        "data": {
            "hospitalStamp": number,
            "jailStamp": number,
            "logoutHash": string,
            "userID": number,
            "avatar": {
                "isDefault": false,
                "link": string
            }
        }
    },
    "serverState": {
        "serverName": string,
        "currentTime": number
    },
    "settings": {
        "emptyHeader": boolean,
        "emptyLinks": boolean,
        "hideActivityLog": boolean,
        "hideAdvancedSearch": boolean,
        "hideSettingsDropdown": boolean,
        "hasAccessToDarkMode": boolean
    },
    "logo": {
        "name": string,
        "title": string,
        "country": string,
        "darkModeLogo": string,
        "additionalData": unknown[]
    },
    "links": Link[],
    "headlines": unknown
}

interface Link {
    "name": string,
    "link": string,
    "icon": string,
    "inNewTab": boolean
}

interface StatusIcons {
    "visible": boolean,
    "icons": {
        "enbyGender": Icon,
        "donator": Icon,
        "married": Icon,
        "company": Icon,
        "faction": Icon,
        "education": Icon,
        "bazaar": Icon,
        "stock_market": Icon,
        "drug_cooldown": Icon,
        "drug_addiction": Icon,
        "travelling": Icon,
        // 未收录图标
        [key: string]: Icon,
    },
    "size": "big svg" | string,
    "onTop": boolean
}

interface Icon {
    "iconID": string,
    "title": string,
    "subtitle"?: string,
    "link"?: string,
    "serverTimestamp"?: number,
    "timerExpiresAt"?: number,
    "isShortFormatTimer"?: boolean
}