import Device from "../enum/Device";
import { BeerMonitorLoop } from "../feature/BuyBeerHelper";
import Popup from "../class/utils/Popup";
import TravelItem from "../feature/TravelItem";

export default interface IGlobal {
    GM_xmlhttpRequest?: Function;

    href?: string;
    // 插件图标
    $zhongNode?: MyHTMLElement;
    // 弹窗
    popup_node?: MyHTMLElement | Popup;
    // 啤酒助手
    beer?: BeerMonitorLoop;
    // 留存的通知
    notifies?: NotifyWrapper;
    // 价格监控
    // priceWatcher?: { status: boolean };
    // 海外库存
    fStock?: TravelItem;
    // 玩家名和数字id
    player_info?: PlayerInfo;
    // 设备类型
    device?: Device;
    // PDA运行环境
    isPDA?: boolean;
    // PDA自带apikey
    PDA_APIKey?: string;
    // 脚本版本
    version?: string;
    // window 副本
    window?: Window;
    /**
     * unsafeWindow 副本
     * @deprecated
     */
    UWCopy?: Window & typeof globalThis;
    unsafeWindow?: Window & typeof globalThis;
    // document body 属性
    bodyAttrs?: {
        'data-country'?: string;
        'data-celebration'?: string;
        'data-traveling'?: string;
        'data-abroad'?: string;
        // [key: string]: string;
    };

    // 窗口活动状态
    isWindowActive?: { get: () => boolean };
}