export default interface ClassWithName {
    readonly className: string
}
