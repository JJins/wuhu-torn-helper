export default interface IWHNotify {
    // 秒
    timeout?: number;
    callback?: Function;
    sysNotify?: boolean;
    sysNotifyTag?: string;
    sysNotifyClick?: Function;
    // 强制后台也通知
    force?: boolean;
}
