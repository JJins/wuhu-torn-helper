/** 有同步数据来源时返回data，否则返回promise对象 */
export default interface Asyncable<T> {
    data?: T
    promise?: Promise<T>
}
