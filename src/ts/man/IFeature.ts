export default interface IFeature {
    iStart(): void | Promise<void>

    /**
     * 包含url、不能返回null
     */
    urlIncludes(): RegExp[]

    /**
     * 排除url
     */
    urlExcludes(): RegExp[]

    description(): string
}
