import FeatureStatus from "./const/FeatureStatus";

export default interface IFeatureResult {
    desc: string
    status: FeatureStatus
    clazz: string
    load: number
}
