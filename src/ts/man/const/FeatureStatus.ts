enum FeatureStatus {
    ADDED = '已添加',
    RUNNING = '正常',
    EXCLUDED = '页面排除',
    EXCEPTION = '异常',
}

export default FeatureStatus
