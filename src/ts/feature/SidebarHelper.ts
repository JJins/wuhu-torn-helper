import CommonUtils from "../class/utils/CommonUtils";
import Global from "../class/Global";
import Device from "../enum/Device";
import ClassName from "../container/ClassName";
import { Injectable } from "../container/Injectable";
import LocalConfigWrapper from "../class/LocalConfigWrapper";
import Logger from "../class/Logger";
import IFeature from "../man/IFeature";
import ATK_PAGE_REG from "./url/ATK_PAGE_REG";
import ALL_PAGE_REG from "./url/ALL_PAGE_REG";

/**
 * ### 边栏助手
 * - 隐藏按钮
 * - 4条转跳
 */
@ClassName('SidebarHelper')
@Injectable()
export default class SidebarHelper implements IFeature {
    private readonly logger: Logger = Logger.factory(SidebarHelper)

    description(): string {
        return "边栏助手";
    }

    iStart(): void | Promise<void> {
        this.init()
    }

    urlExcludes(): RegExp[] {
        return [ATK_PAGE_REG];
    }

    private readonly sidebarRootNode: HTMLElement;
    private readonly toggleBtn: HTMLButtonElement;
    private isHide: boolean;

    urlIncludes(): RegExp[] {
        return [ALL_PAGE_REG];
    }

    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper,
        private readonly global: Global,
    ) {
        this.sidebarRootNode = document.querySelector('#sidebarroot');
        this.toggleBtn = document.createElement('button');
    }

    init(): void {
        this.isHide = !!this.localConfigWrapper.config.HideSidebar;

        if (!document.body.classList.contains('without-sidebar') || window.location.href.includes('christmas_town.php')) {
            this.initToggleBtn();
            this.barRedirect();
        } else {
            this.logger.warn('页面未开启边栏，边栏助手退出');
        }
    }

    // 初始化隐藏按钮
    private initToggleBtn(): void {
        if (this.global.device === Device.PC && this.sidebarRootNode && this.localConfigWrapper.config.HideSidebarBtn) {
            this.isHide ? this.hideHandler() : this.showHandler();
            let container = document.createElement('div');
            container.append(this.toggleBtn);
            container.style.width = '0';
            container.style.zIndex = '1';
            container.style.opacity = '0.75';
            this.toggleBtn.classList.add('torn-btn');
            this.sidebarRootNode.before(container);
            this.toggleBtn.addEventListener('click', () => {
                this.isHide ? this.showHandler() : this.hideHandler();
            });
        }
    }

    private hideHandler(): void {
        this.sidebarRootNode.classList.add('wh-hide');
        this.localConfigWrapper.config.HideSidebar = true;
        this.isHide = true;
        this.toggleBtn.innerHTML = '>>';
    }

    private showHandler(): void {
        this.sidebarRootNode.classList.remove('wh-hide');
        this.localConfigWrapper.config.HideSidebar = false;
        this.isHide = false;
        this.toggleBtn.innerHTML = '<<';
    }

    // 4条转跳
    private barRedirect(): void {
        if (this.localConfigWrapper.config.barsRedirect) {
            const eb = document.querySelector<HTMLAnchorElement>('#barEnergy');
            const nb = document.querySelector<HTMLAnchorElement>('#barNerve');
            const hb = document.querySelector<HTMLAnchorElement>('#barHappy');
            const lb = document.querySelector<HTMLAnchorElement>('#barLife');
            if (eb) {
                eb.addEventListener('click', () => location.href = '/gym.php');
                eb.href = '/gym.php';
            } else {
                CommonUtils.elementReady('#barEnergy').then(eb => {
                    eb.addEventListener('click', () => location.href = '/gym.php');
                    (eb as HTMLAnchorElement).href = '/gym.php';
                });
            }
            if (nb) {
                nb.addEventListener('click', () => location.href = '/crimes.php');
                nb.href = '/crimes.php';
            } else {
                CommonUtils.elementReady('#barNerve').then(nb => {
                    nb.addEventListener('click', () => location.href = '/crimes.php');
                    (nb as HTMLAnchorElement).href = '/crimes.php';
                });
            }
            if (hb) {
                hb.addEventListener('click', () => location.href = '/item.php#boosters-items');
                hb.href = '/item.php#boosters-items';
            } else {
                CommonUtils.elementReady('#barHappy').then(hb => {
                    hb.addEventListener('click', () => location.href = '/item.php#boosters-items');
                    (hb as HTMLAnchorElement).href = '/item.php#boosters-items';
                });
            }
            if (lb) {
                lb.addEventListener('click', () => location.href = '/item.php#medical-items');
                lb.href = '/item.php#medical-items';
            } else {
                CommonUtils.elementReady('#barLife').then(lb => {
                    lb.addEventListener('click', () => location.href = '/item.php#medical-items');
                    (lb as HTMLAnchorElement).href = '/item.php#medical-items';
                });
            }
        }
    }
}
