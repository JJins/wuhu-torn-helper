import IFeature from "../man/IFeature";
import ClassName from "../container/ClassName";
import { Injectable } from "../container/Injectable";
import TornStyleBlock from "../class/utils/TornStyleBlock";
import TornStyleSwitch from "../class/utils/TornStyleSwitch";
import cityFinder from "../func/module/cityFinder";
import LocalConfigWrapper from "../class/LocalConfigWrapper";

@ClassName('MapItem')
@Injectable()
export default class MapItem implements IFeature {
    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper
    ) {
    }

    description(): string {
        return "捡垃圾助手";
    }

    iStart(): void | Promise<void> {
        this.start()
    }

    urlExcludes(): RegExp[] {
        return [];
    }

    urlIncludes(): RegExp[] {
        return [/city\.php/];
    }

    start() {
        if (this.localConfigWrapper.config.cityFinder) {
            let _base = new TornStyleBlock('芜湖助手').insert2Dom();
            // let reloadSwitch = new TornStyleSwitch('解决一直转圈(加载中)的问题');
            // reloadSwitch.getInput().checked = this.localConfigWrapper.config.SolveGoogleScriptPendingIssue;
            // _base.append(reloadSwitch.getBase()).insert2Dom();
            // reloadSwitch.getInput().addEventListener('change', () => {
            //     if (reloadSwitch.getInput().checked) window.location.replace(window.location.href);
            //     this.localConfigWrapper.config.SolveGoogleScriptPendingIssue = reloadSwitch.getInput().checked;
            // });

            _base.append(document.createElement('br'));

            cityFinder(_base);
        }
    }

}
