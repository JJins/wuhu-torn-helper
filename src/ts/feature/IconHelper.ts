import Logger, { LoggerKey } from "../class/Logger"
import ClassName from "../container/ClassName"
import { Injectable } from "../container/Injectable"
import CommonUtils, { CommonUtilsKey } from "../class/utils/CommonUtils"
import LocalConfigWrapper, { LocalConfigWrapperKey } from "../class/LocalConfigWrapper"
import { createApp } from "vue"
import FloatMenu from "../../vue/FloatMenu.vue"
import PopupWrapper, { PopupWrapperKey } from "../class/utils/PopupWrapper"
import TravelItem from "./TravelItem"
import QuickGymTrain, { QuickGymTrainKey } from "../class/action/QuickGymTrain"
import QuickFlyBtnHandler, { QuickFlyBtnHandlerKey } from "../class/handler/QuickFlyBtnHandler"
import ItemHelper, { ItemHelperKey } from "../class/utils/ItemHelper"
import MathUtils, { MathUtilsKey } from "../class/utils/MathUtils"
import IFeature from "../man/IFeature"
import ALL_PAGE_REG from "./url/ALL_PAGE_REG"

@ClassName("IconHelper")
@Injectable()
export default class IconHelper implements IFeature {
    private readonly _element: HTMLElement = document.createElement('div');
    private readonly logger = Logger.factory(IconHelper)

    description(): string {
        return "浮动图标、侧边菜单、标签式便携功能";
    }

    iStart(): void | Promise<void> {
        this.init()
    }

    urlExcludes(): RegExp[] {
        return [];
    }

    urlIncludes(): RegExp[] {
        return [ALL_PAGE_REG];
    }

    constructor(
        private readonly commonUtils: CommonUtils,
        private readonly localConfigWrapper: LocalConfigWrapper,
        private readonly popupWrapper: PopupWrapper,
        private readonly travelItem: TravelItem,
        private readonly quickGymTrain: QuickGymTrain,
        private readonly quickFlyBtnHandler: QuickFlyBtnHandler,
        private readonly itemHelper: ItemHelper,
        private readonly mathUtils: MathUtils,
    ) {
    }

    get element() {
        return this._element;
    }

    init() {
        this._element.id = 'WHMenu2023';
        document.body.append(this._element);
        let app = createApp(FloatMenu);
        this.logger.info('Vue实例已创建', { app })

        app.config.errorHandler = (err) => this.logger.error('[VUE错误]', err);
        app.config.warnHandler = (err) => this.logger.warn('[VUE警告]', err);
        app.provide(LoggerKey, this.logger);
        app.provide(CommonUtilsKey, this.commonUtils);
        app.provide(MathUtilsKey, this.mathUtils);
        // app.provide(TravelItemKey, this.travelItem);
        app.provide(PopupWrapperKey, this.popupWrapper);
        app.provide(LocalConfigWrapperKey, this.localConfigWrapper);
        app.provide(QuickGymTrainKey, this.quickGymTrain);
        app.provide(QuickFlyBtnHandlerKey, this.quickFlyBtnHandler);
        app.provide(ItemHelperKey, this.itemHelper);
        app.mount(this._element);
        this.logger.info('Vue实例已挂载')
    }

}
