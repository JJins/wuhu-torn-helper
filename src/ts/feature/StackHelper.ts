import TornStyleBlock from "../class/utils/TornStyleBlock";
import TornStyleSwitch from "../class/utils/TornStyleSwitch";
import { Injectable } from "../container/Injectable";
import ClassName from "../container/ClassName";
import LocalConfigWrapper from "../class/LocalConfigWrapper";
import IFeature from "../man/IFeature";

@Injectable()
@ClassName('StackHelper')
export default class StackHelper implements IFeature {

    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper,
    ) {
    }

    description(): string {
        return "叠e助手";
    }

    iStart(): void | Promise<void> {
        this.start()
    }

    urlExcludes(): RegExp[] {
        return [];
    }

    urlIncludes(): RegExp[] {
        return [/gym\.php/];
    }

    start() {
        let block = new TornStyleBlock('叠E保护').insert2Dom();
        let switcher = new TornStyleSwitch('启用');
        let input = switcher.getInput();
        block.append(switcher.getBase());
        input.checked = this.localConfigWrapper.config.SEProtect;
        if (input.checked) document.body.classList.add('wh-gym-stack');
        // 绑定点击事件
        input.onchange = e => {
            let target = e.target as HTMLInputElement;
            document.body.classList.toggle('wh-gym-stack');
            this.localConfigWrapper.config.SEProtect = target.checked;
        };
    }
}
