import ClassName from "../container/ClassName";
import { Injectable } from "../container/Injectable";
import IFeature from "../man/IFeature";
import christmasTownHelper from "../func/module/christmasTownHelper";

@ClassName('ChristmasTown')
@Injectable()
export default class ChristmasTown implements IFeature {

    urlIncludes(): RegExp[] {
        return [/christmas_town\.php/]
    }

    urlExcludes(): RegExp[] {
        return []
    }

    description(): string {
        return '圣诞小镇助手'
    }

    iStart(): void | Promise<void> {
        christmasTownHelper()
    }
}
