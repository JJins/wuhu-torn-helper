import ClassName from "../container/ClassName";
import { Injectable } from "../container/Injectable";
import IFeature from "../man/IFeature";
import LocalConfigWrapper from "../class/LocalConfigWrapper";
import getTaskHint from "../func/translate/getTaskHint";
import { missionDict } from "../dictionary/translation";

@ClassName('MissionPageModifier')
@Injectable()
export default class MissionPageModifier implements IFeature {
    constructor(
        private readonly localConfigWrapper: LocalConfigWrapper
    ) {
    }

    description(): string {
        return "页面快速crime";
    }

    iStart(): void | Promise<void> {
        this.start()
    }

    urlExcludes(): RegExp[] {
        return [];
    }

    urlIncludes(): RegExp[] {
        return [/crimes\.php/];
    }

    start() {
        const anchor = document.querySelector('.content-wrapper');
        const OB = new MutationObserver(() => {
            OB.disconnect();
            trans();
            OB.observe(anchor, {
                characterData: true,
                attributes: true,
                subtree: true,
                childList: true
            });
        });
        const taskList = {};
        const trans = () => {
            $('ul#giver-tabs a.ui-tabs-anchor').each((i, e) => {
                let $e = $(e);
                if ($e.children().hasClass('mission-complete-icon')) {
                    taskList[i] = e.innerText.trim();
                } else {
                    taskList[i] = $e.clone().children().remove().end().text().trim();
                }
            });
            // 助手注入
            $('div.max-height-fix.info').each((i, e) => {
                let $e = $(e);
                if ($e.find('.wh-translated').length !== 0) return;
                $e.append(`<div class="wh-translated"><h6 style="color:green"><b>任务助手</b></h6><p>${ getTaskHint(taskList[i]) }</p></div>`);
            });
            // 任务目标
            $('ul.tasks-list span.title-wrap').contents().each((i, e) => {
                if (e.nodeType === 3) {
                    if (missionDict[e.nodeValue.trim()]) {
                        e.nodeValue = missionDict[e.nodeValue.trim()];
                    }
                }
            });
        };
        trans();
        OB.observe(anchor, {
            characterData: true,
            attributes: true,
            subtree: true,
            childList: true
        });
    }
}
