import { itemEffectDict, itemNameDict, itemPageDict, itemTypeDict } from "../../dictionary/translation";

// 展开物品详情
export default function showItemInfoTrans(dom: HTMLElement = document.querySelector('.show-item-info')) {
    if (dom) {
        const $item_info = dom.querySelector('span.info-msg');
        if ($item_info) {
            // tt插件
            const is_tt_modified = !!$item_info.querySelector('.tt-modified');
            if (is_tt_modified) {
                console.warn(is_tt_modified)
            }
            // 物品名
            const $item_name = $item_info.querySelector('span.bold');
            // 去除物品名的the
            const the_removed = $item_name.innerText.trim().slice(4);
            // 物品的类别
            const $item_type = $item_name.nextSibling;
            // 绿字 物品效果
            const $item_effect = $item_info.querySelector('div.item-effect');
            if (itemNameDict[the_removed]) {
                $item_name.innerText = `${ itemNameDict[the_removed] }(${ the_removed })`;
            }
            if (itemTypeDict[$item_type.nodeValue.trim()]) {
                $item_type.nodeValue = itemTypeDict[$item_type.nodeValue.trim()];
            }
            if ($item_effect && itemEffectDict[$item_effect.innerText.trim()]) {
                $item_effect.innerText = itemEffectDict[$item_effect.innerText.trim()];
            }
        }
        // 下方的表格
        const $info_table_title = dom.querySelectorAll('div.title');
        $info_table_title.forEach((e) => {
            if (itemPageDict[e.innerText.trim()]) {
                e.innerText = itemPageDict[e.innerText.trim()];
            }
        });
    }
}