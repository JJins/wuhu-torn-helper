import { hosDict, statusDict } from "../../dictionary/translation";

/**
 * 玩家状态翻译
 */
function playerStatusTrans(slt) {
    if (!slt) return;
    slt.contents().each((i, e) => {
        if (e.nodeType === 3) {
            if (statusDict[e.nodeValue.trim()]) {
                e.nodeValue = statusDict[e.nodeValue.trim()];
            } else if (hosDict[e.nodeValue.trim()]) {
                e.nodeValue = hosDict[e.nodeValue.trim()];
            }
            // 医院 监狱
            else {
                if (e.nodeValue.contains(/In hospital for/))
                    e.nodeValue = e.nodeValue
                        .replace('In hospital for', '住院')
                        .replaceHMS();
                else if (e.nodeValue.contains(/In jail for/))
                    e.nodeValue = e.nodeValue
                        .replace('In jail for', '坐牢')
                        .replaceHMS();
            }
        }
    });
}

export default playerStatusTrans