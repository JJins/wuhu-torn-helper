import { sendCashDict } from "../../dictionary/translation";

/**
 * 发钱翻译
 */
export default function sendCashTrans(domPath = '', buttonClass = '.send-cash') {
    const sc = $(`${ domPath } ${ buttonClass } *`);
    if (sc.length === 0) return;
    sc.contents().each((i, e) => {
        if (e.nodeType === 1) {
            if (sendCashDict[$(e).attr('placeholder')]) {
                $(e).attr('placeholder', sendCashDict[$(e).attr('placeholder')]);
                return;
            }
            if (sendCashDict[$(e).attr('title')]) {
                $(e).attr('title', sendCashDict[$(e).attr('title')]);
            }
        } else if (e.nodeType === 3) {
            if (sendCashDict[e.nodeValue.trim()]) {
                e.nodeValue = sendCashDict[e.nodeValue.trim()];
            }
        }
    });
}