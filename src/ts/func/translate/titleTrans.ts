import { cityDict, titleDict } from "../../dictionary/translation";

/**
 * 页标题翻译
 */
export default function titleTrans() {
    let node = $('h4#skip-to-content');
    const $title = node.length === 0 ? $('h4[class^="title"]') : node;
    const title = titleDict[$title.text().trim()] || cityDict[$title.text().trim()];
    if (title && $title.css('display') !== 'none')
        $title.after($title.clone().text(title)).css('display', 'none');
}