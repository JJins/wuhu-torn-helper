import { titleLinksDict } from "../../dictionary/translation";

// 页标题右侧按钮
export default function contentTitleLinksTrans() {
    const $links_default = document.querySelectorAll('div.content-title span:nth-child(2)');
    const $links = $links_default.length === 0
        ? document.querySelectorAll('div[class^="topSection"] span[class*="Title"]')
        : $links_default;
    $links.forEach(e => {
        if (titleLinksDict[e.innerText.trim()]) {
            e.innerText = titleLinksDict[e.innerText.trim()];
        } else if (e.id === 'events') {
            if (titleLinksDict[e.innerText.trim().split(' ')[0]])
                e.innerText = e.innerText.trim()
                    .replace(
                        e.innerText.trim().split(' ')[0],
                        titleLinksDict[e.innerText.trim().split(' ')[0]]
                    );
        }
    });
}