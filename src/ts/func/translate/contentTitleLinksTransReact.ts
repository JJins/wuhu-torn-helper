import { titleLinksDict } from "../../dictionary/translation";

export default function contentTitleLinksTransReact(dom = document.querySelectorAll('div[class^="linksContainer___"] span[class^="linkTitle___"]')) {
    dom.forEach(e => {
        const links_trans = titleLinksDict[e.innerText.trim()];
        if (links_trans) e.innerText = links_trans;
    });
}