/**
 * 遍历所有子节点
 * @param {Node} node 需要遍历的容器父元素
 * @param {Function} handler 调用的方法
 */
export default function walkNode(node, handler) {
    let list = node.childNodes;
    if (list.length === 0) handler(node);
    else list.forEach(n => walkNode(n, handler));
}