import { eventsDict, gymList, ocList } from "../../dictionary/translation";

let log = { info: (args) => null };

export default function eventsTrans(events: JQuery = $('span.mail-link')) {
    const index = window.location.href.indexOf('events.php#/step=received') >= 0 ? 1 : 0;
    const isReceived = index === 1;
    // 通知的类型选择栏
    $('ul.mailbox-action-wrapper a').contents().each((i, e) => {
        if (e.nodeValue)
            if (eventsDict[e.nodeValue.trim()])
                e.nodeValue = eventsDict[e.nodeValue.trim()];
    });

    // 桌面版右边按钮浮动提示消息
    $('div.mailbox-container i[title]').each((i, e) => {
        if (eventsDict[$(e).attr('title')]) {
            $(e).attr('title', eventsDict[$(e).attr('title')]);
        }
    });

    // 手机版底部按钮
    $('.mobile-mail-actions-wrapper div:nth-child(2)').each((i, e) => {
        if (eventsDict[$(e).text().trim()])
            $(e).text(eventsDict[$(e).text().trim()]);
    });

    // 黑框标题
    $('#events-main-wrapper .title-black').each((i, e) => {
        if (eventsDict[$(e).text().trim()]) {
            $(e).text(eventsDict[$(e).text().trim()]);
        }
    });

    // 发送的两个按钮 + user id
    $('#events-main-wrapper div.send-to a.btn').each((i, e) => {
        if (eventsDict[$(e).text().trim()]) {
            $(e).text(eventsDict[$(e).text().trim()]);
        }
    });
    $('#events-main-wrapper div.send-to span.cancel a').each((i, e) => {
        if (eventsDict[$(e).text().trim()]) {
            $(e).text(eventsDict[$(e).text().trim()]);
        }
    });
    $('#events-main-wrapper div.send-to span.name').each((i, e) => {
        if (eventsDict[$(e).text().trim()]) {
            $(e).text(eventsDict[$(e).text().trim()]);
        }
    });

    // 通知翻译的开关
    // if (!$('div#event-trans-msg').get(0) && !window.location.href.contains(/index\.php/)) {
    //     let node = $('input#eventTransCheck');
    //     node.attr('checked', localStorage.getItem('wh_trans_event') === 'true');
    //     node.change(function () {
    //         if ($(this).attr('checked') === undefined) {
    //             localStorage.setItem('wh_trans_event', 'false');
    //         } else {
    //             localStorage.setItem('wh_trans_event', 'true');
    //         }
    //         eventsTrans();
    //     });
    // }

    if (localStorage.getItem('wh_trans_event') === 'false') return;
    if (events.length === 0) return;
    events.each((i, e) => {
        // todo “收到的信息” 暂时删除发送人节点 不影响显示
        if (isReceived) {
            $(e).children('a.sender-name').remove();
        }

        if (eventsDict[$(e).text().trim()]) {
            $(e).text(eventsDict[$(e).text().trim()]);
            return;
        }

        /**
         * 赛车
         * You finished 5th in the Hammerhead race. Your best lap was 01:14.87.
         * You finished 1st in the Docks race. Your best lap was 04:01.33.
         * You finished 1st in the Hammerhead race and have received 3 racing points! Your best lap was 01:06.92.
         * You finished 4th in the Docks race. Your best lap was 03:29.27 beating your previous best lap record of 03:35.77 by 00:06.50.
         * You have crashed your Honda NSX on the Sewage race! The upgrades Paddle Shift Gearbox (Short Ratio) and Carbon Fiber Roof were lost.
         * You have crashed your Ford Mustang on the Docks race! Your car has been recovered.
         */
        if ($(e).text().indexOf('finished') >= 0) {

            if ($(e).text().indexOf('crashed') >= 0) return; // todo 撞车

            const isGainRacingPoint = $(e).text().indexOf('racing point');
            let racingPoint = isGainRacingPoint >= 0 ? $(e).text()[isGainRacingPoint - 2] : null;

            const isBeat = $(e).text().indexOf('beating') >= 0;
            let record, bestBy;
            if (isBeat) {
                record = $(e).text().split('record of ')[1].split(' by ')[0];
                bestBy = $(e).text().split('record of ')[1].split(' by ')[1].split('. ')[0];
            }

            const pos = e.childNodes[1].firstChild.nodeValue.match(/[0-9]+/)[0];

            const splitList = e.childNodes[2].nodeValue.split(' ');
            const bestLap = e.childNodes[2].nodeValue.split(' best lap was ')[1].slice(0, 8);//.split(' ')[0];

            let map = splitList[3];
            map = map === 'Two' ? 'Two Islands' : map;
            map = map === 'Stone' ? 'Stone Park' : map;

            e.firstChild.nodeValue = '你在赛车比赛 ' + map + ' 中获得第 ';
            e.childNodes[1].firstChild.nodeValue = pos;
            e.childNodes[2].nodeValue = ' 名，';
            if (isGainRacingPoint >= 0) {
                e.childNodes[2].nodeValue += '获得' + racingPoint + '赛车点数 (Racing Points)。';
            }

            e.childNodes[2].nodeValue += '你的最佳圈速是 ' + bestLap;
            if (isBeat) e.childNodes[2].nodeValue += '，比之前最佳 ' + record + ' 快 ' + bestBy;
            e.childNodes[2].nodeValue += '。'


            e.childNodes[2].nodeValue += '[';
            e.childNodes[3].firstChild.nodeValue = '查看';
            return;
        }

        /**
         * 还贷
         */
        if ($(e).text().contains(/You have been charged \$[\d,]+ for your loan/)) {
            const node1Value = e.firstChild.nodeValue; // You have been charged $29,000 for your loan. You can pay this by visiting the
            //e.childNodes[1].firstChild.nodeValue; // <a href="loan.php">Loan Shark</a>
            // const node3Value=e.childNodes[2].nodeValue;  内容是 ". "

            let charge = node1Value.split(' ')[4];
            let replace;
            replace = '你需要支付 ';
            replace += charge;
            replace += ' 贷款利息，点此支付：';

            e.firstChild.nodeValue = replace;
            e.childNodes[1].firstChild.nodeValue = '鲨客借贷';
            e.childNodes[2].nodeValue = '。';
            return;
        }

        /**
         * 收到钱物
         * You were sent $21,000,000 from
         * <a href="profiles.php?XID=2703642">JNZR</a>
         * .
         * 附带信息： with the message: Manuscript fee OCT
         * e.firstChild.nodeValue
         * e.childNodes[1].firstChild.nodeValue
         * e.childNodes[2].nodeValue
         *
         * You were sent 4x Xanax from RaichuQ with the message: Manuscript fee OCT
         * You were sent $21,000,000 from JNZR.
         * You were sent some Xanax from runningowl
         * You were sent 1x Present from Duke with the message: Is it your birthday?
         * You were sent Duke's Safe from DUKE
         * You were sent a Diamond Bladed Knife from charapower
         */
        if ($(e).text().contains(/You were sent .+ from/)) {
            // 数量 物品 信息
            // spl = [You were sent 1x Birthday Present from]
            const spl = $(e).contents().get(0).nodeValue.trim().split(' ');
            const msgSpl = $(e).text().trim().split('with the message: ');
            const num = /^\$[0-9,]+\b/.test(spl[3]) ? '' : spl[3].numWordTrans();
            const item = num === '' ? spl[3] : spl.slice(4, -1).join(' ');
            const msg = msgSpl[1] ? msgSpl[1] : null;
            e.childNodes[0].nodeValue = `你收到了 ${ num } ${ item }，来自 `;
            if (e.childNodes[2]) {
                e.childNodes[2].nodeValue = `。`;
            }
            if (msg) {
                e.childNodes[2].nodeValue = `，附带信息：${ msg }。`;
            }
            return;
        }

        /**
         * bazaar
         * Dewei3 bought 2 x Toyota MR2 from your bazaar for $56,590.
         * ['', 'bought', '2', 'x', 'Toyota', 'MR2', 'from', 'your', 'bazaar', 'for', '$56,590.\n']
         * e.childNodes[1].nodeValue
         */
        if ($(e).text().contains(/bought .+ from your bazaar for/)) {
            const bazEN = e.childNodes[1].nodeValue;
            const spl = bazEN.split(' ');

            const num = spl[2];
            const item = spl.slice(4, spl.indexOf('from')).join(' ');
            const money = spl[spl.length - 1].replace('.', '');

            e.childNodes[1].nodeValue = ' 花费 ' + money + ' 从你的店铺购买了 ' + num + ' 个 ' + ' ' + item + '。';
            return;
        }

        /**
         * 交易
         */
        if ($(e).text().indexOf('trade') >= 0) {
            const PCHC = '点此继续';
            if ($(e).text().indexOf('You must now accept') >= 0) {
                /**
                 * 接受交易
                 * <a href="profiles.php?XID=2703642">JNZR</a>
                 * has accepted the trade titled "g't". You must now accept to finalize it.
                 * <a href="trade.php#step=view&amp;ID=6567058" i-data="i_598_654_156_14">Please click here to continue.</a>
                 * JNZR已经接受了名为 "g't "的交易。你现在必须接受以完成它。
                 */
                const firstWords = e.childNodes[1].nodeValue.split('. You must')[0];
                const tradeName = firstWords.slice(31, firstWords.length);
                e.childNodes[1].nodeValue = ' 已经接受了名为 ' + tradeName + ' 的交易。你现在必须接受以完成它。';
                e.childNodes[2].firstChild.nodeValue = PCHC;
                return;
            }
            if ($(e).text().indexOf('expired') >= 0) {
                /**
                 * 交易过期
                 * The trade with
                 * <a href="profiles.php?XID=2696209" i-data="i_278_269_71_14">sabrina_devil</a>
                 *  has expired
                 *  与sabrina_devil的交易已经过期。
                 */
                e.firstChild.nodeValue = '与 ';
                e.childNodes[2].nodeValue = ' 的交易已过期。';
                return;
            }
            if ($(e).text().indexOf('initiated') >= 0) {
                /**
                 * 交易发起
                 * <a href="profiles.php?XID=2696209" i-data="i_199_374_71_14">sabrina_devil</a>
                 *  has initiated a trade titled "gt".
                 *  <a href="trade.php#step=view&amp;ID=6563577" i-data="i_435_374_156_14">Please click here to continue.</a>
                 *  sabrina_devil发起了一项名为 "gt "的交易。
                 */
                const node2 = e.childNodes[1].nodeValue;
                const tradeName = node2.slice(30, node2.length - 2);
                e.childNodes[1].nodeValue = ' 发起了标题为 ' + tradeName + ' 的交易。';
                e.childNodes[2].firstChild.nodeValue = PCHC;
                return;
            }
            if ($(e).text().indexOf('now complete') >= 0) {
                /**
                 * 交易完成
                 * <a href="profiles.php?XID=2692672" i-data="i_199_829_51_14">Tmipimlie</a>
                 *  has accepted the trade. The trade is now complete.
                 * Tmipimlie已经接受交易。现在交易已经完成。
                 */
                e.childNodes[1].nodeValue = ' 已经接受交易。该交易现已完成。';
                return;
            }
            if ($(e).text().indexOf('canceled') >= 0) {
                /**
                 * 交易完成
                 * <a href="profiles.php?XID=2431991">WOW</a>
                 *  has canceled the trade.
                 * WOW已经取消了这项交易。
                 */
                e.childNodes[1].nodeValue = ' 已经取消了这个交易。';
                return;
            }
            if ($(e).text().indexOf('commented') >= 0) {
                /**
                 * 交易评论
                 * <a href="profiles.php?XID=2567772">QIJI</a>
                 *  commented on your
                 * <a href="/trade.php#step=view&amp;ID=6405880" i-data="i_334_968_73_14">pending trade</a>
                 * : "Thank you for trading with me! The total is $19,461,755 and you can view your receipt here: https://www.tornexchange.com/receipt/mhWuuL7hrE"
                 */
                e.childNodes[1].nodeValue = ' 对';
                e.childNodes[2].firstChild.nodeValue = '进行中的交易';
                e.childNodes[3].nodeValue = '添加了一条评论' + e.childNodes[3].nodeValue;
                return;
            }
            return;
        }

        /**
         * 被mug
         */
        if ($(e).text().indexOf('mugged') >= 0) {
            const spl = $(e).text().trim().split(' ');
            if (spl.length > 7) return; // todo 多人运动暂时跳过
            const money = spl[spl.length - 2];
            if (spl[0] === 'Someone') { // 被匿名mug
                e.firstChild.nodeValue = '有人打劫你并抢走了 ' + money + ' [';
                e.childNodes[1].firstChild.nodeValue = '查看';
            } else {
                e.childNodes[1].nodeValue = ' 打劫你并抢走了 ' + money + ' [';
                e.childNodes[2].firstChild.nodeValue = '查看';
            }
            return;
        }

        /**
         * 被打
         */
        if ($(e).text().indexOf('attacked') >= 0) { // 被打
            /**
             * 攻击方式 词数=spl.length
             * 匿名 4 Someone attacked you [view]
             * - hosp 6 Someone attacked and hospitalized you [view]
             * -- 有人袭击了你并安排你住院
             * 实名 4 EternalSoulFire attacked you [view]
             * - lost 6 EternalSoulFire attacked you but lost [view]
             * - hosp 6
             * - 逃跑esc 6 Dr_Bugsy_Siegel attacked you but escaped [view]
             * - 25回合平手stale 6 Tharizdun attacked you but stalemated [view]
             * - 起飞或bug 6 Mrew tried to attack you [view]
             *
             * You attacked Cherreh but timed out [view]
             *
             * 多人运动 todo
             * 10 Pual (and 2 others) attached you and hospitalized you [view]
             * 9 Argozdoc attacked you but Norm fought him off [view]
             */
            const spl = $(e).text().trim().split(' ');

            if (spl.length > 6) { // 多人运动暂时跳过
                /**
                 * 超时自动失败
                 */
                if (spl[4] === 'timed') {
                    if (e.firstChild.firstChild) { // 由第一个节点是否有子节点判断 被攻击
                        e.childNodes[1].nodeValue = ' 袭击你但是超时了 [';
                        e.childNodes[2].firstChild.nodeValue = '查看';
                        return;
                    }
                    e.firstChild.nodeValue = '你袭击 ';
                    e.childNodes[2].nodeValue = ' 但是超时了 [';
                    e.childNodes[3].firstChild.nodeValue = '查看';
                    return;
                }
                return;
            }

            if ($(e).find('a').text().toLowerCase().indexOf('someone') < 0 && // 避免玩家名带有someone字样
                $(e).text().split(' ')[0].toLowerCase() === 'someone') { // 被匿名
                if (spl.length === 6 && spl[3] === 'hospitalized') { // 匿名hos
                    e.firstChild.nodeValue = '有人袭击你并将你强制住院 [';
                    e.childNodes[1].firstChild.nodeValue = '查看';
                    return;
                }
                e.firstChild.nodeValue = '有人袭击了你 [';
                e.childNodes[1].firstChild.nodeValue = '查看';
                return;
            }

            if (spl.length === 4) { // 实名leave
                e.childNodes[1].nodeValue = ' 袭击了你 [';
                e.childNodes[2].firstChild.nodeValue = '查看';
                return;
            }

            if (spl.length === 6) { // 实名的情况
                switch (spl[4]) {
                    case 'lost':
                        e.childNodes[1].nodeValue = ' 袭击你但输了 [';
                        e.childNodes[2].firstChild.nodeValue = '查看';
                        return;
                    case 'escaped':
                        e.childNodes[1].nodeValue = ' 袭击你但逃跑了 [';
                        e.childNodes[2].firstChild.nodeValue = '查看';
                        return;
                    case 'stalemated':
                        e.childNodes[1].nodeValue = ' 袭击你但打成了平手 [';
                        e.childNodes[2].firstChild.nodeValue = '查看';
                        return;
                }
                switch (spl[3]) {
                    case 'attack': // Mrew tried to attack you [view]
                        e.childNodes[1].nodeValue = ' 尝试袭击你 [';
                        e.childNodes[2].firstChild.nodeValue = '查看';
                        return;
                    case 'hospitalized':
                        e.childNodes[1].nodeValue = ' 袭击你并将你强制住院 [';
                        e.childNodes[2].firstChild.nodeValue = '查看';
                        return;
                }
            }

        }

        /**
         * 每日彩票
         * 有人在Lucky Shot彩票中赢得11,832,100,000美元!
         * zstorm won $5,574,200 in the Daily Dime lottery!
         */
        if ($(e).text().indexOf('lottery') >= 0) {
            const split = e.childNodes[1].nodeValue.split(' ');
            const type = split[split.length - 3] + ' ' + split[split.length - 2];
            const money = split[2];
            e.childNodes[1].nodeValue = ' 在 ' + type + ' 彩票中赢得了 ' + money + '！';
            return;
        }

        /**
         * 公司职位变更
         */
        if ($(e).text().contains(/, the director of .+, has/)) {
            $(e).contents().each((i, e) => {
                if (e.nodeType === 3) {
                    if (eventsDict[e.nodeValue.trim()]) {
                        e.nodeValue = eventsDict[e.nodeValue.trim()];
                    } else {
                        // 工资改变
                        if (e.nodeValue.contains(/wage/)) {
                            const money = e.nodeValue.trim().slice(27, -9);
                            e.nodeValue = ` 的老板) 将你的每日工资改为 ${ money }。`;
                            return;
                        }
                        // 职位改变
                        if (e.nodeValue.contains(/rank/)) {
                            const pos = e.nodeValue.trim().slice(27, -1);
                            e.nodeValue = ` 的老板) 将你的公司职位改为 ${ pos }。`;
                            return;
                        }
                        if (e.nodeValue.contains(/assigned/)) {
                            e.nodeValue = ` 的老板) 将你指派为新的公司老板。`;
                            return;
                        }
                        // 火车
                        if (e.nodeValue.contains(/trained/)) {
                            const spl = e.nodeValue.trim().split(' ');
                            const pri = spl[10];
                            const sec = spl[13].slice(0, -1);
                            e.nodeValue = ` 的老板) 从公司训练了你。你获得了 50 ${ eventsDict[pri] } 和 25 ${ eventsDict[sec] }。`;
                        }
                    }
                }
            });
            return;
        }

        /**
         * 悬赏已被领取
         */
        if ($(e).text().contains(/bounty reward/)) {
            $(e).contents().each((i, e) => {
                if (e.nodeType === 3) {
                    if (eventsDict[e.nodeValue.trim()]) {
                        e.nodeValue = ` ${ eventsDict[e.nodeValue.trim()] } `;
                    } else {
                        if (e.nodeValue.contains(/bounty reward/)) {
                            const bountyAmount = e.nodeValue.trim().split(' ')[3];
                            if (eventsDict['and earned your'] && eventsDict['bounty reward']) {
                                e.nodeValue = ` ${ eventsDict['and earned your'] } ${ bountyAmount } ${ eventsDict['bounty reward'] }`;
                            }
                        }
                    }
                }
            });
            return;
        }

        /**
         * oc开启
         * You have been selected by
         * <a href="profiles.php?XID=2537542" class="h" i-data="i_346_233_63_14">endlessway</a>
         *  to participate in an organized crime. You, along with 2 others will make up the team to
         * <a href="organisedcrimes.php" i-data="i_312_248_107_14">make a bomb threat</a>
         *  in 72 hours.
         *
         * 你被endlessway选中参与一项有组织的犯罪活动。你和另外两个人将组成一个团队，在72小时内进行炸弹威胁。
         */
        if ($(e).text().indexOf('organized crime') >= 0) {
            const time = e.childNodes[4].nodeValue.split(' ')[2];
            const OCName = e.childNodes[3].firstChild.nodeValue;
            let others = e.childNodes[2].nodeValue.split(' ')[10];
            others = others === 'one' ? '1' : others;
            e.firstChild.nodeValue = '你被 ';
            e.childNodes[2].nodeValue = ` 选中参与一项组织犯罪(OC)。你和另外${ others }人将组成一个团队，在${ time }小时后进行 `;
            e.childNodes[3].firstChild.nodeValue = ocList[OCName] ? ocList[OCName] : OCName;
            e.childNodes[4].nodeValue = '。';
            return;
        }

        /**
         * oc结束
         * - You and your team tried to make a bomb threat but failed! View the details
         * - You and your team successfully blackmailed someone! View the details
         * <a href="organisedcrimes.php?step=log&amp;ID=9404419" i-data="i_595_234_24_14">here</a>
         * !
         */
        if ($(e).text().indexOf('You and your team') >= 0) {
            let rs = '成功';
            let OCName = e.firstChild.nodeValue.slice(31, -19);
            if ($(e).text().indexOf('fail') >= 0) {
                rs = '失败';
                OCName = e.firstChild.nodeValue.slice(27, -30);
            }
            e.firstChild.nodeValue = `你和团队的组织犯罪(OC) ${ ocList[OCName] ? ocList[OCName] : OCName } ${ rs }了！`;
            e.childNodes[1].firstChild.nodeValue = '点此查看详情';
            e.childNodes[2].nodeValue = '！';
            return;
        }

        /**
         * bust
         * <a href="profiles.php?XID=2208715">Spookyt</a>
         *  failed to bust you out of jail.
         */
        if ($(e).text().indexOf('bust') >= 0) {
            if (e.childNodes[1].nodeValue[1] === 'f') { // 失败
                e.childNodes[1].nodeValue = ' 没能把你从监狱救出来。';
                return;
            }
            if (e.childNodes[1].nodeValue[1] === 'w') { // 失败被抓
                e.childNodes[1].nodeValue = ' 在尝试救你出狱时被抓了。';
                return;
            }
            if (e.childNodes[1].nodeValue[1] === 's') {
                e.childNodes[1].nodeValue = ' 成功把你从监狱里救了出来。';
                return;
            }
        }

        /**
         * 保释
         */
        if ($(e).text().indexOf('bailed') >= 0) {
            const cost = e.childNodes[1].nodeValue.trim().slice(27, -1);
            e.childNodes[1].nodeValue = ' 花费 ' + cost + ' 保释了你。';
            return;
        }

        /**
         * 收到帮派的钱
         */
        if ($(e).text().contains(/You were given \$[\d,]+ from your faction/)) {
            const money = e.firstChild.nodeValue.split(' ')[3];
            let isNamed = e.childNodes.length > 1;
            if (isNamed) {
                e.firstChild.nodeValue = '';
                e.childNodes[2].nodeValue = ' 为你从帮派取了 ' + money + '。';
            } else {
                e.firstChild.nodeValue = '你得到了从帮派取出的 ' + money + '。';
            }
            return;
        }

        /**
         * 被下悬赏
         */
        if ($(e).text().contains(/has placed .+ bount.+ on you/)) {
            // 是否匿名 悬赏个数 悬赏单价 原因
            const spl = $(e).text().trim().split(' ');
            const reasonSpl = $(e).text().trim().split(' and the reason: ');
            const someone = !e.children.length;
            const num = spl[3] === 'a' ? '1' : spl[3];
            const price = reasonSpl[0].split(' ').slice(-1)[0];
            const reason = reasonSpl[1] ? reasonSpl[1] : null;
            const trans = `${ someone ?? '某人1' }对你进行了 ${ num } 次赏金为 ${ price } 的悬赏${ reason ? '，原因：' + reason : '' }`;
            // 匿名悬赏
            if (!!someone) {
                $(e).text(trans);
            }
            // 实名悬赏
            else {
                $(e).contents().get(1).nodeValue = trans;
            }
            return;
        }

        /**
         * 成功复活
         */
        if ($(e).text().contains(/successfully revived you/)) {
            if (e.children.length !== 1) return;
            if (eventsDict[$(e).contents().get(1).nodeValue.trim()]) {
                $(e).contents().get(1).nodeValue = eventsDict[$(e).contents().get(1).nodeValue.trim()]
            }
            return;
        }

        /**
         * 失败复活
         */
        if ($(e).text().contains(/failed to revive you/)) {
            if (e.children.length !== 1) return;
            if (eventsDict[$(e).contents().get(1).nodeValue.trim()]) {
                $(e).contents().get(1).nodeValue = eventsDict[$(e).contents().get(1).nodeValue.trim()]
            }
            return;
        }

        /**
         * 收到帮派的pt
         */
        if ($(e).text().contains(/You were given [\d,]+ points? from your faction/)) {
            const pt = e.firstChild.nodeValue.split(' ')[3];
            e.firstChild.nodeValue = '你得到了从帮派取出的 ' + pt + ' PT。'
            return;
        }

        /**
         * 帮派借东西
         */
        if ($(e).text().contains(/loaned you .+ from the faction armory/)) {
            const [num, item] = (() => {
                const spl = e.lastChild.nodeValue.trim().slice().slice(11, -25).split(' ');
                return spl.length === 1 ? [spl[0], null] : [spl[0], spl.slice(1).join(' ')];
            })();
            if (num && item) {
                e.lastChild.nodeValue = ` 从帮派军械库中借给你 ${ num.numWordTrans() } ${ item }。`;
            }
            return;
        }

        /**
         * 教育完成
         * <a href="/education.php" i-data="i_199_234_363_14">The education course you were taking has ended. Please click here.</a>
         */
        if ($(e).text().indexOf('edu') >= 0) {
            if ($(e).text().trim().split(' '))
                e.firstChild.firstChild.nodeValue = '你的课程已学习结束，请点此继续。';
            return;
        }

        /**
         * LSD od
         */
        if ($(e).text().contains(/LSD .+ overdosed/)) {
            if (eventsDict[$(e).text().trim()]) $(e).text(eventsDict[$(e).text().trim()]);
            return;
        }

        /**
         * 公司申请
         */
        if ($(e).text().contains(/Your application to join the company .+ has been/)) {
            $(e).contents().each((i, e) => {
                if (e.nodeType === 3) {
                    if (eventsDict[e.nodeValue.trim()]) {
                        e.nodeValue = eventsDict[e.nodeValue.trim()];
                    }
                }
            });
            return;
        }

        /**
         * 银行完成
         */
        if ($(e).text().contains(/Your bank investment has ended/)) {
            $(e).children().text('你的银行投资已经结束。请点击这里领取你的资金。');
            return;
        }

        /**
         * 人物升级
         * <span class="mail-link" id="event-865162632">Congratulations! You upgraded your level to 31!
         </span>
         */
        if ($(e).text().indexOf('upgraded') >= 0) {
            const level = e.firstChild.nodeValue.slice(44, -2);
            e.firstChild.nodeValue = '恭喜！你已升至' + level + '级！';
            return;
        }

        /**
         * 开新健身房
         * <span class="mail-link" id="event-855834754">You have successfully purchased membership in Deep Burn.</span>
         * 你已成功购买Deep Burn的健身房会员卡。
         */
        if ($(e).text().contains(/You have successfully purchased membership in/)) {
            const gymName = e.firstChild.nodeValue.trim().slice(46, -1);
            e.firstChild.nodeValue = `你已购买【${ gymList[gymName] }】健身房会员卡。`;
            return;
        }

        /**
         * 人物称号
         */
        if ($(e).text().contains(/You are now known in the city as a/)) {
            const trans = '现在你在这个城市中被称为';
            const title = $(e).text().trim().split(' ').slice(9).join(' ').slice(0, -1);
            $(e).text(`${ trans } ${ title }。`);
            return;
        }

        /**
         * 收下线
         */
        if ($(e).text().contains(/You have successfully referred/)) {
            $(e).contents().each((i, e) => {
                // 文字
                if (e.nodeType === 3) {
                    if (eventsDict[e.nodeValue.trim()]) {
                        e.nodeValue = eventsDict[e.nodeValue.trim()];
                    }
                }
                // referral list
                else if (e.nodeType === 1) {
                    if (eventsDict[$(e).text().trim()]) {
                        $(e).text(eventsDict[$(e).text().trim()]);
                    }
                }
            });
            return;
        }

        /**
         * new virus病毒
         * You completed the Simple Virus which is now in your inventory. You can begin programming a new virus
         * <a href="pc.php">here</a>
         * .
         *
         * 你完成了 "简单病毒"，它现在在你的库存中。你可以【点此】开始编程一个新的病毒。
         */
        if ($(e).text().indexOf('new virus') >= 0) {
            const virusName = e.firstChild.nodeValue.split(' ').slice(3, 5).join(' ');
            e.firstChild.nodeValue = `你完成了 ${ virusName }，它现在在你的物品库存中。你可以`;
            e.childNodes[1].firstChild.nodeValue = '点此';
            e.childNodes[2].nodeValue = '开始编程一个新的病毒。';
            return;
        }

        /**
         * 每月蓝星奖励
         */
        if ($(e).text().contains(/You found .+ and .+ on your doorstep/)) {
            const [item1, item2] = $(e).text().trim().slice(10, -18).split(' and ');
            const bookTitle = item2.contains(/a book titled/) ? item2.slice(15, -1) : null;

            if (bookTitle) {
                $(e).text(`你在家门口发现了 ${ item1.numWordTrans() } 和《${ bookTitle }》。`);
            } else {
                $(e).text(`你在家门口发现了 ${ item1.numWordTrans() } 和 ${ item2.numWordTrans() }。`);
            }
            return;
        }

        /**
         * 季度邮件奖励

         if ($(e).text().contains(/used the reward bonus code/)) {
                const code = $(e).text().trim().split(' ')[7];
                if (eventsDict[$(e).text().trim().replace(code, '{$}')])
                    $(e).text(eventsDict[$(e).text().trim().replace(code, '{$}')]
                        .replace('{$}', code));
                return;
            }

         /**
         * 求婚
         */
        if ($(e).text().contains(/accepted your proposal, you are now engaged/)) {
            const spouse = $(e).children(':first').text().trim();
            if (e.childNodes[1]) {
                e.childNodes[1].nodeValue = ` 接受了你的求婚，你现在和 ${ spouse } 订婚了！前往`;
            }
            if (e.childNodes[2] && e.childNodes[2].firstChild) {
                e.childNodes[2].firstChild.nodeValue = `这里`;
            }
            if (e.childNodes[3]) {
                e.childNodes[3].nodeValue = `完成仪式。`;
            }
            return;
        }

        /**
         * 帮派职位变更
         * Your position in
         * <a href="factions.php?step=profile&amp;ID=36134" i-data="i_92_242_62_14">Silver Hand</a>
         *  changed from Recruit to Knight.
         */
        if ($(e).text().indexOf('position') >= 0) {
            let prePos, curPos;
            const node3Spl = e.childNodes[2].nodeValue.split(' to ');
            if (node3Spl.length === 2) {
                prePos = node3Spl[0].slice(14, node3Spl[0].length);
                curPos = node3Spl[1].slice(0, node3Spl[1].length - 2);
            } else {
                log.info('职位出现" to "');// todo
                return;
            }
            e.firstChild.nodeValue = '你在 ';
            e.childNodes[2].nodeValue = ` 的职位从 ${ prePos } 变为 ${ curPos }。`;
            return;
        }

        /**
         * 加入帮派结果
         */
        if ($(e).text().indexOf('join the faction') >= 0) {
            const rsName = e.childNodes[2].nodeValue.trim().split(' ')[2];
            const rsDict = { 'accepted': '通过', 'declined': '拒绝', };
            e.firstChild.nodeValue = '加入帮派 ';
            e.childNodes[2].nodeValue = ` 的申请已${ rsDict[rsName] }。`;
            return;
        }
    });
}