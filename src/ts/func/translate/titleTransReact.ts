import { titleDict } from "../../dictionary/translation";

export default function titleTransReact(dom = document.querySelectorAll('h4[class^="title___"]')) {
    dom.forEach(e => {
        const title_trans = titleDict[e.innerText.trim()];
        if (title_trans) e.innerText = title_trans;
    });
}