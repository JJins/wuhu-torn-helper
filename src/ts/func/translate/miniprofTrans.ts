import playerStatusTrans from "./playerStatusTrans";
import sendCashTrans from "./sendCashTrans";

// mini profile 翻译
function miniprofTrans() {
    // 迷你资料卡状态
    playerStatusTrans($('div.profile-mini-root div.description span'));
    // 转钱
    sendCashTrans('div.profile-mini-root');
}

export default miniprofTrans