let log = { info: (args) => null };

/**
 * observe 包装
 * @deprecated TODO 泛用模版有性能问题
 */
export default function initOB(dom: Document | Element = document, opt = {}, func = () => {
}, dev = false, once = false) {
    //let count = -1;
    if (dev) {
        const mo = new MutationObserver((mutation) => {
            //count++;
            log.info(mutation)
            mo.disconnect();
            func();
            if (!once) {
                mo.observe(dom, opt)
            }
        });
        func();
        mo.observe(dom, opt);
    } else {
        //count++;
        const mo = new MutationObserver(() => {
            mo.disconnect();
            func();
            if (!once) mo.observe(dom, opt)
        });
        func();
        mo.observe(dom, opt)
    }
}
