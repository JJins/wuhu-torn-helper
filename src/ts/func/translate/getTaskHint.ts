import { missionDict } from "../../dictionary/translation";

/**
 * 任务助手
 */
export default function getTaskHint(task_name): string {
    task_name = task_name
        .toLowerCase()
        .replaceAll(' ', '_')
        .replaceAll('!', '')
        .replaceAll('-', '_')
        .replaceAll(',', '');
    if (!missionDict._taskHint[task_name]) return '暂无，请联系开发者';
    const task = missionDict._taskHint[task_name].task || null;
    const hint = missionDict._taskHint[task_name].hint || null;
    return `${ task ? '任务要求：' + task : '暂无，请联系<a href="profiles.php?XID=2687093">Woohoo</a>' }${ hint ? '<br>提示：' + hint : '' }`;
}