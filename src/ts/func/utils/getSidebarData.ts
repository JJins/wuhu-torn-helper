import { SessionStoreDataType } from "../../interface/SessionStoreDataType";

/**
 * 寻找 session storage 中以 sidebarData 开头的数据以对象返回
 */
export default function (): SessionStoreDataType {
    let json: string = null
    for (let i = 0; i < sessionStorage.length; i++) {
        let key = sessionStorage.key(i)
        if (key.startsWith('sidebarData')) {
            json = sessionStorage.getItem(key)
            break
        }
    }
    if (!json) {
        throw new TypeError('未找到 sidebarData')
    }
    return JSON.parse(json)
}
