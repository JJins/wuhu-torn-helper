const convertToCSV = (data: any[]) => {
    let csv = ''
    for (let i = 0; i < data.length; i++) {
        let row = ''
        for (const key in data[i]) {
            row += `"${ data[i][key] }",`
        }
        row = row.slice(0, -1) // 删除最后一个逗号
        csv += row + '\r\n' // 添加换行符
    }
    return csv
}

export { convertToCSV }
