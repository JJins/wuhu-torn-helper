const init: RequestInit = {
    "headers": {
        "accept": "*/*",
        "sec-ch-ua-mobile": "?0",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "x-requested-with": "XMLHttpRequest"
    },
    "referrer": "https://www.torn.com/companies.php",
    "referrerPolicy": "strict-origin-when-cross-origin",
    "body": null,
    "method": "GET",
    "mode": "cors",
    "credentials": "include"
};

export const fetchCurrentMoney = async (action?: string): Promise<number> => {
    return Number(await (await fetch(window.addRFC("/inputMoneyAction.php?step=" + (action ? action : "generalAction")), init)).text());
};

export const fetchCurrentCompanyAvailableMoney = () => {
    return fetchCurrentMoney("companyAction");
};

export const fetchCurrentPropVaultAvailableMoney = () => {
    return fetchCurrentMoney("propertyDepositAction");
};
