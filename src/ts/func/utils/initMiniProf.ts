// 引入torn miniprofile
export default function initMiniProf(selector) {
    let profileMini = {
        timeout: 0,
        clickable: false,
        rootElement: null,
        targetElement: null,
        rootId: 'profile-mini-root',
        rootSelector: '#profile-mini-root',
        userNameSelector: "a[href*='profiles.php?XID=']",
        // contentWrapper: '#wh-trans-icon',
        contentWrapper: selector,
        setClickable: function (value) {
            this.clickable = value
        },
        setRootElement: function () {
            if (!document.getElementById(this.rootId)) {
                this.rootElement = document.createElement('div');
                this.rootElement.classList.add(this.rootId);
                this.rootElement.id = this.rootId;
                $('body').append(this.rootElement);
            } else {
                window.ReactDOM.unmountComponentAtNode($(this.rootSelector).get(0));
                this.rootElement = document.getElementById(this.rootId);
            }
        },
        subscribeForHideListeners: function () {
            const that = this;
            let width = $(window).width();

            function handleResize(this: Window, e) {
                let $this = $(this);
                if ($this.width() !== width) {
                    width = $this.width();
                    hideMiniProfile.call(that, e);
                }
            }

            function handleScroll(this, e) {
                if (!document.activeElement.classList.contains('send-cash-input')) {
                    hideMiniProfile.call(that, e);
                }
            }

            function hideMiniProfile(this: typeof profileMini, e) {
                if ($(e.target).closest(this.rootSelector).length === 0 || ['resize', 'scroll'].includes(e.type)) {
                    that.targetElement = null;
                    let $thisUserNameSelector = $(this.userNameSelector);
                    window.ReactDOM.unmountComponentAtNode($(this.rootSelector).get(0));
                    $thisUserNameSelector.off('click', this.handleUserNameClick);
                    $thisUserNameSelector.unbind('contextmenu');
                    $(document).off('click', hideMiniProfile);
                    let $window = $(window);
                    $window.off('hashchange', hideMiniProfile);
                    $window.off('resize', handleResize);
                    $window.off('scroll', handleScroll);
                }
            }

            let $window = $(window);
            $(document).on('click', hideMiniProfile.bind(this));
            $window.on('hashchange', hideMiniProfile.bind(this));
            $window.on('resize', handleResize);
            if (that.targetElement.closest('#chatRoot')) {
                $window.on('scroll', handleScroll);
            }
        },
        subscribeForUserNameClick: function () {
            $(this.userNameSelector).click(this.handleUserNameClick.bind(this))
        },
        handleUserNameClick: function () {
            if (!this.clickable) {
                this.setClickable(true);
                return false;
            }
        },
        subscribeForContextMenu: function (element) {
            $(element).on('contextmenu', function (e) {
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();
                return false;
            })
        },
        handleMouseDown: function () {
            const that = this;
            $(this.contentWrapper).on('mousedown touchstart', this.userNameSelector, function (e) {
                if (e.which !== 1 && e.type !== 'touchstart') {
                    return false;
                }
                that.targetElement = e.currentTarget;
                that.subscribeForContextMenu(that.targetElement);
                that.handleFocusLost(e.currentTarget);
                that.timeout = window.setTimeout(function () {
                    if (e.type !== 'touchstart') {
                        that.setClickable(false);
                        that.subscribeForUserNameClick();
                    } else {
                        $(e.currentTarget).off('touchmove mouseleave');
                    }
                    that.subscribeForHideListeners();
                    that.setRootElement();
                    const userID = e.currentTarget.search.slice('?XID='.length);
                    const props = {
                        userID: userID,
                        event: e.originalEvent
                    };
                    window.renderMiniProfile(that.rootElement, props);
                }, 500);
                if (e.type !== 'touchstart') {
                    return false;
                }
            })
        },
        handleMouseUp: function () {
            const that = this;
            $(this.contentWrapper).on('mouseup touchend', this.userNameSelector, function () {
                that.timeout && clearTimeout(that.timeout);
            })
        },
        handleFocusLost: function (element) {
            const that = this;
            $(element).on('touchmove mouseleave', function unsubscribe() {
                that.timeout && clearTimeout(that.timeout);
                $(this).off('touchmove mouseleave', unsubscribe)
            })
        },
        init: function () {
            this.handleMouseDown();
            this.handleMouseUp();
        }
    };
    profileMini.init();
}
