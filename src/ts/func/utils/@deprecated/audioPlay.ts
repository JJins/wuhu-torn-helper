import Log from "../../../class/Log";

/**
 * 播放音频
 * @param {string} url 播放的音频URL
 * @returns {undefined}
 * @deprecated
 */
export default function audioPlay(url: string = 'https://www.torn.com/js/chat/sounds/Warble_1.mp3') {
    const audio = new Audio(url);
    audio.addEventListener("canplaythrough", () => {
        audio.play()
            .catch(err => Log.error(err))
            .then();
    });
}