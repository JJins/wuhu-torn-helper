/**
 * @deprecated
 */
export default function getYaoCD(): string {
    if (document.querySelector("#icon49-sidebar")) { // 0-10min
        return '<10分'
    } else if (document.querySelector("#icon50-sidebar")) { // 10min-1h
        return '<1时'
    } else if (document.querySelector("#icon51-sidebar")) { // 1h-2h
        return '1~2时'
    } else if (document.querySelector("#icon52-sidebar")) { // 2h-5h
        return '2~5时'
    } else if (document.querySelector("#icon53-sidebar")) { // 5h+
        return '>5时'
    } else {
        return '无效'
    }
}