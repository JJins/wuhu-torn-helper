/**
 * 使用 FetchUtils.ajax 替代
 * @deprecated
 */
export default function jQueryAjax(url: string, method: 'GET' | 'POST'): Promise<string> {
    return new Promise((res, rej) => {
        $.ajax({
            method: method,
            url: url,
            success: function (data) {
                res(data)
            },
            error: function (e) {
                rej(e)
            }
        });
    });
}