/**
 * @deprecated
 */
export default function elementReady(selector, content = document, timeout: number = 30000): Promise<HTMLElement | null> {
    return new Promise((resolve, reject) => {
        let el = content.querySelector(selector);
        if (el) {
            resolve(el);
            return
        }
        let observer = new MutationObserver((_, observer) => {
            content.querySelectorAll(selector).forEach((element) => {
                observer.disconnect();
                resolve(element);
            });
        });
        setTimeout(() => {
            observer.disconnect();
            reject(`等待元素超时! [${ selector }]\n${ content.documentElement.tagName }`);
        }, timeout);
        observer.observe(content.documentElement, { childList: true, subtree: true });
    });
}
