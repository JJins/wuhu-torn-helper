// import UserScriptEngine from "../../../enum/UserScriptEngine";
// import WuhuBase from "../../../class/WuhuBase";
// import CommonUtils from "../../../class/utils/CommonUtils";
// import Popup from "../../../class/utils/Popup";
// import * as FILTER from "../../../static/json/for_stock_item_filter.json";
//
// /**
//  * @deprecated
//  */
// export default async function forStock() {
//     if (CommonUtils.getScriptEngine() === UserScriptEngine.RAW) {
//         const insert = `<img alt="stock.png" src="https://jjins.github.io/t2i/stock.png?{{}}" style="max-width:100%;display:block;margin:0 auto;" />`;
//         new Popup(insert, '飞花库存');
//     } else {
//         const popup = new Popup(`请稍后${ CommonUtils.loading_gif_html() }`, '飞花库存');
//         let table = `<table><tr><th colspan="2">目的地 - 更新时间</th><th colspan="3">库存</th></tr>`;
//         const dest = FILTER;
//         const now = new Date();
//         const res = await WuhuBase.glob.fStock.get();
//         if (!res['stocks']) return;
//         dest.forEach(el => {
//             const update = (now.getTime() - new Date(res.stocks[el.name]['update'] * 1000).getTime()) / 1000 | 0
//             table += `<tr><td>${ el.show }</td><td>${ update / 60 | 0 }分${ update % 60 | 0 }秒前</td>`;
//             let count = 0;
//             res.stocks[el.name]['stocks'].forEach(stock => {
//                 if (el.stocks[stock.name]) {
//                     table += `<td${ stock['quantity'] === 0 ? ' style="background-color:#f44336;color:white;border-color:#000;"' : '' }>${ el.stocks[stock.name] } (${ stock['quantity'] })</td>`;
//                     count++;
//                 }
//             });
//             while (count < 3) {
//                 count++;
//                 table += '<td></td>';
//             }
//             table += '</tr>';
//         });
//         table += '</table>';
//         popup.getElement().innerHTML = table;
//     }
// }