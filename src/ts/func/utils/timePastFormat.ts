const timePastFormat = (ts: number): string => {
    // 毫秒
    if (ts < 1000) {
        return ts + 'ms'
    }
    // 秒
    else if (ts < 60000) {
        return (ts / 1000 | 0) + 's'
    }
    // 分
    else if (ts < 3600000) {
        return (ts / 60000 | 0) + 'm'
    }
    // 时
    else {
        return (ts / 3600000 | 0) + 'h'
    }
}

export { timePastFormat }
