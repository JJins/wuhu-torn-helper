const equipItem = (itemId: string, armoryId: string, type: number) => {
    return fetch(window.addRFC("https://www.torn.com/item.php"), {
        "headers": {
            "accept": "*/*",
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest"
        },
        "referrer": "https://www.torn.com/item.php",
        "referrerPolicy": "strict-origin-when-cross-origin",
        "body": `step=actionForm&item_id=${ itemId }&armour-from-set=&type=${ type }&action=equip&item=${ itemId }&id=${ armoryId }&confirm=1`,
        "method": "POST",
        "mode": "cors",
        "credentials": "include"
    })
}

export default equipItem
