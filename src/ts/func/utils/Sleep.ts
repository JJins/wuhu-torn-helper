const Sleep = (ms: number = 0) => {
    return new Promise(resolve => window.setTimeout(resolve, ms))
}

export default Sleep
