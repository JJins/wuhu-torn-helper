import Popup from "../../class/utils/Popup";
import LocalConfigWrapper from "../../class/LocalConfigWrapper";
import { Container } from "../../container/Container";

// 落地转跳设置
export default function landedRedirect() {
    let localConfigWrapper: LocalConfigWrapper = Container.factory(LocalConfigWrapper);

    let p = document.createElement('p');
    let input = document.createElement('input');
    let buttonSave = document.createElement('button');
    let buttonCmp = document.createElement('button');
    let buttonFct = document.createElement('button');
    let buttonTest = document.createElement('button');
    let br = document.createElement('br');

    p.innerHTML = '飞机落地后转跳的页面，关闭功能请置空：';
    input.placeholder = 'URL';
    input.value = localConfigWrapper.config.landedRedirect ?? '';
    input.style.display = 'block';
    input.style.textAlign = 'left';
    input.style.width = '100%';
    input.style.padding = '8px';
    input.style.margin = '8px -8px';
    buttonSave.innerHTML = '保存';
    buttonCmp.innerHTML = '填入公司金库';
    buttonFct.innerHTML = '填入帮派金库金库';
    buttonTest.innerHTML = '测试链接';

    buttonSave.addEventListener('click', () => localConfigWrapper.config.landedRedirect = input.value);
    buttonCmp.addEventListener('click', () => input.value = 'https://www.torn.com/companies.php#/option=funds');
    buttonFct.addEventListener('click', () => input.value = 'https://www.torn.com/factions.php?step=your#/tab=armoury');
    buttonTest.addEventListener('click', () => window.open(input.value));

    let node = new Popup('', '落地转跳').getElement();
    node.append(p, input, buttonSave, br, buttonCmp, buttonFct, buttonTest);
}