import CommonUtils from "../../class/utils/CommonUtils"

type YataBSEstData = {
    "total": number,
    "score": number,
    "type": "Offensive" | "Defensive" | "Balanced",
    "skewness": number,
    "timestamp": number,
    "version": 1,
}

type YataApiResponse = {
    // key为请求id
    [key: number]: YataBSEstData
    error?: { 'error': string, 'code': 1 | 2 | 3 | 4 }
}

type YataApiDataWrap = YataBSEstData & {
    id: string,
    isCache: boolean,
}

const cacheExpireMs = 86400000 // 一天
const KEY = 'WHBSEstCache'

const getCacheObj = () => {
    let obj: { [key: string]: YataApiDataWrap }
    try {
        obj = JSON.parse(localStorage.getItem(KEY)) ?? {}
    } catch (e) {
        obj = {}
    }
    return obj
}

const getCache = (id: number): YataApiDataWrap => {
    let cache: YataApiDataWrap = getCacheObj()[id]
    if (cache && ((Date.now() - (cache.timestamp * 1000)) < cacheExpireMs)) {
        cache.isCache = true
    } else {
        cache = null
    }
    return cache
}

const setCache = (data: YataApiDataWrap): void => {
    const cache = getCacheObj()
    cache[data.id] = data
    localStorage.setItem(KEY, JSON.stringify(cache))
}

/**
 * 清空本地缓存
 */
const purge = () => {
    localStorage.removeItem(KEY)
}

const fetchYata = async (id: number, apikey: string): Promise<YataApiDataWrap> => {
    if (!id || !apikey) {
        throw new TypeError('请求yata接口时出错: id和apikey不能为空')
    }
    const cache = getCache(id)
    if (cache) {
        return cache
    } else {
        let responseString: string, response: YataApiResponse
        try {
            responseString = await CommonUtils.COFetch(`https://yata.yt/api/v1/bs/${ id }?key=${ apikey }`)
        } catch (e) {
            throw new TypeError('请求yata接口时出错 ' + e.message)
        }
        try {
            response = JSON.parse(responseString)
        } catch (e) {
            throw new TypeError('解析yata接口响应时出错 ' + e.message)
        }
        if (response.error) {
            switch (response.error.code) {
                case 1:
                    throw new TypeError('请求yata接口时出错: yata服务端错误-' + response.error.error)
                case 2:
                    throw new TypeError('请求yata接口时出错: 脚本逻辑错误-' + response.error.error)
                case 3:
                    throw new TypeError('请求yata接口时出错: 已达到次数限制-' + response.error.error)
                case 4:
                    throw new TypeError('请求yata接口时出错: apikey错误-' + response.error.error)
            }
        }
        const wrapper = <YataApiDataWrap>response[id]
        wrapper.id = String(id)
        wrapper.isCache = false
        setCache(wrapper)
        return wrapper
    }
}

export { fetchYata, YataApiDataWrap, purge }
