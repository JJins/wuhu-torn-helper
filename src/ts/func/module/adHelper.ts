import Popup from "../../class/utils/Popup";
import { Container } from "../../container/Container";
import Logger from "../../class/Logger";

// 传单助手
export default function adHelper() {
    Container.factory(Logger).error('adHelper')
    let popup = new Popup('', '传单助手').getElement();
    document.querySelector('#chatRoot').classList.remove('wh-hide');
    let info = document.createElement('p');
    let ad_input = document.createElement('textarea');
    let place_button = document.createElement('button');
    let clear_button = document.createElement('button');
    let paste_button = document.createElement('button');
    let style = document.createElement('style');

    info.innerHTML = '打开多个聊天框后，点击<b>填写传单</b>将自动粘贴文本框中的内容进入所有<b>已打开的聊天框</b>。页面外的聊天框同样有效。';
    ad_input.placeholder = '此处输入广告语';
    ad_input.style.width = '100%';
    ad_input.style.minHeight = '80px';
    place_button.innerText = '填写传单';
    clear_button.innerText = '清空所有聊天框';
    paste_button.innerText = '粘贴剪切板';
    style.innerHTML = '#chatRoot > div{z-index:199999 !important;}';
    place_button.classList.add('torn-btn');
    clear_button.classList.add('torn-btn');
    paste_button.classList.add('torn-btn');

    place_button.addEventListener('click', () => {
        let chats = Array.from(document.querySelectorAll('#chatRoot textarea[name="chatbox2"]') as NodeListOf<HTMLTextAreaElement>);
        chats.forEach(chat => chat.value = ad_input.value);
    });
    clear_button.addEventListener('click', () => {
        let chats = document.querySelectorAll('#chatRoot textarea[name="chatbox2"]') as NodeListOf<HTMLTextAreaElement>;
        chats.forEach(chat => chat.value = '');
    });
    paste_button.addEventListener('click', async () => {
        ad_input.focus();
        ad_input.value = await navigator.clipboard.readText();
    });

    popup.appendChild(style);
    popup.appendChild(info);
    popup.appendChild(ad_input);
    popup.appendChild(document.createElement('br'));
    popup.appendChild(place_button);
    popup.appendChild(clear_button);
    popup.appendChild(paste_button);
}