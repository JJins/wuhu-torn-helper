import toThousands from "../utils/toThousands";
import CommonUtils from "../../class/utils/CommonUtils";
import CITY_FINDER_CSS from "../../../static/css/city_finder.module.css";
import TornStyleBlock from "../../class/utils/TornStyleBlock";
import ItemHelper from "../../class/utils/ItemHelper";
import { Container } from "../../container/Container";

/**
 * 2023-02-22 修改物品价格查询逻辑，缓存优先
 * @param _base
 */
export default function cityFinder(_base: TornStyleBlock): void {
    let commonUtils: CommonUtils = Container.factory(CommonUtils);
    commonUtils.styleInject(CITY_FINDER_CSS);
    let itemHelper: ItemHelper = Container.factory(ItemHelper);
    // 物品名与价格
    let items: {
        [k: number]: {
            name: string
            price: number
        }
    } = null;

    const header = document.createElement('div');

    header.innerHTML = '捡垃圾助手';
    const info = document.createElement('div');
    info.innerHTML = '已找到物品：';

    _base.append(header, info);
    document.body.classList.add('wh-city-finds');

    commonUtils.querySelector('div.leaflet-marker-pane').then(elem => {

        // 发现的物品id与map img node
        const founds = [];
        elem.querySelectorAll('img.map-user-item-icon').forEach(node => {
            const item_id = node.src.split('/')[5];
            const finder_item = document.createElement('span');

            finder_item.classList.add('wh-city-finder-item');
            finder_item.innerHTML = item_id;
            founds.push({ 'id': item_id, 'node': finder_item, 'map_item': node });

            info.append(finder_item);
        });
        // 未发现物品 返回
        if (founds.length === 0) {
            info.innerHTML = '空空如也，请大佬明天再来';
            return;
        }
        // 将id显示为物品名与价格的函数
        const displayNamePrice = () => {
            // 总价
            let total = 0;
            founds.forEach(el => {
                const value = items[el.id]['price'];
                el.node.innerHTML = `<img src="${ el.map_item.src }" alt="" />${ items[el.id]['name'] } ($${ toThousands(value) })`;
                // 灰色 100k以下
                if (value < 100000) el.node.style.backgroundColor = '#9e9e9e';
                // 绿色 1m以下
                else if (value < 1000000) el.node.style.backgroundColor = '#4caf50';
                // 蓝色 25m以下
                else if (value < 25000000) el.node.style.backgroundColor = '#03a9f4';
                // 橙色 449m以下
                else if (value < 449000000) el.node.style.backgroundColor = '#ffc107';
                // 红色 >449m
                else if (value >= 449000000) el.node.style.backgroundColor = '#f44336';
                total += items[el.id]['price'];
            });
            header.innerHTML = `捡垃圾助手 - ${ founds.length } 个物品，总价值 $${ toThousands(total) }`;
            // _base.setTitle(`捡垃圾助手 - ${ founds.length } 个物品，总价值 $${ toThousands(total) }`);
        };
        // // 未取到数据时添加循环来调用函数
        // if (items === null) {
        //     // 15s超时
        //     let timeout = 30;
        //     const interval = window.setInterval(() => {
        //         timeout--;
        //         if (items !== null) {
        //             displayNamePrice();
        //             window.clearInterval(interval);
        //         }
        //         if (0 === timeout) {
        //             Log.info('获取物品名称与价格信息超时')
        //             window.clearInterval(interval)
        //         }
        //     }, 500);
        // }
        // // 无法跨域获取数据时
        // else if (items === undefined) {
        //     info.innerHTML += '(当前平台暂不支持查询价格)';
        // }
        // // 调用函数
        // else {
        //     displayNamePrice();
        // }

        let priceData = itemHelper.getLocalPriceData();
        if (priceData.data) {
            items = priceData.data;
            displayNamePrice();
        } else {
            window.setTimeout(async () => {
                items = (await priceData.promise);
                displayNamePrice();
            }, 0);
        }
    })
}
