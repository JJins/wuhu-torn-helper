// import Alert from "../../class/utils/Alert";
//
// // 一键起飞
// export default function doQuickFly() {
//     // [id: dest, _type: (1...4), ts: timestamp]
//     const [_id, _type, ts] = sessionStorage['wh-quick-fly'].trim().split(' ');
//     if (new Date().getTime() - ts > 20000) {
//         new Alert('超时，一键起飞计划已取消');
//         return;
//     }
//     const keynode = document.querySelector('div[data-id][data-key]');
//     if (!keynode) {
//         new Alert('出错了，无法起飞，已取消');
//         return;
//     }
//     const _key = keynode.getAttribute('data-key');
//     window.getAction({
//         type: 'post',
//         data: {
//             step: 'travel',
//             id: getDestId(_id),
//             key: _key,
//             type: ['standard', 'airstrip', 'private', 'business'][_type]
//         },
//         success: function (str) {
//             new Alert(str)
//             if (str.includes('err')) {
//                 new Alert('起飞出错了');
//                 return;
//             }
//             window.location.href = 'https://www.torn.com/index.php'
//         },
//         before: function () {
//         }
//     });
//     delete sessionStorage['wh-quick-fly'];
// }
//
// // 起飞目的地id
// function getDestId(dest) {
//     // 墨、开、加、夏、英、阿、瑞s、立本、祖、迪、南
//     return [2, 12, 9, 3, 10, 7, 8, 5, 6, 11, 4][dest];
// }
