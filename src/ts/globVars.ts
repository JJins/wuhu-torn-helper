import Popup from "./class/utils/Popup";
import { reactive } from 'vue'
import IFeatureResult from "./man/IFeatureResult";

type ResponseHandlers = ((url: string, responseBody: {
    json: unknown,
    text: string,
    isModified: boolean
}, opt: {
    method: string,
    requestBody: unknown
}) => void)[];

/**
 * 通用全局参数
 */
class GlobVars {
    // 监听到的fetch数据
    WH_NET_LOG: unknown[] = [];
    map: { [key: string]: unknown } = {};
    responseHandlers: ResponseHandlers = [];
    version = '$$WUHU_DEV_VERSION$$';
    popup_node: MyHTMLElement | Popup = null;
    loadTime: number = 0;
    buttons = reactive<FloatButtonData[]>([])
    featureStatus = reactive<IFeatureResult[]>([])
}

export default new GlobVars();

interface FloatButtonData {
    txt: string,
    func: (ev: Event) => void
}
