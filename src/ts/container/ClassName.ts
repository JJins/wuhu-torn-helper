const CLASSNAME_METADATA_KEY = Symbol("CLASSNAME_KEY");

/**
 * 记录类名，避免打包后丢失原类名
 */
export default function ClassName(className: string): ClassDecorator {
    return function <TFunction extends Function>(target: TFunction): TFunction {
        Reflect.defineMetadata(CLASSNAME_METADATA_KEY, className, target);
        return target;
    };
}

export function GetClassName(target: ClassType<{}>): string {
    return Reflect.getMetadata(CLASSNAME_METADATA_KEY, target);
}
