import { GetClassName } from "./ClassName";

const INJECTABLE_METADATA_KEY = Symbol("INJECTABLE_KEY");

// TODO 实现非单例注入
export enum INJECT_MODE {
    Singleton = 1,
}

export function Injectable(injectMode: INJECT_MODE = INJECT_MODE.Singleton): ClassDecorator {
    return function (target: any) {
        Reflect.defineMetadata(INJECTABLE_METADATA_KEY, injectMode, target);
        return target;
    };
}

export function assertInjectable(target: any) {
    if (!Reflect.getMetadata(INJECTABLE_METADATA_KEY, target)) {
        throw new TypeError(
            `[${ GetClassName(target) || target.name }] not injectable`
        );
    }
}
