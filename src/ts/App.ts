import Interrupt from "./class/Interrupt"
import Initializer from "./class/Initializer"
// import { Common } from "./class/Common"
// import UrlRouter from "./class/UrlRouter"
import translateMain from "./func/translate/translateMain"
import CommonUtils from "./class/utils/CommonUtils"
import LocalConfigWrapper from "./class/LocalConfigWrapper"
import ClassName from "./container/ClassName"
import { Injectable } from "./container/Injectable"
import FeatureMan from "./man/FeatureMan"
import Logger from "./class/Logger"

@ClassName('Application')
@Injectable()
export default class App {
    private readonly logger = Logger.factory(App)

    constructor(
        private readonly interrupt: Interrupt,
        private readonly initializer: Initializer,
        // private readonly common: Common,
        // private readonly urlRouter: UrlRouter,
        private readonly configWrapper: LocalConfigWrapper,
        private readonly utils: CommonUtils,
        private readonly featureMan: FeatureMan,
    ) {
    }

    public run() {

        this.interrupt.conditionInterrupt();

        // 初始化
        this.initializer.init();

        // this.featureMan.fStart().then(() => this.featureMan.printTable());

        // 插件图标和设置菜单
        // this.icon.init();

        let tmp = () => {
            // // 所有页面通用
            // try {
            //     // this.common.resolve.apply(this.common, this.run);
            //     // this.common.resolve(() => this.run());
            // } catch (e) {
            // }
            (async function (self: App) {
                await self.featureMan.fStart()
                self.featureMan.printTable()
            })(this)

            // URL匹配
            // this.urlRouter.resolve();

            // 翻译
            // if (this.configWrapper.config.transEnable)
            //     translateMain(window.location.href);
        };
        // TODO 临时检测jquery
        if (typeof $ === "function") {
            tmp();
        } else {
            this.utils.jQueryReady().then(() => tmp());
        }
    }
}
