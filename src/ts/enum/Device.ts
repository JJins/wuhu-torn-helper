enum Device {
    PC = 'pc',
    MOBILE = 'mobile',
    TABLET = 'tablet',
}

export default Device