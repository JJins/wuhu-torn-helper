enum UserScriptEngine {
    RAW = 'raw',
    GM = 'gm',
    PDA = 'pda',
}

export default UserScriptEngine