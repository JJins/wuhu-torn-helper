import Log from "../class/Log";
import { MENU_ITEM_TYPE } from "../interface/MenuItem";
import ClassName from "../container/ClassName";
import Debug from "../class/provider/Debug";
import "reflect-metadata";
import { Injectable } from "../container/Injectable";
import { Container } from "../container/Container";
import NetHighLvlWrapper from "../class/utils/NetHighLvlWrapper";
import Logger from "../class/Logger";
import QuickGymTrain from "../class/action/QuickGymTrain";
import LocalConfigWrapper from "../class/LocalConfigWrapper";
import { ElMessage } from "element-plus";

@ClassName('Test')
@Injectable()
class Test {
    constructor(
        private readonly logger: Logger,
        private readonly netHighLvlWrapper: NetHighLvlWrapper,
        private readonly quickGymTrain: QuickGymTrain,
        private readonly localConfigWrapper: LocalConfigWrapper,
    ) {
    }

    public show(): void {
        this.test();
    }

    @Debug
    private test() {
        this.quickGymTrain.doTrain();
        return 0;
    }

    private case1() {
        // FetchUtils.getInstance().ajaxFetch({
        //     url: '/inventory.php?step=info&itemID=205&armouryID=0&asObject=true',
        //     method: 'GET',
        //     referrer: 'bazaar.php',
        // });
        // ItemValueQueryHandler.getInstance().show();
    }

    private case2() {
        document.head.insertAdjacentHTML(
            "afterbegin",
            // `<meta http-equiv="Content-Security-Policy" content="script-src 'none'">`
            `<meta http-equiv="Content-Security-Policy" content="connect-src 'self'">`
        );
    }

    private async case3() {
    }

    private case4() {
        Log.info('case4()');
    }
}

export default {
    domType: MENU_ITEM_TYPE.BUTTON,
    domText: '🐞 测试2',
    clickFunc: () => {
        Container.factory(Test).show()
        ElMessage('test');
    },
}
