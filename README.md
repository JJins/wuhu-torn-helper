# Wuhu Torn Helper

[![LICENSE](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

[中文](README_ZHCN.md)

[CHANGELOG(CN)](CHANGELOG.md)

A customized auxiliary-enhancement user script designed for a browser-based MMORPG game, featuring a range of convenient
functions.

This script does not include any automation-related code.

## Build

    npm init
    npm run rollup

## Use

[release.min.user.js](release.min.user.js)

Please install with Tampermonkey (for PC browser) or TornPDA.
