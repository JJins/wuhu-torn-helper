declare interface String {
    contains(keywords: RegExp | string): boolean;

    /* 翻译 */

    // 时分秒转换
    replaceHMS(): string;

    // 数词转换 a an some
    numWordTrans(): string;
}

declare interface Window {
    $?: JQueryStatic;
    jQuery?: JQueryStatic;
    WHPARAMS?: any;
    hasWHQuickFlyOpt?: boolean;
    // 插件运行标识
    WHTRANS?: number;
    Vue?: Function;
    /**
     *  油猴脚本引擎自带
     */
    unsafeWindow?: Window & typeof globalThis;
    /**
     * google
     */
    _gaUserPrefs?: unknown;

    GM_xmlhttpRequest(init: GM_RequestParams): void;

    GM_getValue(k: string, def: any): unknown;

    GM_setValue(k: string, v: any): void;

    /**
     *  TORN自带
     */
    ReactDOM?: any;
    dataLayer?: unknown;

    eval(exc: string): void;

    addRFC(url: URL | string): string;

    getAction(opt: TornGetActionParams): void;

    initializeTooltip(selector: string, elemId: string): void;

    renderMiniProfile(node: Element, props: any): never;

    /**
     *  PDA自带
     */
    PDA_httpGet(url: URL | string): Promise<PDA_Response>;

    PDA_httpPost(url: URL | string, init: any, body: any): Promise<PDA_Response>;

    // TODO 临时测试用
    // [key: string]: unknown;
}

declare interface GM_RequestParams {
    method?: string,
    url?: URL | string,
    data?: any,
    headers?: any,
    onload?: Function,
    onerror?: Function,
    ontimeout?: Function,
}

declare interface PDA_Response {
    responseText: string
}

declare interface Element {
    innerText?: string;
    src?: string;
}

declare interface Notification {
    id?: number;
}

declare interface Array<T> {
    fest_date_dict?: { [key: string]: { name: string, eff: string } };
    fest_date_list?: string[];

    [key: string]: any;
}

declare interface Navigator {
    userAgentData?: any;
}

declare interface TornGetActionParams {
    type: 'post' | 'get',
    action?: string,
    data?: {
        step?: string,
        id?: number,
        key?: string,
        type?: string
    },
    success: Function,
    before?: Function
}

declare module "*.html" {
    const html: string;
    export default html;
}
declare module "*.module.css" {
    const css: string;
    // const classes: { [key: string]: string };
    // export default classes;
    export default css;
    // export { css };
}

declare function GM_xmlhttpRequest(init: any): void;

declare var unsafeWindow: Window & typeof globalThis;
declare type Constructor<T = any> = new (...args: any[]) => T;

declare interface ClassType<T> {
    new(...args: unknown[]): T
}
